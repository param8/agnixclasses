
<!DOCTYPE html>
<html lang="en">

    
<!-- Mirrored from themes.mintycodes.com/quicky/ltr/light-skin/chat-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2023 10:55:19 GMT -->
<head>
        <meta charset="UTF-8">
        <!-- Viewport-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
        <!-- SEO Meta Tags-->
        <meta name="keywords" content="" />
        <meta name="description"
            content="" />
        <meta name="subject" content="communication">
        <meta name="copyright" content="frontendmatters">
        <meta name="revised" content="" />
        <title>Chat</title>
        <!-- Favicon and Touch Icons-->
        <!--<link rel="apple-touch-icon" sizes="180x180" href="../public/favicon.ico">-->
        <!--<link rel="icon" type="image/png" sizes="32x32" href="../public/favicon.ico">-->
        <!--<link rel="icon" type="image/png" sizes="16x16" href="../public/favicon.ico">-->
        <link rel="shortcut icon" href="javascript:void(0)" />
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="assets/webfonts/inter/inter.css">
        <link rel="stylesheet" href="assets/css/app.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
    </head>