<?php 
  session_start();
  if(isset($_SESSION['unique_id'])){
      include_once "config.php";
      $outgoing_id = $_SESSION['unique_id'];
      $incoming_id = mysqli_real_escape_string($conn, $_POST['incoming_id']);
      $output = "";
      if($outgoing_id!==$incoming_id){
      $sql = "SELECT messages.*,users.profile_pic FROM messages LEFT JOIN users ON users.unique_id = messages.outgoing_msg_id
              WHERE (outgoing_msg_id = {$outgoing_id} AND incoming_msg_id = {$incoming_id})
              OR (outgoing_msg_id = {$incoming_id} AND incoming_msg_id = {$outgoing_id}) ORDER BY msg_id";
      $query = mysqli_query($conn, $sql);
      if(mysqli_num_rows($query) > 0){
          while($row = mysqli_fetch_assoc($query)){
            $sendDate =  date('d-m-Y')==date(date('d-m-Y',strtotime($row['created_at']))) ? date('H:i A',strtotime($row['created_at'])) : date('d M-Y H:i',strtotime($row['created_at']));
              if($row['outgoing_msg_id'] == $outgoing_id){
              //echo  $row['outgoing_msg_id'];
                  $output .='<div class="message self ">
                  <div class="message-wrapper">
                      <div class="message-content">
                          <span>'. $row['msg'] .'</span>
                      </div>
                  </div>
                  <div class="message-options">
                      <div class="avatar avatar-sm"><img alt=""src="../'.$_SESSION['profile_pic'].'"></div>
  
                      <span class="message-date">'.$sendDate.'</span>
  
              </div></div>'; 
              }else{
               // echo  $row['incoming_msg_id'];
              $output .= ' <div class="message">
                              <div class="message-wrapper">
                      <div class="message-content">
                          <span>'. $row['msg'].'</span>
                      </div>
                  </div>
                  <div class="message-options">
                      <div class="avatar avatar-sm"><img alt=""
                              src="../'.$row['profile_pic'] .'"></div>
                      <span class="message-date">'.$sendDate.'</span>
                  </div>
              </div>';
                 
              }
          }
      }else{
          $output .= '<div class="text-center mt-5 text-danger">No messages are available. Once you send message they will appear here.</div>';
      }
    }else{
        $output .= '<h3 class="text-center mt-5 text-success">Welcome '.$_SESSION['name'].' Please continue chat .</h3>';  
    }
      echo $output;
  }
  
  ?>