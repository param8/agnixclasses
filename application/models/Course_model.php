<?php 
class Course_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_courses($condition){
    $this->db->select('*');
    $this->db->where($condition);
    return $this->db->get('courses')->result();
  }
  
  public function get_course($condition){
    $this->db->where($condition);
    return $this->db->get('courses')->row();
  }



  // public function get_course_with_subject_counter($condition){ 

  //   if($this->session->userdata('user_type')=='Teacher'){
  //     $this->db->select('subjects.*,courses.course');
  //     $this->db->from('subjects');
  //     $this->db->join('courses', 'courses.id = subjects.courseID','left');
  //     $this->db->where($condition);
  //     $this->db->where_in('subjects.id',explode(',',$this->session->userdata('subjectID')));
  //     $this->db->group_by('subjects.courseID'); 
  //     return $this->db->get()->result();
  //     foreach($query as $key => $value)
  //     {
  //       $this->db->select('count(id) as totalCourses');
  //       $this->db->where('categoryID', $value['id']);
  //       $query[$key]['course_count'] = $this->db->get('courses')->row_array();
  //     }  
  //     }else{
  //       $this->db->where($condition);
  //       return $this->db->get('courses')->result();
  //       foreach($query as $key => $value)
  //     {
  //       $this->db->select('count(id) as totalCourses');
  //       $this->db->where('categoryID', $value['id']);
  //       $query[$key]['course_count'] = $this->db->get('courses')->row_array();
  //     }  
  //     }

    // $this->db->select('category.*,users.name');
    // $this->db->join('users', 'users.id = category.userID','left');
    // $this->db->where($condition);
    // $query = $this->db->get('category')->result_array();
    // foreach($query as $key => $value)
    // {
    //    $this->db->select('count(id) as totalCourses');
    //    $this->db->where('categoryID', $value['id']);
    //   $query[$key]['course_count'] = $this->db->get('courses')->row_array();
    // }  
    //  return $query;

   //}


  public function store_course($data){
     $this->db->insert('courses', $data);
     return $this->db->insert_id();
  }

  public function update_course($data,$condition){
    $this->db->where($condition);
    return $this->db->update('courses',$data);
  }

 public function get_student_booked_course($condition){
    $this->db->select('booked_courses.*,subjects.subject as subject_name,subjects.start_date,subjects.end_date,subjects.fees,subjects.description as subject_discription,subjects.discount,subjects.mode,subjects.image,courses.course,users.name as user_name,users.email as user_email,users.contact as user_contact');
    $this->db->from('booked_courses');
    $this->db->join('subjects', 'subjects.id = booked_courses.courseID','left');
    $this->db->join('courses', 'courses.id = subjects.courseID','left');
    // $this->db->join('cities', 'cities.id = courses.locationID','left');
    // $this->db->join('subjects', 'subjects.id = courses.id','left');
    $this->db->join('users', 'users.id = booked_courses.studentID','left');
    // $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
    $this->db->where($condition);
    if($this->session->userdata('courses_booked')){
        $this->db->where('subject.id',$this->session->userdata('courses_booked'));  
    }
    $this->db->order_by('booked_courses.courseID','desc');
    return $this->db->get()->result();
}

public function get_booked_course($condition){
    $this->db->where($condition);
    return $this->db->get('booked_courses')->row();
}
}