<div class="page-content">
		<!-- inner page banner -->
			<div class="dz-bnr-inr overlay-secondary-dark dz-bnr-inr-sm" style="background-image:url(images/background/bg3.jpg);">
				<div class="container">
					<div class="dz-bnr-inr-entry">
						<h1><?=$page_title?></h1>
						<nav aria-label="breadcrumb" class="breadcrumb-row">
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?= base_url(); ?>"> Home</a></li>
								<li class="breadcrumb-item"><?=$page_title?></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- inner page banner End-->
            <!-- contact area -->
			<section class="invoice printableArea">
		  <div class="row">
			<div class="col-12 no-print">
			  <div class="bb-1 clearFix">
				<div class="text-end pb-15">
					<!-- <button class="btn btn-success" type="button"> <span><i class="fa fa-print"></i> Save</span> </button> -->
					<button id="print2" class="btn btn-warning" type="button" onclick="printInvoice();"> <span><i class="fa fa-print"></i> Print</span> </button>
				</div>	
			  </div>
			</div>
			<div class="col-12">
			<div class="page-header">
				<h2 class="d-inline"><span class="font-size-30">Invoice Details</span></h2>
				<div class="pull-right text-right">
					<h3>01March2022</h3>
				</div>	
			  </div>
</div>
			<!-- /.col -->
		  </div>
		  <div class="row invoice-info">
			<div class="col-md-6 invoice-col">
			  <strong>From</strong>	
			  <address>
				<strong class="text-blue font-size-24"><?=$order_details->name;?></strong><br>
				<?=$order_details->address;?><br>
				<?=$order_details->pin_code;?>, <?=$order_details->city;?>, <?=$order_details->state;?><br>
				<strong>Phone: <?=$order_details->mobile;?> &nbsp;&nbsp;&nbsp;&nbsp; Email: <?=$order_details->email;?></strong>  
			  </address>
			</div>
			<!-- /.col -->
			<div class="col-md-6 invoice-col text-right pl-160">
			  <strong>To</strong>
			  <address>
				<strong class="text-blue font-size-24"><?=$shipping_details->name;?></strong><br>
				<?=$shipping_details->address;?><br>
				<?=$shipping_details->street;?> <?=$shipping_details->pin_code;?>, <?=$shipping_details->city;?>, <?=$shipping_details->state;?><br>
				<strong>Phone: <?=$shipping_details->mobile;?> &nbsp;&nbsp;&nbsp;&nbsp; Email: <?=$order_details->email;?></strong>
			  </address>
			</div>
			<!-- /.col -->
			<div class="col-sm-12 invoice-col mb-15 pl-160">
				<div class="invoice-details row no-margin">
				  <div class="col-md-6 col-lg-6"><b>Order ID:</b> <?=$order_details->order_no;?></div>
				  <div class="col-md-6 col-lg-6 text-right"><b>Payment Status:</b> <?=$order_details->payment_status;?></div>
				</div>
			</div>
		  <!-- /.col -->
		  </div>
		  <div class="row">
			<div class="col-12 table-responsive">
			  <table class="table table-bordered">
				<tbody>
				<tr>
				  <th>#</th>
				  <th>School</th>
				  <th>Class</th>
				  <th>Book Set</th>
				  <th>Details</th>
				  <th class="text-right">Quantity</th>
				  <th class="text-right">Unit Cost</th>
				  <th class="text-right">Subtotal</th>
				</tr>
				<?php
					$i=1;
					$total=0;
					$subtotal=0;
					foreach($order_items as $item){
						$total = $item->qty*$item->mrp;
						$subtotal += $total;
				?>
				<tr>
				  <td><?=$i++;?></td>
				  <td><?=$item->school_name;?></td>
				  <td><?=$item->school_class;?></td>
				  <td><?=$item->title;?></td>
				  <td>
					<?php 
						$item_ids = explode(',',$item->items);
						$i=1;
						foreach($item_ids as $item_id){
							echo $i. ". " . getItemNameByItemId($item_id)."<br>";
							$i++;
						}
				  	?>
				  </td>
				  <td class="text-right"><?=$item->qty;?></td>
				  <td class="text-right">रु<?=$item->mrp;?></td>
				  <td class="text-right">रु<?=number_format((float)$total, 2, '.', '');?></td>
				</tr>
				<?php } ?>
				</tbody>
			  </table>
			</div>
			<!-- /.col -->
		  </div>
		  <div class="row">
			<div class="col-12 text-right">
				<div>
					<?php
						$tax=0;
						$shipping_charge=0;
						$tax = $subtotal*18/100;
						$shipping_charge = $order_details->shipping_charge;					
					?>
					<p>Sub - Total amount  :  रु<?=number_format((float)$subtotal, 2, '.', '');?></p>
					<p>Tax (18%)  :  रु<?=number_format((float)$tax, 2, '.', '');?></p>
					<p>Shipping  :  रु<?=number_format((float)$shipping_charge, 2, '.', '');?></p>
				</div>
				<div class="total-payment">
					<h3><b>Total :</b> रु<?=number_format((float)($subtotal+$tax+$shipping_charge), 2, '.', '');?></h3>
				</div>

			</div>
			<!-- /.col -->
		  </div>
		  <div class="row no-print">
			<div class="col-12 text-center">
			  <button type="button" class="btn btn-success" onclick="window.history.go(-1); return false;"><i class="fa fa-backward"></i> Back
			  </button>
			</div>
		  </div>
		</section>
		<!-- contact area End--> 
	
</div>