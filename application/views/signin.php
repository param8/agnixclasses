<section class="space-bottom">
  <div class="container">
    <div class="contact-wrap1">
      <div class="row align-items-center">
        <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">

        </div>
        <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">
          <form action="<?=base_url('Authantication/login')?>" id="Frontlogin" class="form-style3">

            <h3 class="form-title text-center">Signin</h3>
            <div class="row gx-20">
              <div class="form-group col-md-12"><input type="email" name="email" id="email" placeholder="Email Address">
                <i class="fal fa-envelope"></i></div>
              <div class="form-group col-md-12"><input type="password" name="password" id="password"
                  placeholder="Enter Password"> <i class="fal fa-lock"></i></div>


              <div class="form-btn col-12 text-center">
                <button type="submit" class="as-btn style2">Signin Now<i
                    class="fas fa-long-arrow-right ms-2"></i></button></div>
            </div>
            <p class="mt-15 mb-0">Don't have an account? <a href="<?=base_url('signup')?>" data-toggle="modal"
                  class="text-warning ml-5">Signup</a></p>

                  <p class="mt-15 mb-0 text-center"> <a href="javascrip:void(0)" onclick="forget_password_show()"
                  class="btn text-warning ml-5">Forget Password</a></p>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="forget_password" tabindex="-1" role="dialog" aria-labelledby="forget_passwordLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="forget_passwordLabel">Forget Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?=base_url('Users/send_otp')?>" method="POST" id="sendOTPForm">
				<div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">email:</label>
            <input type="email" class="form-control" name="email" id="email">
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script>
$("form#Frontlogin").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        //toastr.success(data.message);
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          if (data.user_type == 'Teacher') {
            location.href = "<?=base_url('home')?>";
          } else {
            location.href = "<?=base_url('home')?>";
          }

        }, 1000)

      } else if (data.status == 403) {
        //toastr.error(data.message);
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$("form#sendOTPForm").submit(function(e) {

$(':input[type="submit"]').prop('disabled', true);
e.preventDefault();
var formData = new FormData(this);
$.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function(data) {
    if (data.status == 200) {
      toastr.success(data.message);
      $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function() {
        location.href = "<?=base_url('password-change/')?>"+data.email;
      }, 1000)

    } else if (data.status == 403) {
      toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
    } else {
      toastr.error('Something went wrong');
      $(':input[type="submit"]').prop('disabled', false);
    }
  },
  error: function() {}
});
});

function forget_password_show(){
  $('#forget_password').modal('show');
}
</script>