
    <style>
     .daysClass {
    display: inline-block;
    line-height: 1;
    font-size: 14px;
    font-weight: 700;
    color: var(--white-color)!important;
    background-color: var(--theme-color);
    padding: 8px 15px;
    border-radius: 9999px;
}
    </style>

    <section class="course-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-12 col-xl-12">
            <div class="as-sort-bar mb-30">
              <div class="row justify-content-between align-items-center">
              
                <div class="col-md">
                  <p class="woocommerce-result-count"><strong class="text-title"><?=count($courses)?></strong> <?=$page_title?></p>
                </div>
           
              </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade active show" id="tab-grid" role="tabpanel" aria-labelledby="tab-shop-grid">
                <div class="row">
                <?php 
        $id = "";
          foreach($courses as $course){
            $id = $course->id;
            $earlier = new DateTime($course->start_date);
            $later = new DateTime($course->end_date);
            $totalDays =  $later->diff($earlier)->format("%a") +1;
            if($course->fees > 0 && $course->discount > 0){
                $price =$course->fees - ($course->discount*$course->fees)/100;
            }else{
                $price =$course->fees;
            }
            $this->db->where(array('course_rating.courseID' => $course->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;


            //   if($newwholeRating > 0){
            //   for($s=1;$s<=$newwholeRating;$s++){
            //     $stars_rating .= '<i class="icon-23"></i>';	
            //   }
            //   if($fractionRating >= 0.25){
            //     $stars_rating .= '<i class="icon-23 n50"></i>';	
            //   }
            // }
            //   else{
            //     for($s=1;$s<=5;$s++){
            //      $stars_rating .= '<i class="icon-23 text-secondary"></i>';
            //     }
            //   }  
        ?>
                  <div class="col-md-6 col-lg-6 col-xxl-3">
                    <div class="as-box as-box--style4">
                      <div class="as-box__img">
                        <img src="<?=base_url($course->image)?>" alt="course Image" class="w-100" style="width:332px;height:232px;">
                        <div class="as-box__category"><a href="<?=base_url('course-details/'.base64_encode($course->id))?>">₹ <?=$price?> / <del><?=$course->fees?></del></a></div>
                      </div>
                      <div class="as-box__middle">
                        <div class="as-box__author"><span class="mr-5 daysClass" ><a href="#"><?=$totalDays?> Days</a></span></div>
                        <div class="as-box__rating">
                        <?=$TotalratingAvg .'/'.count($allOverRating)?>
                          <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:<?=$newwholeRating * 20?>%"></span></div>
                        </div>
                      </div>
                      <h3 class="as-box__title"><a href="<?=base_url('course-details/'.base64_encode($course->id))?>"><?=$course->course?></a></h3>
                      <div class="as-box__bottom">
                        <div class="as-box__meta"><span><i class="fal fa-file"></i>T-Seats <?=$course->seats?></span> <span><i class="fal fa-file"></i>R-Seats <?=$course->remaning_seats?></span> <span><i class="fal fa-eye"></i>Mode : <?=$course->mode?></span></div>
                      </div>
                      <div class="as-box__bottom ">
                        <div class="text-center"><a href="<?=base_url('course-details/'.base64_encode($course->id))?>" class="as-btn  btn-sm "> Book Now</a></div>
                      </div>
                    </div>
                  </div>
    <?php } ?>
              
                </div>
              </div>
              <!-- <div class="tab-pane fade" id="tab-list" role="tabpanel" aria-labelledby="tab-shop-list">
                <div class="row">
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-1.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$336</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-1.png" alt="course author"> <a href="course.html">Kevin Perry</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Learn React JS Tutorial For Beginners</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 8</span> <span><i class="fal fa-user"></i>Students 78</span> <span><i class="fal fa-eye"></i>View: 12K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-2.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$445</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-2.jpg" alt="course author"> <a href="course.html">Justin Viber</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Learning Python for data visualization</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 5</span> <span><i class="fal fa-user"></i>Students 56</span> <span><i class="fal fa-eye"></i>View: 11K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-3.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$456</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-3.jpg" alt="course author"> <a href="course.html">Mark Joker</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Learn Programming from scratch to advance</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 3</span> <span><i class="fal fa-user"></i>Students 46</span> <span><i class="fal fa-eye"></i>View: 10K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-4.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$360</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-4.jpg" alt="course author"> <a href="course.html">Powel Fello</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Complete Web Design & Development Course</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 2</span> <span><i class="fal fa-user"></i>Students 49</span> <span><i class="fal fa-eye"></i>View: 09K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-5.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$120</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-5.jpg" alt="course author"> <a href="course.html">Marry Franco</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Microsoft Excel for Beginner to Expert</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 1</span> <span><i class="fal fa-user"></i>Students 96</span> <span><i class="fal fa-eye"></i>View: 06K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-6.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$123</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-6.jpg" alt="course author"> <a href="course.html">Jerzzy Lamot</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Learn Microsoft Excel 2013 - Advanced</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 3</span> <span><i class="fal fa-user"></i>Students 11</span> <span><i class="fal fa-eye"></i>View: 12K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-7.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$455</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-7.jpg" alt="course author"> <a href="course.html">Peter Mark</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">7 Steps To Excel Success and Power Tips</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 2</span> <span><i class="fal fa-user"></i>Students 23</span> <span><i class="fal fa-eye"></i>View: 11K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-8.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$996</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-8.jpg" alt="course author"> <a href="course.html">Rose Marry</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Real-Life Data Analysis and Experience</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 7</span> <span><i class="fal fa-user"></i>Students 56</span> <span><i class="fal fa-eye"></i>View: 10K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-9.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$336</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-4.jpg" alt="course author"> <a href="course.html">Vivi Marian</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Lorem ipsum is placeholder text commonly</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 1</span> <span><i class="fal fa-user"></i>Students 86</span> <span><i class="fal fa-eye"></i>View: 06K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-10.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$445</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-3.jpg" alt="course author"> <a href="course.html">Peter Parker</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">From its medieval origins to the digital era</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 3</span> <span><i class="fal fa-user"></i>Students 53</span> <span><i class="fal fa-eye"></i>View: 12K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-11.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$456</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-2.jpg" alt="course author"> <a href="course.html">Marry Rose</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">The passage experienced in popularity during</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 2</span> <span><i class="fal fa-user"></i>Students 82</span> <span><i class="fal fa-eye"></i>View: 11K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xxl-12">
                    <div class="as-box as-box--style8">
                      <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-12.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$360</a></div>
                      </div>
                      <div class="media-body">
                        <div class="as-box__middle">
                          <div class="as-box__author"><img src="assets/img/blog/author-1-1.png" alt="course author"> <a href="course.html">Kevin Poul</a></div>
                          <h3 class="as-box__title"><a href="course-details.html">Latin derived from Cicero's 1st century BC</a></h3>
                          <p class="as-box__text">Completely reinvent covalent e-tailers rather than front-end results. Enthusiastically reinvent interoperable sources through error-free architectures. Continually myocardinate.</p>
                          <div class="as-box__rating">
                            5.00
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                          </div>
                        </div>
                        <div class="as-box__bottom">
                          <div class="as-box__meta"><span><i class="fal fa-file"></i>Lesson 1</span> <span><i class="fal fa-user"></i>Students 96</span> <span><i class="fal fa-eye"></i>View: 10K</span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="as-pagination pt-20">
              <ul>
                <li><a href="blog.html">1</a></li>
                <li><a href="blog.html">2</a></li>
                <li><a href="blog.html">Next<i class="far fa-long-arrow-right"></i></a></li>
              </ul>
            </div>
          </div>
          <!-- <div class="col-lg-4 col-xl-3">
            <aside class="course-sidebar">
              <div class="widget widget_search">
                <form class="search-form"><input type="text" placeholder="Search Here"> <button type="submit"><i class="far fa-search"></i></button></form>
              </div>
              <div class="widget">
                <h3 class="widget_title">Category</h3>
                <div class="filter-list">
                  <ul>
                    <li>
                      <div><input type="checkbox" name="Beginner" id="Beginner" checked="checked"> <label for="Beginner">Beginner</label></div>
                      <ul class="children">
                        <li>
                          <div><input type="checkbox" name="Design" id="Design" checked="checked"> <label for="Design">Design</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Development" id="Development"> <label for="Development">Development</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Photography" id="Photography"> <label for="Photography">Photography</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Music" id="Music"> <label for="Music">Music</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Cooking" id="Cooking"> <label for="Cooking">Cooking</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Finance" id="Finance"> <label for="Finance">Finance</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Health" id="Health"> <label for="Health">Health</label></div>
                        </li>
                        <li>
                          <div><input type="checkbox" name="Technology" id="Technology"> <label for="Technology">Technology</label></div>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <div><input type="checkbox" name="Intermediate" id="Intermediate"> <label for="Intermediate">Intermediate</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="Expert" id="Expert"> <label for="Expert">Expert</label></div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h3 class="widget_title">Price Level</h3>
                <div class="filter-list">
                  <ul>
                    <li>
                      <div><input type="checkbox" name="Free6" id="Free6"> <label for="Free6">Free (6)</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="Paid12" id="Paid12" checked="checked"> <label for="Paid12">Paid (12)</label></div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h3 class="widget_title">Duration Time</h3>
                <div class="filter-list">
                  <ul>
                    <li>
                      <div><input type="checkbox" name="6hours15" id="6hours15" checked="checked"> <label for="6hours15">6+ hours (15)</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="10hours8" id="10hours8"> <label for="10hours8">10+ hours (8)</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="14hours3" id="14hours3"> <label for="14hours3">14+ hours (3)</label></div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h3 class="widget_title">Duration Time</h3>
                <div class="filter-list">
                  <ul>
                    <li>
                      <div><input type="checkbox" name="KoryAnderson" id="KoryAnderson" checked="checked"> <label for="KoryAnderson">Kory Anderson</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="LillianWalsh" id="LillianWalsh"> <label for="LillianWalsh">LillianWalsh</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="HenryLoselso" id="HenryLoselso"> <label for="HenryLoselso">Henry Loselso</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="KiaraDesuja" id="KiaraDesuja"> <label for="KiaraDesuja">Kiara Desuja</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="GonjalesOtamendi" id="GonjalesOtamendi"> <label for="GonjalesOtamendi">Gonjales Otamendi</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="ImonHossain" id="ImonHossain"> <label for="ImonHossain">Imon Hossain</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="KarlosAnjelina" id="KarlosAnjelina"> <label for="KarlosAnjelina">Karlos Anjelina</label></div>
                    </li>
                    <li>
                      <div><input type="checkbox" name="AlexJender" id="AlexJender"> <label for="AlexJender">Alex Jender</label></div>
                    </li>
                  </ul>
                </div>
              </div>
            </aside>
          </div> -->
        </div>
      </div>
    </section>
    