
    <section class="space">
      <div class="container">
        <div class="row gy-30">
          <div class="col-lg-5 col-xl-4">
            <div class="info-box">
              <h2 class="info-box__title h4">Contact Info</h2>
              <p class="info-box__label">Call Us:</p>
              <div class="info-box__media">
                <div class="info-box__icon"><i class="fal fa-phone"></i></div>
                <div class="media-body">
                  <p class="info-box__info"><a href="tel:+977-<?=$siteinfo->site_contact?>">+977-<?=$siteinfo->site_contact?></a></p>
                  <?php if(!empty($siteinfo->whatsapp_no)){?><p class="info-box__info"><a href="tel:+977-<?=$siteinfo->whatsapp_no?>">+977-<?=$siteinfo->whatsapp_no?></a></p><?php } ?>
                </div>
              </div>
              <p class="info-box__label">Email Us:</p>
              <div class="info-box__media">
                <div class="info-box__icon"><i class="fal fa-envelope"></i></div>
                <div class="media-body">
                  <p class="info-box__info"><a href="mailto:<?=$siteinfo->site_email?>"><?=$siteinfo->site_email?></a></p>
                  <!-- <p class="info-box__info"><a href="mailto:info@Aduki.com">info@Aduki.com</a></p> -->
                </div>
              </div>
              <p class="info-box__label">Location:</p>
              <div class="info-box__media">
                <div class="info-box__icon"><i class="fal fa-map-marker-alt"></i></div>
                <div class="media-body">
                  <p class="info-box__info"><?=$siteinfo->site_address?></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-7 col-xl-8">
            <div class="contact-map1">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d112658.59994594597!2d81.54704528363581!3d28.067807898280662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3998675a30f8e175%3A0x93c04013828c9891!2sNepalgunj%2C%20Nepal!5e0!3m2!1sen!2sin!4v1684407845600!5m2!1sen!2sin"></iframe></div>
          </div>
        </div>
      </div>
    </section>
    <section class="space-bottom">
      <div class="container">
        <div class="contact-wrap1">
          <div class="row align-items-center">
            <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 text-center"><img src="<?=base_url('public/website/assets/img/shape/contact-shape-1-1.png')?>" alt="Contact image shape"></div>
            <div class="col">
              <form id="enquirySend"  action="<?=base_url('home/user_enquiry')?>" method="POST" class=" form-style3">
                <h3 class="form-title">Quick Contact</h3>
                <div class="row gx-20">
                  <div class="form-group col-md-12">
                    <input type="text" name="name" id="name" placeholder="Enter Name"> <i class="fal fa-user"></i>
                </div>
                <div class="form-group col-md-12">
                    <input type="email" name="email" id="email" placeholder="Email Address"> <i class="fal fa-envelope"></i>
                </div>
                <div class="form-group col-md-12">
                    <input type="text" name="phone" id="phone" placeholder="Enter Mobile no." maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"> <i class="fal fa-mobile"></i>
                </div>
                <div class="form-group col-12">
                    <textarea name="message" id="message" cols="30" rows="3" placeholder="Message here"></textarea> <i class="fal fa-comment-alt-edit"></i>
                </div>
                <div class="form-btn col-12">
                    <button type="submit" class="as-btn style2">Submit Now<i class="fas fa-long-arrow-right ms-2"></i></button>
                </div>
                </div>
                <!-- <p class="form-messages mb-0 mt-3"></p> -->
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script>
         $("form#enquirySend").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.reload();
				}, 1000) 
		
				}else if(data.status==403) {
				toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});

        </script>