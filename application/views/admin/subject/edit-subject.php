<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="">
        <form action="<?=base_url('admin/subject/update')?>" id="editSubject" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course:</label>
                <select class="form-control" name="courseID" id="courseID">
                  <option value="">Select Course</option>
                  <?php foreach($courses as $course){?>
                  <option value="<?=$course->id?>" <?=$course->id==$subject->courseID ? 'selected' : '' ;?>><?=$course->course?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Subject Name:</label>
                <input type="text" class="form-control" name="subject" id="subject" value="<?=$subject->subject?>">
              </div>
            </div>
      
      
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <?php $modes = array('Online','Offline','Both');?>
                <label for="name" class="col-form-label">Mode:</label>
                <select class="form-control" name="mode" id="mode">
                 <option>Select Mode</option>
                 <?php foreach($modes as $mode){?>
                 <option value="<?=$mode?>" <?=$mode==$subject->mode ? 'selected' : '' ;?>><?=$mode?></option>
                 <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Seats:</label>
                <input type="text" class="form-control" name="seats" id="seats" value="<?=$subject->seats?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Start Date:</label>
                <input type="text" onfocus="(this.type='date')" class="form-control" min="<?= date('Y-m-d'); ?>" onchange="endDateValidate(this.value)" name="start_date" id="start_date" value="<?=$subject->start_date?>" placeholder="Start Date">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12" id="endDate_div">
              <div class="form-group">
                <label for="name" class="col-form-label">End Date:</label>
                <input type="text" class="form-control"  placeholder="End Date">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Price:</label>
                <input type="text" class="form-control" name="fees" id="fees" value="<?=$subject->fees?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Discount in %:</label>
                <input type="text" class="form-control" name="discount" id="discount" value="<?=$subject->discount?>"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Free Demo:</label>
                <input type="text" class="form-control" name="free_demo" id="free_demo" value="<?=$subject->free_demo?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Teacher:</label>
                <input type="text" class="form-control" name="teacher" id="teacher" value="<?=$subject->teacher?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Batch Timing:</label>
                <input type="text" class="form-control" placeholder="Batches" name="batch" id="batch" value="<?=$subject->batch?>">
                <!-- <select class="js-example-placeholder-multiple form-control"  name="batch[]" id="batch" multiple="multiple" placeholder="select Batch">
                  <?php //foreach($batchs as $batch){?>
                  <option value="<?//=$batch->id?>"><?//=$batch->batch .' '. $batch->time_formate?></option>
                  <?php //} ?>
                </select> -->
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Zoom Link:</label>
                <input type="text" class="form-control" name="zoom_link" id="zoom_link" value="<?=$subject->zoom_link?>">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Discription:</label>
                <textarea  class="form-control" name="description" id="description"><?=$subject->description?></textarea>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
              <div class="form-group">
                <label for="name" class="col-form-label">Course Image:</label>
                <input type="file" class="form-control" name="image" id="image">
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12">
                <img src="<?=base_url($subject->image)?>" style="width:100px;height:100px;">
              </div>
            </div>
   
            <!-- <div class="col-lg-12 col-md-12 col-sm-12">
              <?php //if($course->course_upload_pdf == ""){?>
              <div class="form-group">
                <label>Upload Course PDF</label>    
                <div class="input-group control-group after-add-more">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="course_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="course_upload_pdf[]" accept="application/pdf,application/vnd.ms-excel" class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
                </div>
              </div>
              <?php //} else{?>
               <hr>
               <label> 
                <div class="input-group control-group after-add-more mt-5">
                <div class="input-group-btn">   
                <b class="mr-2">Upload Course PDF</b>    <button class="btn btn-success add-more" type="button">Add More <i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
              </div>
              </label>
                  <hr>
                  <?php 

                  //  $pdfs =  json_decode($course->course_upload_pdf);
                  //  $count = 0;
                  //   foreach($pdfs as $key=>$pdf_pdf){
                      
                  //   foreach($pdf_pdf as $pdf_title=>$pdf){
                      
                  ?>
                     
                <div class="control-group input-group" style="margin-top:10px">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="course_pdf_title[]"  class="form-control" placeholder="Enter PDF Title" value="<?//=$pdf_title?>">  
                      <input type="hidden" name="course_pdf_title_hidden[]" value="<?//=$pdf_title?>">
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="course_upload_pdf[]"  class="form-control" placeholder="Enter Name Here" >
                      <input type="hidden" name="course_upload_pdf_hidden[<?//=$pdf_title?>]"   value="<?//=$pdf?>"> 
                      <p><a href="<?//= base_url('uploads/courses_pdf/'.$pdf)?>" target="_blank">Pdf Document</a></p> 
                    </div>
                  </div>
                  <div class="input-group-btn">   
                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                  </div>
                </div>
              <?php //$count++; } } } ?>
              <div class="form-group copy hide">
                <div class="control-group input-group" style="margin-top:10px">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="course_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="course_upload_pdf[]"  class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="input-group-btn">   
                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
  <!-- /.box -->          
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $("form#editSubject").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     formData.append("id", '<?php echo $subject->id?>');
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      window.location="<?=base_url('subjects')?>";
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   function endDateValidate(start_date){
      $('#endDate_div').html(' <div class="form-group"><label for="name" class="col-form-label">End Date:</label><input type="date" class="form-control" min="'+start_date+'" name="end_date" id="end_date" value="<?=$subject->end_date?>" placeholder="End Date"></div>');
    }
  
    $(document).ready(function() {  
  
  $(".add-more").click(function(){  
      var html = $(".copy").html();  
      $(".after-add-more").after(html);  
  });  
  
  $("body").on("click",".remove",function(){   
      $(this).parents(".control-group").remove();  
  });  
  endDateValidate(<?=$subject->start_date?>)
  }); 
</script>