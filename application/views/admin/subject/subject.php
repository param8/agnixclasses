 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-graduation-cap"></i> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  <div class="box">
				<div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="box-header with-border">
              <h3 class="box-title">All <?=$page_title?></h3>
             
             </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 ">
            <div class="box-header with-border float-right">
              
            <a href="<?=base_url('create-subject')?>" class="btn btn-primary btn-sm float-right" >Add <?=$page_title?> <i class="fa fa-plus"></i></a>
              
            </div>
         </div>
         </div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>Action</th>
								<th>Image</th>
                <th>Course</th>
								<th>Subject</th>
								<th>Mode</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Seats</th>
								<th>Fees</th>
								<th>Discount</th>
								<th>Discription</th>
								<th>Created Date</th>
								<th>Status</th>
								<!-- <th>Action</th> -->
							</tr>
						</thead>
						<tbody>
              <?php foreach($subjects as $key=>$subject){?>
							<tr>
								<td><?=$key+1;?></td>
                <td><a href="<?=base_url('edit-subject/'.base64_encode($subject->id))?>"><i class="fa fa-edit"></i></a></td>
								<td><img src="<?=base_url($subject->image)?>" width="100" height="70"></td>
                <td><?= $subject->course?></td>
								<td><?= $subject->subject?></td>
								
								<td><?= $subject->mode?></td>
								<td><?= date('d-m-Y',strtotime($subject->start_date))?></td>
								<td><?= date('d-m-Y',strtotime($subject->end_date))?></td>
								<td><?= $subject->seats?></td>
								<td><?= $subject->fees?></td>
								<td><?= $subject->discount?></td>
								<td><span title="<?=$subject->description?>" data-toggle="tooltip" style="cursor:pointer"><?= substr($subject->description,0,50)?></span></td>
                <td><?= date('d-m-Y',strtotime($subject->created_at));?></td>
								<td><a  href="javascript:void(0);"  <?= $subject->status==0 ? 'onclick="updateCourseStatus('.$subject->id.')" title="Click to Change Status"' : NULL;?>  data-toggle="tooltip" ><?= $subject->status == 1 ? '<span class="text-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
							</tr>
               <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>

  <div class="modal fade" id="addCoursetModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/courses/store')?>" id="addCourses" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="form-group">
        <label for="name" class="col-form-label">Category:</label>
        <select class="form-control" name="category" id="category">
          <option value="">Select Category</option>
          <?php foreach($categories as $category){?>
              <option value="<?=$category->id?>"><?=$category->category?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group">
          <label for="name" class="col-form-label">Course Name:</label>
          <input type="text" class="form-control" name="course" id="course">
        </div>
        <div class="form-group">
          <label for="name" class="col-form-label">Subject:</label>
          <input type="text" class="form-control" name="subject" id="subject">
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Free Demo:</label>
          <input type="text" class="form-control" name="free_demo" id="free_demo">
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Mode:</label>
          <input type="text" class="form-control" name="mode" id="mode">
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Start Date:</label>
          <input type="text" class="form-control" name="start_date" id="start_date">
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">End Date:</label>
          <input type="text" class="form-control" name="end_date" id="end_date">
        </div>

        <div class="form-group">
          <label for="name" class="col-form-label">Seats:</label>
          <input type="text" class="form-control" name="seats" id="seats">
        </div>

        
      <div class="form-group">
        <label for="name" class="col-form-label">Price:</label>
        <input type="text" class="form-control" name="fees" id="fees">
      </div>

      <div class="form-group">
        <label for="name" class="col-form-label">Batch:</label>
        <input type="text" class="form-control" name="batch" id="batch">
      </div>

      <div class="form-group">
        <label for="name" class="col-form-label">Discription:</label>
        <textarea  class="form-control" name="description" id="description"></textarea>
      </div>

      <div class="form-group">
        <label for="name" class="col-form-label">Course Image:</label>
        <input type="file" class="form-control" name="image" id="image">
      </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>

      
      </form>
    </div>
  </div>
</div>


<script>
function updateCourseStatus(user_id){
     var messageText  = "You want to Active this course?";
     var confirmText =  'Yes, Change it!';
     var message  ="Course status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/courses/update_status')?>', 
                method: 'POST',
                data: {userid: user_id},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }

  function viewModalShow(userid){
    $.ajax({
       url: '<?=base_url('admin/user/viewUser')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#viewUserModal').modal('show');
         $('#viewUserData').html(data);
       }
     });
  }
</script>