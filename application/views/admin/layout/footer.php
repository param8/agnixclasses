<footer class="main-footer no-print">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)">FAQ</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Purchase Now</a>
		  </li>
		</ul>
    </div>
	  &copy; 2020 <a href="#">Lakashya</a>. All Rights Reserved.
  </footer>
    <!-- Vendor JS -->
	<script src="<?=base_url('public/admin/js/vendors.min.js')?>"></script>
	<script src="<?=base_url('public/admin/js/pages/chat-popup.js')?>"></script>
    <script src="<?=base_url('public/admin/icons/feather-icons/feather.min.js')?>"></script>
    <script src="<?=base_url('public/admin/vendor_components/datatable/datatables.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/apexcharts-bundle/dist/apexcharts.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/moment/min/moment.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/fullcalendar/fullcalendar.j')?>'"></script>

	<script src="<?=base_url('public/admin/icons/feather-icons/feather.min.js')?>"></script>	
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-select/dist/js/bootstrap-select.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/select2/dist/js/select2.full.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_plugins/input-mask/jquery.inputmask.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/moment/min/moment.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_plugins/timepicker/bootstrap-timepicker.min.js')?>"></script>
	<script src="<?=base_url('public/admin/vendor_plugins/iCheck/icheck.min.js')?>"></script>
	
	<!-- EduAdmin App -->
	<script src="<?=base_url('public/admin/js/template.js')?>"></script>
	<script src="<?=base_url('public/admin/js/pages/advanced-form-element.js')?>"></script>
	<script src="<?=base_url('public/admin/js/pages/dashboard.js')?>"></script>
	<script src="<?=base_url('public/admin/js/pages/calendar.js')?>"></script>
    <script src="<?=base_url('public/admin/js/pages/data-table.js')?>"></script>
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

   <script>
     $(document).ready(function() {
      $(".js-example-placeholder-multiple").select2({
        placeholder: "Select a batch"
    });
    
    });
</script>
	
	
</body>
</html>