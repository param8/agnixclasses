
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
      <div class="row">
			  <!-- <div class="col-4">
          <label for="school" class="col-form-label">School:</label>
          <select class="form-control" name="school" id="school" onchange="setSchoolFilter(this.value)">
            <option value="">Select School</option>
            <?php //foreach($schools as $school){?>
              <option value="<?//=$school->id?>" <?//=$this->session->userdata('school')==$school->id ? 'selected' : ''; ?>><?//=$school->name?></option>
              <?php //} ?>
          </select>
          <p class="text-danger" style="cursor:pointer" onclick="resetSchoolFilter()">Reset Filter</p>
        </div> -->
        <div class="col-4">
          <label for="fromDate" class="col-form-label">From Date:</label>
          <input type="date" class="form-control" name="fromDate" id="fromDate" value="<?=$this->session->userdata('fromDate')?>" onchange="setFromDateFilter(this.value)">
          <p class="text-danger" style="cursor:pointer" onclick="resetFromDateFilter()">Reset Filter</p>
        </div>
        <div class="col-4">
          <label for="toDate" class="col-form-label">To Date:</label>
          <input type="date" class="form-control" name="toDate" id="toDate" value="<?=$this->session->userdata('toDate')?>" onchange="setToDateFilter(this.value)">  
          <p class="text-danger"  style="cursor:pointer" onclick="resetToDateFilter()">Reset Filter</p>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
		  <div class="row">
			  <div class="col-12">
				<div class="box">
				  <div class="box-body">
					<div class="table-responsive">
						<table id="productorder" class="table table-hover no-wrap product-order" data-page-size="10">
							<thead>
								<tr>
                   <th>#</th>
									 <th>Customer</th>
									 <th>Order ID</th>
									 <th>Quantity</th>
                   <th>Amount</th>
									 <th>Date</th>
									 <th>Delivery Status</th>
                   <th>Payment Status</th>
									 <th>Actions</th>
								</tr>
							</thead>
							<tbody>
                  <?php 
                    $i=1;
                    foreach($orders as $key=>$order){ ?>
					 <tr>
                       <td><?=$i;?></td>
					   <td><?= $order->name;?></td>
					   <td><?= $order->orderID;?></td>
					   <td><?= $order->qty;?></td>
                       <td><?= $order->price;?></td>
					   <td><?=!empty($order->created_at)?date('d-m-Y',strtotime($order->created_at)):'';?></td>
					   <td>
                      <?php if($order->status == '0')
                      { ?>
                          <span class="badge badge-pill badge-warning">Pending</span>
                      <?php
                      }elseif($order->status == '1'){
                      ?>
                          <span class="badge badge-pill badge-info">In-Progress</span>
                      <?php
                      }elseif($order->status == '2'){
                      ?>
                          <span class="badge badge-pill badge-success">Delivered</span>
                      <?php
                      }
                      ?>
                  </td>
                  <td>
                      <?= $order->payment_status == 'Paid' ? '<span class="text-success">'.$order->payment_status.'</span>' : '<span class="text-danger">'.$order->payment_status.'</span>';?></td>
					<td>
                      <a href="<?=base_url('order-invoice/'.base64_encode($order->id));?>" class="text-default mr-10" data-toggle="tooltip" data-original-title="View Invoice">
											<i class="ti-eye"></i>
										</a>
                      <a href="javascript:void(0)" class="text-info mr-10 btn-edit" data-orderid="<?= $order->id;?>" data-status="<?= $order->status;?>" data-toggle="tooltip" data-original-title="Update Status">
											<i class="ti-marker-alt"></i>
										</a> 
										<!-- <a href="javascript:void(0)" class="text-danger" data-original-title="Delete" data-toggle="tooltip">
											<i class="ti-trash"></i>
										</a> -->
									</td>
								</tr>
                    <?php $i++; } ?>
							</tbody>						
						</table>
					</div>
				  </div>
				</div>
			  </div>		  
		  </div>

		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- edit form modal -->
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Update Order Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="<?=base_url('admin/order/edit')?>" id="editOrder" method="POST">
        <div class="modal-body">
            <input type="hidden" name="order_id" class="order_id" value="">
            <div class="form-group">
                <label for="order_status" class="col-form-label">Status:</label>
                <select class="form-control order_status" name="order_status" id="order_status">
                  <option value="0">Pending</option>
                  <option value="1">In-Progress</option>
                  <option value="2">Delivered</option>
                </select>
            </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" name="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
        </div>
    </div>
</div>

  <script type="text/javascript">
   $('.btn-edit').on('click',function(){
        // get data from button edit
        const orderid = $(this).data('orderid');
        const status = $(this).data('status');
        // Set data to Form Edit
        $('.order_id').val(orderid);
        $('.order_status').val(status);
        // Call Modal Edit
        $('#editModal').modal('show');
    });

    $("form#editOrder").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to update status');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function deleteOrder(u_id){
      var action = "Delete";
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "<?=base_url('admin/order/delete/')?>", 
                method: 'POST',
                data: {u_id,action},
                // cache: false,
                // contentType: false,
                // processData: false,
                //dataType: 'json',
                success: function(result){
                  if(result==1) {
                  toastr.success("Order Deleted Successfully");
                  setTimeout(function(){
                      location.href="<?=base_url('orders')?>";
                  }, 1000) 
          
                }else{
                  toastr.error('Something went wrong');
                }
        }});
          
        }
        });
    }
    function setSchoolFilter(val){
      $.ajax({
       url: '<?=base_url("admin/order/setSchoolFilter")?>',
       type: 'POST',
       data: {val},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetSchoolFilter(){
      $.ajax({
       url: '<?=base_url("admin/order/resetSchoolFilter")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setFromDateFilter(val){
      $.ajax({
       url: '<?=base_url("admin/order/setFromDateFilter")?>',
       type: 'POST',
       data: {val},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetFromDateFilter(){
      $.ajax({
       url: '<?=base_url("admin/order/resetFromDateFilter")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setToDateFilter(val){
      $.ajax({
       url: '<?=base_url("admin/order/setToDateFilter")?>',
       type: 'POST',
       data: {val},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetToDateFilter(){
      $.ajax({
       url: '<?=base_url("admin/order/resetToDateFilter")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

</script>