<section class="space-bottom">
  <div class="container">
    <div class="contact-wrap1">
      <div class="row align-items-center">
        <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">

        </div>
        <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">
          <form action="<?=base_url('Users/update_password')?>" id="updatePasswordForm" class="form-style3">

            <h3 class="form-title text-center">Change Password</h3>
            <div class="row gx-20">
              <div class="form-group col-md-12"><input type="type" name="otp" id="otp" placeholder="Enter OTP">
                <i class="fal fa-envelope"></i></div>
              <div class="form-group col-md-12"><input type="password" name="new_password" id="new_password"
                  placeholder="Enter New Password"> <i class="fal fa-lock"></i></div>

                  <div class="form-group col-md-12"><input type="password" name="confirm_password" id="confirm_password"
                  placeholder="Enter Confirm Password"> <i class="fal fa-lock"></i></div>


              <div class="form-btn col-12 text-center">
                <button type="submit" class="as-btn style2">Submit<i
                    class="fas fa-long-arrow-right ms-2"></i></button></div>
            </div>
            
          </form>
        </div>
      </div>
    </div>
  </div>
</section>



<script>
$("form#updatePasswordForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("email", '<?=$email?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        setTimeout(function() {
          location.href = "<?=base_url('my-account')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error('Unable to add site info');
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>