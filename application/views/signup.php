
    <section class="space-bottom">
      <div class="container">
        <div class="contact-wrap1">
          <div class="row align-items-center">
            <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">
              
            </div>
            <div class="col-lg-5 col-xl-4 mb-40 mb-lg-0 ">
              <form action="<?=base_url('Authantication/register')?>" id="Frontregister" method="POST" class="form-style3">
                <h3 class="form-title text-center">Signup</h3>
                <div class="row gx-20">
                  <div class="form-group col-md-12">
                       <?php $user_types = array('Student'=>'Student','Teacher'=>'Teacher');?>
                        <select class="form-control" id="user_type" name="user_type" onchange="userTypeFields(this.value)">
                        <option value="">Select User Type</option>
                          <?php foreach($user_types as $key=>$userType){?>
                          <option value="<?=$key?>"><?=$userType?></option>
                          <?php } ?>
                        </select>
                   <i class="fal fa-user"></i>
                  </div>
                  <div class="form-group col-md-12">
                    <input type="text" name="name" id="name" placeholder="Enter Your Name"> <i class="fal fa-user"></i>
                  </div>
                  <div class="form-group col-md-12">
                    <input type="text" name="contact" id="contact" placeholder="Enter Your Contact No." maxlength="10" minlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"> <i class="fal fa-mobile-phone"></i>
                  </div>
                  <div class="form-group col-md-12">
                    <input type="email" name="email" id="email" placeholder="Enter Your Email Address"> <i class="fal fa-envelope"></i>
                  </div>
                  <div class="form-group col-md-12" id="subject_div" style="display:none">
                  <label> Select Subjects</label>
                        <select class="js-example-placeholder-multiple form-control" id="subjectID" name="subjectID[]"  multiple="multiple" placeholder="select Subject">
                        <option value="">Select Subject</option>
                          <?php foreach($subjects as $key=>$subject){?>
                          <option value="<?=$subject->id?>"><?=$subject->course.' ('.$subject->subject.')'?></option>
                          <?php } ?>
                        </select>
                   <!-- <i class="fal fa-book"></i>  -->
                  </div>
                  <div class="form-group col-md-12" id="ref_userID_div" style="display:none">
                    <input type="text" name="ref_userID" id="ref_userID" placeholder="Enter user referral id." autocomplete="off" > 
                    <!-- <i class="">AGNIX-</i> -->
                  </div>
                  <div class="form-group col-12">
                    <textarea name="address" id="address" cols="20" rows="3" placeholder="Enter Your Address"></textarea> <i class="fal fa-address-card"></i>
                  </div>
                   <div class="form-group col-md-12">
                    <input type="file" name="profile_pic" id="profile_pic" placeholder="Upload Profile PIc"> <i class="fal fa-picture-o"></i>
                  </div>

                  <div class="form-group col-md-12">
                    <input type="password" name="password" id="password" placeholder="Enter Your password"> <i class="fal fa-lock"></i>
                  </div>
                  
                  <div class="form-btn col-12 text-center">
                    <button type="submit" class="as-btn style2">Signup Now<i class="fas fa-long-arrow-right ms-2"></i></button></div>
                </div>
                <p class="mt-15 mb-0">if you have an account? <a href="<?=base_url('my-account')?>" data-toggle="modal"
                  class="text-warning ml-5">Signin</a></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>

     <script>
		$("form#Frontregister").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				  toastr.success(data.message);
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.href="<?=base_url('my-account')?>";
				}, 1000) 
		
				}else if(data.status==403) {
				//toastr.error(data.message);
        toastr.error(data.message);
				$(':input[type="submit"]').prop('disabled', false);
				}else{
          toastr.error(data.message);
				toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});

        function userTypeFields(user_type){
          //alert(user_type)
          if(user_type=='Teacher'){
            $('#subject_div').show();
            $('#ref_userID_div').hide();
          }else if(user_type=='Student'){
            $('#subject_div').hide();
            $('#ref_userID_div').show();
          }else{
            $('#subject_div').hide();
            $('#ref_userID_div').hide();
          }
        }
        </script>

   