<body>
  <div class="as-menu-wrapper">
    <div class="as-menu-area text-center">
      <button class="as-menu-toggle"><i class="fal fa-times"></i></button>
      <div class="mobile-logo"><a href="<?=base_url('home')?>"><img src="<?=base_url($siteinfo->site_logo)?>" alt="agnixclasses" style="width:100%;height:200px;"></a></div>
      <div class="as-mobile-menu">
        <ul>
          <?php if(empty($this->session->userdata('email'))){?>
          <li><a href="<?=base_url('my-account')?>" class="btn btn-primary btn-sm">Login/Register</a></li>
          <?php }else{ ?>
          <li><a href="<?=base_url('home')?>"><i class="fal fa-user"> </i> <?=$this->session->userdata('name') . ' ('. $this->session->userdata('user_type').')'?></a></li>
          <?php } ?>
          <li><a href="<?=base_url('home')?>"><i class="fal fa-home"> </i> Home</a></li>
          <?php if(!empty($this->session->userdata('email'))){?>
            <li><a href="<?=base_url('profile')?>" class=""><i class="fal fa-user-circle"> </i> My Profile</a></li>
          <?php } ?>
          <li><a href="<?=base_url('about-us')?>"><i class="fal fa-info-circle" aria-hidden="true"></i> About</a></li>
          <?php if($this->session->userdata('user_type')!='Doctor'){?>
          <li class="menu-item-has-children">
            <a href="<?=base_url('courses')?>"><i class="fal fa-book" aria-hidden="true"></i> Course</a>
            <ul class="sub-menu">
            <?php foreach($totalCourses as $totalCourse){?>
              <li><a href="<?=base_url('courses/'.base64_encode($totalCourse->id))?>"><?=$totalCourse->course?></a></li>
            <?php } ?>
            </ul>
          </li>
          <?php }if($this->session->userdata('user_type')!='Teacher'  OR $this->session->userdata('user_type')!='Doctor'){ ?>
            <li><a href="<?=base_url('store')?>"><i class="fas fa-store-alt"></i> Agnix Class Store</a></li>
          <?php } if($this->session->userdata('user_type')=='Student'){?>
            
            <li><a href="<?=base_url('my-books')?>"><i class="fas fa-book-alt"></i> My Books</a></li>
            <li><a href="<?=base_url('cart')?>" class=""><i class="fal fa-shopping-bag"> </i> My Cart<span class="badge"><?php echo $this->cart->total_items()?></span></a></li>
            <li><a href="<?=base_url('order')?>" class=""><i class="fal fa-shopping-bag"> </i> My Order</a></li>
            <li><a href="<?=base_url('doctor')?>"><i class="fal fa-user-md"> </i> Doctors</a></li>
            <li><a href="<?=base_url('refer-earn')?>"><i class="fal fa-share-alt"> </i> Referral & Earn</a></li>
            <li><a href="<?=base_url('booked-courses')?>"><i class="fal fa-share-alt"> </i> Booked Course</a></li>
          <?php } ?>
					
          <li><a href="<?=base_url('contact-us')?>"><i class="fal fa-mobile"> </i> Contact Us</a></li>
          <?php if(!empty($this->session->userdata('email'))){?>
            <li><a href="<?=base_url('Authantication/chat')?>"><i class="fal fa-comment"> </i> Chat</a></li>
          <li><a href="<?=base_url('authantication/logout')?>" class="btn btn-primary">Logout</a></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>
  <div class="sidemenu-wrapper d-none d-lg-block">
    <div class="sidemenu-content">
      <button class="closeButton sideMenuCls"><i class="far fa-times"></i></button>
      <div class="widget woocommerce widget_shopping_cart">
        <h3 class="widget_title">Shopping cart</h3>
        <div class="widget_shopping_cart_content">
          <ul class="woocommerce-mini-cart cart_list product_list_widget">
						<?php 
								$cartItems=$this->cart->contents();                              //print_r($cartItems);
								foreach($cartItems as $item){
							?>
            <li class="woocommerce-mini-cart-item mini_cart_item"><a href="javascript:void(0)" class="remove remove_from_cart_button" onclick="removeCartItem('<?=$item['rowid']?>')"><i class="far fa-times"></i></a> 
						 <a href=""><img src="<?= base_url($item['image'])?>" alt="Cart Image"><?= $item['name']?></a> <span class="quantity">qty <?= $item['qty']?> *  <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">रु </span><?= $item['price']?></span></span>
					 </li>
					 <?php } ?>
          </ul>
          <p class="woocommerce-mini-cart__total total"><strong>Subtotal:</strong> <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">रु </span><?=$this->cart->total()?></span></p>
          <?php if(!empty($cartItems)){?>
          <p class="woocommerce-mini-cart__buttons buttons"><a href="<?=base_url('cart')?>" class="as-btn style1 wc-forward">View cart</a> 
					
					<a href="<?=base_url('checkout')?>" class="as-btn style1 checkout wc-forward">Checkout</a>
					<?php } ?>
				</p>
				
        </div>
      </div>
    </div>
  </div>
  <header class="as-header header-layout6">
    <div class="header-top">
      <div class="top-shape"></div>
      <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-between align-items-center">
          <div class="col-auto">
            <p class="header-notice">Welcome to our <?=$siteinfo->site_name?></p>
            <div class="header-top-links style-white">
              <ul>
                <li><i class="fal fa-phone"></i> Phone: <a href="tel:<?=$siteinfo->site_contact?>">+977-<?=$siteinfo->site_contact?></a></li>
                <li class="d-none d-md-inline-block"><i class="fal fa-envelope"></i> Email: <a href="mailto:<?=$siteinfo->site_email?>"><?=$siteinfo->site_email?></a></li>
              </ul>
            </div>
          </div>
          <div class="col-auto d-none d-lg-block">
            <div class="header-social style-white">
              <a href="<?=!empty($siteinfo->facebook_url) ? $siteinfo->facebook_url : 'javascript:void(0)';?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
              <a href="<?=!empty($siteinfo->twitter_url) ? $siteinfo->twitter_url : 'javascript:void(0)';?>" target="_blank"><i class="fab fa-twitter"></i></a> 
              <a href="<?=!empty($siteinfo->insta_url) ? $siteinfo->insta_url : 'javascript:void(0)';?>" target="_blank"><i class="fab fa-instagram"></i></a>
              <a href="<?=!empty($siteinfo->linkedin_url) ? $siteinfo->linkedin_url : 'javascript:void(0)';?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="sticky-wrapper">
      <div class="sticky-active">
        <div class="container">
          <div class="menu-area">
            <div class="row align-items-center justify-content-between">
              <div class="col-auto">
                <div class="header-logo py-3 py-lg-0"><a href="<?=base_url('home')?>"><img src="<?=base_url($siteinfo->site_logo)?>" alt="agnixclasses"></a></div>
              </div>
              <div class="col-auto ms-auto">
                <nav class="main-menu d-none d-lg-block">
                  <ul>
                    <li><a href="<?=base_url('home')?>">Home</a></li>
                    <li><a href="<?=base_url('about-us')?>">About</a></li>
                    <?php if($this->session->userdata('user_type')=='Student' OR $this->session->userdata('user_type')=='Teacher'){?>
                    <li class="menu-item-has-children">
                      <a href="<?=base_url('courses')?>">Course</a>
                      <ul class="sub-menu">
                        <?php foreach($totalCourses as $totalCourse){?>
                         <li><a href="<?=base_url('courses/'.base64_encode($totalCourse->id))?>"><?=$totalCourse->course?></a></li>
                         <?php } ?>
                      </ul>
                    </li>
                    <?php }  if($this->session->userdata('user_type')!='Teacher' OR $this->session->userdata('user_type')!='Doctor'){?>
                      <li><a href="<?=base_url('store')?>">Store</a></li>
                      <?php } if($this->session->userdata('user_type')=='Student'){?>
                      <li><a href="<?=base_url('booked-courses')?>">Booked Course</a></li>
                      <li><a href="<?=base_url('my-books')?>">My Books</a></li>
                     
										  <li><a href="<?=base_url('doctor')?>">Doctors</a></li>
                      <li><a href="<?=base_url('refer-earn')?>"> Referral & Earn</a></li>
                    <?php } ?>
                    <li><a href="<?=base_url('contact-us')?>">Contact Us</a></li>
                  </ul>
                  
                </nav>
                <button class="as-menu-toggle d-inline-block d-lg-none"><i class="fal fa-bars"></i></button>
              </div>
              <?php if($this->session->userdata('email')){?>
              <div class="col-auto d-none d-xl-block ms-auto p_auto">
                <div class="header-icons">
                 
                  <a href="<?=base_url('Authantication/chat')?>" class="has-badge" title="Chats" target="_blank"><i class="fal fa-comments"></i></a>
						
                </div>
              </div>
              <?php } if($this->session->userdata('user_type')=='Student'){?>
              <div class="col-auto d-none d-xl-block ma-auto p_auto">
                <div class="header-icons">
                  
                  <a href="javascrip:void(0)" class="has-badge sideMenuToggler" title="My Cart"><i class="fal fa-shopping-bag"></i>
                  <span class="badge"><?php echo $this->cart->total_items()?></span></a>
						
                </div>
              </div>
              <?php } ?>
     
							<?php if(empty($this->session->userdata('email'))){?>
              <div class="col-auto d-none d-xxl-block">
                <div class="header-btn"><a href="<?=base_url('my-account')?>" class="as-btn style2">Login / Sign Up<i class="fas fa-long-arrow-right ms-2"></i></a></div>
              </div>
							<?php } else{?>
                <div class="dropdownuser col-auto d-none d-xxl-block p_auto">
                <div class="header-icons"> <!-- <a href="<?//=base_url('doctor')?>"><i class="fal fa-user-md" title="Doctor"></i></a>  -->
                  <a href="javascript:void(0);" class="has-badge "><img src="<?=base_url($this->session->userdata('profile_pic'))?>"  style="width:100px;height:40px;border-radius:100%; ">
                  <span class="badge green"><i class="fal fa-dot"></i></span>
                  </a>
					     	</div>
                <div class="dropdown-content">
                <a href="javascript:void(0);" class="text-primary"><i class="fal fa-user"></i> <b><?=$this->session->userdata('name')?> </b></a>
                <hr>
                <a href="<?= base_url('order'); ?>"><i class="fal fa-shopping-bag"></i> My Order </a>
                <hr>
                    <!-- <div>
                      <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 24 24" width="20px" fill="#000000">
                        <path d="M0 0h24v24H0V0z" fill="none"/>
                        <path d="M15.55 13c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.37-.66-.11-1.48-.87-1.48H5.21l-.94-2H1v2h2l3.6 7.59-1.35 2.44C4.52 15.37 5.48 17 7 17h12v-2H7l1.1-2h7.45zM6.16 6h12.15l-2.76 5H8.53L6.16 6zM7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zm10 0c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"/>
                      </svg>
                      <span class="ms-2">My Order</span>
                    </div> -->
                  <!-- </a> -->
                <a href="<?=base_url('profile')?>">Profile</a>
                <hr>
                <a href="<?=base_url('Authantication/logout')?>">Log Out</a>
                </div>
              </div>
                <?php }?>
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="logo-bg"></div>
  </header>