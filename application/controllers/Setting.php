<?php
class Setting extends MY_Controller 
{
    public function __construct()
    {
      parent::__construct();
     $this->load->model('setting_model');
    }

    public function site_setting()
    {  	$data['page_title'] = 'Site Info';
        $data['siteinfo'] = $this->siteinfo();
        $data['siteinfo'] = $this->Common_model->get_site_info();
        $this->admin_template('setting/site_setting',$data);
    }

    public function store_siteInfo(){
       $site_name = $this->input->post('site_name');
       $site_contact = $this->input->post('site_contact');
       $whatsapp_no = $this->input->post('whatsapp_no');
       $site_email = $this->input->post('site_email');
       $site_address = $this->input->post('site_address');
       $facebook_url = $this->input->post('facebook_url');
       $youtube_url = $this->input->post('youtube_url');
       $linkedin_url = $this->input->post('linkedin_url');
       $twitter_url = $this->input->post('twitter_url');
       $insta_url = $this->input->post('insta_url');
       $footer_contant = $this->input->post('footer_contant');
       $discription = $this->input->post('discription');
       $siteinfo = $this->Common_model->get_site_info();
      
       if(empty($site_name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($site_contact)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(empty($site_email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(strlen((string)$site_contact) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }

       if(empty($discription)){
        echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
        exit();
       }

       if(empty($footer_contant)){
        echo json_encode(['status'=>403, 'message'=>'Please enter footer contant']); 	
        exit();
       }
     $this->load->library('upload');
    if(!empty($_FILES['site_logo']['name'])){
     $config = array(
      'upload_path' 	=> 'uploads/siteInfo',
      'file_name' 	=> str_replace(' ','',$site_name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
     );
     $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('site_logo'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]); 	
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['site_logo']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($siteinfo->site_logo)){
      $image = $siteinfo->site_logo;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }
      $data = array(
       'site_name' => $site_name,
       'site_email' => $site_email,
       'site_contact' => $site_contact,
       'whatsapp_no' => $whatsapp_no,
       'site_address' => $site_address,
       'discription' => $discription,
       'footer_contant' => $footer_contant,
       'site_logo' => $image,
       'facebook_url' => $facebook_url,
       'youtube_url' => $youtube_url,
       'linkedin_url' => $linkedin_url,
       'twitter_url' => $twitter_url,
       'insta_url' => $insta_url,
      
      );

      $update = $this->setting_model->update_siteInfo($data);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }

    public function setSession(){
      $course = $this->input->post('course');
      $mode = $this->input->post('mode');
      $start_date = $this->input->post('start_date');
      $location_state = $this->input->post('location_state');
      $location_city = $this->input->post('location_city');
  
  
      $session = array(
       'course' => $course,
       'mode' => $mode,
       'start_date' => $start_date,
       'location_state' => $location_state,
       'location_city' => $location_city,
      );
      $this->session->set_userdata($session);
      //echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }
  
    public function resetCourse(){
      $this->session->unset_userdata('course');
    }
  
    public function resetMode(){
      $this->session->unset_userdata('mode');
    }

    public function resetStartDate(){
      $this->session->unset_userdata('start_date');
    }

    public function resetState(){
      $this->session->unset_userdata('location_state');
      $this->session->unset_userdata('location_city');
    }

    public function resetCity(){
      $this->session->unset_userdata('location_city');
    }

    public function setBookedCourses(){
      $course = $this->input->post('courses_booked');
   
      $this->session->set_userdata('courses_booked',$this->input->post('courses_booked'));
    }

    public function resetCourseBooked(){
      $this->session->unset_userdata('courses_booked');
    }

    public function checkCategory(){
      $category = $this->input->post('category');
      $category = $this->category_model->get_category(array('category'=>$category));
      if($category){
        echo json_encode(['status'=>403, 'message'=>'Category already exits!']);
       }else{
        echo json_encode(['status'=>200, 'message'=>'']);   
      }
    }

    public function sendNotification(){
      $courseID = $this->input->post('courses');
      $message = $this->input->post('message');
      if(empty($courseID)){
        echo json_encode(['status'=>403, 'message'=>'Please select course']); 	
        exit();
       }

       if(empty($message)){
        echo json_encode(['status'=>403, 'message'=>'Please enter message']); 	
        exit();
       }
   
      $students =  $this->course_model->get_student_booked_courses(array('courseID'=>$courseID));
      if(count($students)){
      foreach($students as $student){
        $notification = array(
          'userID' => $student->userID,
          'send_by' => $this->session->userdata('id'),
          'message' => $message
        );
        $store_notification = $this->notification_model->store_notification($notification);
      }

      echo json_encode(['status'=>200, 'message'=>'Send Notification Successfully']);
    }else{
      echo json_encode(['status'=>403, 'message'=>'Students not booked this course']);
    }

    }

    public function about()
    {	
        $data['page_title'] = 'About us';
        $data['siteinfo'] = $this->siteinfo();
        $data['about_us'] = $this->Common_model->get_about();
        $this->admin_template('about_us',$data);
    }

    public function store_about_us(){
      $title = $this->input->post('title');
      $description = $this->input->post('desc');
      $short_description = $this->input->post('short_description');
      $aboutus_image = $this->input->post('aboutus_image');

      if(empty($title)){
       echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
       exit();
      }
      if(empty($description)){
       echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
       exit();
      }

      if(empty($short_description)){
        echo json_encode(['status'=>403, 'message'=>'Please enter short description']); 	
        exit();
       }

       if(strlen($short_description) >= 200){
        echo json_encode(['status'=>403, 'message'=>'Please enter short description 200 characters']); 	
        exit();
       }
      
      $this->load->library('upload');
      if(!empty($_FILES['image']['name'])){
        $name = $_FILES['image']['name'];
        $config = array(
        'upload_path' 	=> 'uploads/siteInfo',
        'file_name' 	=> uniqid().str_replace(' ','_',$name),
        'allowed_types' => 'doc|jpg|jpeg|png|pdf|PDF|txt',
        'max_size' 		=> '10000000',
        );
        $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $image = 'uploads/siteInfo/'.$config['file_name'];
        }
      }elseif(!empty($aboutus_image)){
        $image = $aboutus_image;
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
     $data = array(
      'title' => $title,
      'description' => $description,
      'short_description' => $short_description,
      'image' => $image
     );

     $update = $this->Common_model->store_aboutus($data);
     if($update){
      echo json_encode(['status'=>200, 'message'=>'About us content updated successfully!']); 	
      exit();
     }
   }

   public function enquiry()
   {
      $data['page_title'] = 'Enquiry';
      $data['siteinfo'] = $this->siteinfo();
      $data['enquiries'] = $this->Common_model->get_all_enquiry();
      $this->admin_template('enquiry',$data);
     
   }

}

?>