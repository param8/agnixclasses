<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('user_model');
		$this->load->model('Order_model');
		//$this->not_logged_in();
	
	
	}

	public function index(){
		$data['page_title'] = 'Home';
		// $conditionCourse = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
		// $condition = array('users.status'=>1);
		// $conditionBooked = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('booked_courses.studentID'=>$this->session->userdata('id')):array('booked_courses.status'=>1)) ;
		// $studentsCondition = array('users.user_type'=>'Student','users.status'=>1);
		// $instituteCondition = array('users.user_type'=>'Institute','users.status'=>1);
		$condition = array('courses.user_type'=='Teacher','courses.status'=>1);
		//$data['totalCourses'] = $this->course_model->get_courses($condition);
		//$data['courses'] = $this->course_model->get_courses($condition);
		// $data['users'] = $this->user_model->get_all_users($condition);
		// $data['totalStudents'] = $this->user_model->get_all_users($studentsCondition);
		// $data['totalInstitute'] = $this->user_model->get_all_users($instituteCondition);
		// $data['ratings'] = $this->course_model->get_rating_home();
		// $data['booked_courses'] = $this->course_model->get_home_booked_course($conditionBooked);
		 $data['books'] = $this->Order_model->get_all_items();
		 $data['ratings'] = $this->Subject_model->get_rating_home();
		// $data['count_categories'] =$this->category_model->get_categories_with_counter(array('category.status'=>1));
		if($this->session->userdata('user_type')=='Student'){
			$data['student'] = $this->user_model->get_student(array('studentID'=>$this->session->userdata('id')));
		  }
		$this->home_website_template('home',$data);
	}

	public function institute_home(){
		$data['page_title'] = 'Home';
		$conditionCourse = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
		$condition = array('users.status'=>1);
		$conditionBooked = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('booked_courses.studentID'=>$this->session->userdata('id')):array('booked_courses.status'=>1)) ;
		$studentsCondition = array('users.user_type'=>'Student','users.status'=>1);
		$instituteCondition = array('users.user_type'=>'Institute','users.status'=>1);
		$data['totalCourses'] = $this->course_model->get_courses($conditionCourse);
		$data['courses'] = $this->course_model->get_courses_home($conditionCourse);
		$data['users'] = $this->user_model->get_all_users($condition);
		$data['totalStudents'] = $this->user_model->get_all_users($studentsCondition);
		$data['totalInstitute'] = $this->user_model->get_all_users($instituteCondition);
		$data['ratings'] = $this->course_model->get_rating_home();
		$data['booked_courses'] = $this->course_model->get_home_booked_course($conditionBooked);
		$this->website_template('institute_home',$data);
	}

    public function contact_us(){
		$data['page_title'] = 'Contact US';
		$this->website_template('contact',$data);
	}

	public function about_us(){
		$data['page_title'] = 'About US';
		$data['about'] = $this->aboutinfo();
		$data['corses_count'] = $this->Subject_model->get_courses_count(array('status'=> 1));
		$data['totalDoctors'] = $this->user_model->get_all_users(array('user_type'=>'Doctor'));
		$data['totalStudents'] = $this->user_model->get_all_users(array('user_type'=>'Student'));
		$data['books'] = $this->Order_model->get_all_items();
		$data['ratings'] = $this->Subject_model->get_rating_home();
		$this->website_template('about',$data);
	}

	public function user_enquiry()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$contact = $this->input->post('phone');
		$message= $this->input->post('message');
	
		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			exit();
		}
		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
		if(empty($message)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}
		
		$data = array(
			'name'        => $name,
			'email'       => $email,
			'phone'       => $contact,
			'message'     => $message ,
		);
		$register = $this->user_model->stor_enquiry($data);
	
		if($register){
			echo json_encode(['status'=>200, 'message'=>'Enquiry submit successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Enquiry submission fail!']);   
		}
	}
	
   public function createCategory(){
	$data['page_title'] = 'Create Category';
	$this->website_template('courses/create-category',$data);
   }



	
}