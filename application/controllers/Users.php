<?php 
class Users extends MY_Controller 
{
	public function __construct()
	{
	  parent::__construct();
    
    $this->load->model('course_model');
    $this->load->model('user_model');
    $this->load->model('setting_model');
    $this->load->helper('mail_helper');
	}

	public function index()
  {
    $this->not_logged_in();
    $data['page_title'] = 'Profile';
    $data['user'] = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id')));
    $data['states'] = $this->Common_model->get_states(array('country_id'=>154));
    if($this->session->userdata('user_type')=='Student'){
      $data['student'] = $this->user_model->get_student(array('studentID'=>$this->session->userdata('id')));
    }
		$this->website_template('user/profile',$data);
  }

  public function update(){
    $this->not_logged_in();
    $id= $this->session->userdata('id');
    $user_type= $this->session->userdata('user_type');
    $name = $this->input->post('name');
		$address= $this->input->post('address');
	

    $user = $this->user_model->get_user(array('users.id'=>$id));

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}

   
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}


    $this->load->library('upload');
    if($_FILES['profile_pic']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif|webp',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
     }elseif(!empty($user->profile_pic)){
        $image = $user->profile_pic;
     }
    else{
      $image = 'public/website/assets/dummy_image.jpg';
    }
		$data = array(
			'name'        => $name,
			'address'     => $address,
      'profile_pic' => $image,
		);
		$update = $this->user_model->update_user_status($data,array('id'=>$id));
    if($user_type=='Institute'){
      $discription = $this->input->post('discription');
      $facebook_url= $this->input->post('facebook_url');
      $insta_url = $this->input->post('insta_url');
      $twitter_url = $this->input->post('twitter_url');
      $linkedin_url= $this->input->post('linkedin_url');
      $youtube_url= $this->input->post('youtube_url');
      $data = array(
        'site_name'   => $name,
        'discription' => $discription,
        'facebook_url'=> $facebook_url ,
        'insta_url'   => $insta_url ,
        'twitter_url' => $twitter_url,
        'linkedin_url'=> $linkedin_url,
        'youtube_url' => $youtube_url,
      );
      $this->setting_model->update_siteInfo($data);
    }

    if($user_type=='Student'){
      $dob = $this->input->post('dob');
      $whatsappNo = $this->input->post('whatsappNo');
      $current_domain = $this->input->post('current_domain');
      $school_collage_name = $this->input->post('school_collage_name');
      $interested_free_demo = $this->input->post('interested_free_demo');
      $free_demo_class_mode = $this->input->post('free_demo_class_mode');
      $target_exam = $this->input->post('target_exam');
      $target_exam_other = $this->input->post('target_exam_other');
      $skill = $this->input->post('skill');
      $scholarship = $this->input->post('scholarship');

      if(empty($dob)){
        echo json_encode(['status'=>403, 'message'=>'Please enter date of birth']); 	
        exit();
      } 
      if(empty($whatsappNo)){
        echo json_encode(['status'=>403, 'message'=>'Please enter your whatsapp number']); 	
        exit();
      }

      if(empty($current_domain)){
        echo json_encode(['status'=>403, 'message'=>'Please select current domain']); 	
        exit();
      } 
      
      if($current_domain=='Other'){
        if(empty($school_collage_name)){
          echo json_encode(['status'=>403, 'message'=>'Please Enter other domain name']); 	
          exit();
        } 
      }

      if(empty($interested_free_demo)){
        echo json_encode(['status'=>403, 'message'=>'Please Select a free demo']); 	
        exit();
      }

      if(empty($free_demo_class_mode)){
        echo json_encode(['status'=>403, 'message'=>'Please free demo class mode']);  	
        exit();
      } 
      if(empty($target_exam)){
        echo json_encode(['status'=>403, 'message'=>'Please select a target exam']);  	
        exit();
      }

      if($target_exam == 'Other'){
        if(empty($target_exam_other)){
          echo json_encode(['status'=>403, 'message'=>'Please target from other exam']); 	
          exit();
        }
      } 

      if(empty($skill)){
        echo json_encode(['status'=>403, 'message'=>'Please Mention Subject/Course/Skills']); 	
        exit();
      } 
      if(empty($scholarship)){
        echo json_encode(['status'=>403, 'message'=>'Please select a scholarship']); 	
        exit();
      }

      $studentData =array(
        'dob' => $dob,
        'whatsappNo' => $whatsappNo,
        'current_domain' => $current_domain,
        'school_collage_name' => $school_collage_name,
        'interested_free_demo' => $interested_free_demo,
        'free_demo_class_mode' => $free_demo_class_mode,
        'target_exam' => $target_exam,
        'target_exam_other' => $target_exam_other,
        'skill' => $skill,
        'scholarship' => $scholarship,
        'status' => 1,
      );
   
      $this->user_model->update_student($studentData,array('studentID'=>$id));

    }

    $session = array(
       'name' =>$name,
       'profile_pic' =>$image,
    );
    $this->session->set_userdata($session);
		if($update){
   
			echo json_encode(['status'=>200, 'message'=>'Update Profile  successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Something went wrong']);   
		}
  }

public function doctor(){
  $this->not_logged_in();
  $data['page_title'] = 'Doctors';
  $data['doctors'] = $this->user_model->get_doctor(array('user_type'=>'Doctor'));
  $this->website_template('user/doctor',$data);
}


public function ref_registration(){
  $this->not_logged_in();
  $data['page_title'] = 'Referral Registration';
  $data['ref_id'] = base64_decode($this->uri->segment(2));
 $this->website_template('ref-registration',$data);
}

public function send_otp(){
  $email = $this->input->post('email');
  $user = $this->user_model->get_user(array('users.email' => $email,'users.user_type<>'=>'Admin','users.status'=>1));
  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=>'Please enter email']); 	
    exit();
  }
  if($user){
    $otp = rand(11111,999999);
    $html = "Dear ".$user->name.',<br><br> Your reset password OTP is <b>'.$otp .'</b><br> Thanks.. <br> Agnixclasses';
    $subject = 'Reset Password ';
    $sendmail = sendEmail($email,$subject,$html);
    if($sendmail){
      $this->session->set_userdata('otp',$otp);
      echo json_encode(['status'=>200, 'message'=>'Reset OTP send your email please check your email address','email'=>base64_encode($email)]); 	
       exit();
    }else{
      echo json_encode(['status'=>403, 'message'=>'Mail failed']); 	
    exit();
    }
  }else{
    echo json_encode(['status'=>403, 'message'=>'Please enter valid email']); 	
    exit();
  }

}


public function change_password(){
  $data['page_title'] = 'Change Password';
  $data['email'] = base64_decode($this->uri->segment(2));
  $data['subjects'] = $this->Subject_model->get_subjects(array('subjects.status'=>1));
  $this->website_template('password-change',$data);
  
}

public function update_password(){
  $this->session->userdata('otp');
  $email = $this->input->post('email');
  $otp = $this->input->post('otp');
  $new_password = $this->input->post('new_password');
  $confirm_password = $this->input->post('confirm_password');
  $user = $this->user_model->get_user(array('users.email' => $email,'users.user_type'=>'Admin'));
  if(empty($otp)){
    echo json_encode(['status'=>403, 'message'=>'Please enter OTP']); 	
    exit();
  }
   
  if(empty($new_password)){
    echo json_encode(['status'=>403, 'message'=>'Please enter new password']); 	
    exit();
  }

  if(empty($confirm_password)){
    echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
    exit();
  }

  if($otp!=$this->session->userdata('otp')){
    echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
    exit();
  }

  if($confirm_password!=$new_password){
    echo json_encode(['status'=>403, 'message'=>'Please enter new password or  confirm password is not match']);  	
    exit();
  }
    $data = array(
     'password' => md5($new_password),
    );
    $update =  $this->user_model->update_user_status($data,array('email'=>$email));
    if($update){
      $this->session->unset_userdata('otp');
      echo json_encode(['status'=>200, 'message'=>'New password has been changed']);  	
       exit();
    }else{
      echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
    exit();
    }

}

}