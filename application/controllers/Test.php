<?php


  $payment_method = $this->input->post('payment_type');
  $address = $this->input->post('address');
  $totalQty = $this->cart->total_items();
  $totalPrice = $this->cart->total();

  if(empty($totalQty)){
    echo json_encode(['status'=>403, 'message'=>'Please add book in cart']);
    exit();
  }
 $orderID = 'AGNIX'.rand(111111111, 9999999999);
  $data= array(
    'orderID' =>$orderID,
    'uid' =>$this->session->userdata('id'),
    'price' =>$totalPrice,
    'qty' =>$totalQty,
    'address' =>$address,
    'payment_method' =>$payment_method
  );
    
  $storeOrder = $this->Order_model->storeOrder($data);
  if($storeOrder){
    $cartItems=$this->cart->contents();
    foreach($cartItems as $items){
      $Orderitems = array(
        'orderID' => $storeOrder,
        'productID' =>$items['id'],
        'name' =>$items['name'],
        'price' =>$items['price'],
        'qty' =>$items['qty'],
       );
       $storeItemOrder = $this->Order_model->storeOrderItems($Orderitems);
    }


    $curl = curl_init();
    $header_data = array(
      'Authorization: Key live_secret_key_a1615657b2ea400f88aef1b14bdc10b3',
      'Content-Type: application/json',
      //'Cookie: f5avraaaaaaaaaaaaaaaa_session_=KAMGDKGJCICNEHFDEAEMCPLKGHEBBHNJLGHPEGCEEFOJFGOAEHKCCDKABOOMHGHBPLLDAKPCODKDNBPHKHAAIDNOIFFKJIPILCCPIFDMKBFPIDMEEABGDLHEJGBAMJKE; TS01df5cb2=016ebf701634bd8eb0276eb171a851186654cdddcd94c754b6f5c52fe77c03f7171e3e9e9cf454d315837d5fa2575de1041a7a519c3e862864f42836b0c408ba9d798deffd5dce35e4590f08fb9683d21a700e5d10; TS01df5cb2028=0123ed50729d300c8e7eba6c1a7e7b7a4030e8ffa37b53ba5c1b50a54a2ca0a79bfe420dbb9d5c3c7592de4798bbb9e8c1323b27db; sessioncookie=!qRpauekFOLFcXpDAlc9WKnhD4wT63xJ6v2RV1Qpoc8x+4IbZrez5ANGlbvVe4eqcxdvY7YEy6MjtCA=='
    );
   $data_json =  array(
      CURLOPT_URL => 'https://khalti.com/api/v2/epayment/initiate/',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{
      "return_url": "https://example.com/payment/",
      "website_url": "https://example.com/",
      "amount": '.$totalPrice.',
      "purchase_order_id": '.$orderID.',
      "purchase_order_name": "Product",
      "customer_info": {
          "name": '.$this->session->userdata('name').',
          "email": '.$this->session->userdata('email').',
          "phone": '.$this->session->userdata('contact').'
      },
      "amount_breakdown": [
          {
              "label": "Mark Price",
              "amount": 1000
          },
          {
              "label": "VAT",
              "amount": 300
          }
      ],
      "product_details": [
          {
              "identity": "1234567890",
              "name": "Khalti logo",
              "total_price": '.$totalPrice.',
              "quantity": 1,
       "unit_price": '.$totalPrice.'
          }
      ]
    }',
      CURLOPT_HTTPHEADER => $header_data,
    );
  
    curl_setopt_array($curl,$data_json);
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    echo $response;
    // $this->cart->destroy();
    // echo json_encode(['status'=>200, 'message'=>'Thanks for place order!']);
    
    // exit();
  
  }else{
    echo json_encode(['status'=>403, 'message'=>'somethings went wrong']);
    exit();
  }

 


?>