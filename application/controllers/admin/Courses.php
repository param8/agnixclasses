<?php 
class Courses extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    $this->not_admin_logged_in();
		$this->load->model('course_model');
    
	}

	public function index()
	{	
		$data['page_title'] = 'Courses';
		$data['courses'] = $this->course_model->get_courses(array('status'=>1));
    $this->admin_template('courses/course',$data);
	}

	public function store(){
		$course = $this->input->post('course');

    if(empty($course)){
      echo json_encode(['status'=>403,'message'=>'Please enter course']);
      exit();
    }

    $courseCheck= $this->course_model->get_course(array('course' =>$course));
    if($courseCheck){
      echo json_encode(['status'=>403,'message'=>'Course already exists']);
      exit();
    }
    $data = array(
      'course'  => $course,
    );

    $store = $this->course_model->store_course($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Add course successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function editForm(){
    $id = base64_decode($this->input->post('id'));
    $course = $this->course_model->get_course(array('id'=>$id));
    ?>
          <div class="form-group">
            <label for="name" class="col-form-label">Course:</label>
            <input type="text" class="form-control" name="course" id="course" value="<?=$course->course ?>">
          </div>
       
          <input type="hidden" name="id" id="id" value="<?=$id?>">
   
    <?php
  }

  public function update(){
    $id = $this->input->post('id');
    $course = $this->input->post('course');
    $courseCheck= $this->course_model->get_course(array('course' =>$course, 'id <>' =>$id));
		
		if(empty($course)){
		  echo json_encode(['status'=>403, 'message'=>'Please enter a course']);
      exit();
		}

    if($courseCheck){
      echo json_encode(['status'=>403,'message'=>'Course already exists']);
      exit();
    }

    $data = array(
      'course' => $course,
    );
  
    $update = $this->course_model->update_course($data,array('id'=>$id));
    if($update){
      echo json_encode(['status'=>200, 'message'=>'Update course successfully...']);
    }else{
      echo json_encode(['status'=>302, 'message'=>mysqli_error()]);
    }
    
	}

  public function delete(){
    $id = base64_decode($this->input->post('id'));
    $status = $this->input->post('status');
    $data =  array(
      'status' => $status
    );
    $update = $this->course_model->update_course($data,array('id'=>$id));
    
    if($update){
      echo true;
    }


  }


	
}