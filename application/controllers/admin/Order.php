<?php 
class Order extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
        $this->load->model('Order_model');
				$this->load->model('course_model');
        $this->load->model('Common_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Book Items';
	    $data['items'] = $this->Order_model->get_all_items();
		$this->admin_template('book_items',$data);
	}

	public function store_items(){
		$name = $this->input->post('name');
		$price = $this->input->post('price');
		$discription = $this->input->post('discription');
    if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Enter book name!']);
      exit();
    }

    if(empty($price)){
      echo json_encode(['status'=>403, 'message'=>'Enter book price!']);
      exit();
    }

    if(empty($discription)){
      echo json_encode(['status'=>403, 'message'=>'Enter book description!']);
      exit();
    }

		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
		{
		$config = array(
		  'upload_path' 	=> 'uploads/items',
		  'file_name' 	=> date('dmYHis').uniqid(),
		  'allowed_types' => 'jpg|jpeg|png|gif',
		  'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
		  {
			  $error = $this->upload->display_errors();
			  echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
		  }
		  else
		  {
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/items/'.$config['file_name'].'.'.$type;
		  }
		}else{
		  $image = 'public/dummy_image.jpg';
		}
  
    $data = array(
    'name' => $name,
    'image' => $image,
    'price' => $price,
    'discription' => $discription
    );

    $store = $this->Order_model->store_items($data);
    if($store)
		{
			echo json_encode(['status'=>200, 'message'=>'Item add successfully!']);
      exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>show_error()]);
      exit();
		}

	}

  public function editForm(){
    $id = base64_decode($this->input->post('id'));
    $item = $this->Order_model->get_item(array('id'=>$id));
    ?>
    <div class="form-group">
      <label for="name" class="col-form-label">Title:</label>
      <input type="text" class="form-control" name="name" id="name" value="<?=$item->name?>">
      </div>

      <div class="form-group">
      <label for="name" class="col-form-label">Price:</label>
      <input type="text" class="form-control" name="price" id="price" value="<?=$item->price?>">
      </div>

      <div class="form-group">
      <label for="name" class="col-form-label">Description:</label>
      <textarea type="text" class="form-control" name="discription" id="discription"><?=$item->discription?></textarea>
      </div>

      
      <div class="form-group">
      <label for="name" class="col-form-label">Item Image:</label>
      <input type="file" class="form-control" name="image" id="image" accept="image/png, image/gif, image/jpeg, image/jpg" />
      </div>
      <div class="form-group">
        <img src="<?=base_url($item->image)?>" style="width:100px;height:100px;">
      </div>
      <input type="hidden" name="id" id="id" value="<?=$item->id?>" />
    <?php
  }

  public function update(){
    $id = $this->input->post('id');
    $name = $this->input->post('name');
		$price = $this->input->post('price');
		$discription = $this->input->post('discription');
    $item = $this->Order_model->get_item(array('id'=>$id));
    if(empty($name)){
      echo json_encode(['status'=>403, 'message'=>'Enter book name!']);
      exit();
    }

    if(empty($price)){
      echo json_encode(['status'=>403, 'message'=>'Enter book price!']);
      exit();
    }

    if(empty($discription)){
      echo json_encode(['status'=>403, 'message'=>'Enter book description!']);
      exit();
    }

		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
		{
		$config = array(
		  'upload_path' 	=> 'uploads/items',
		  'file_name' 	=> date('dmYHis').uniqid(),
		  'allowed_types' => 'jpg|jpeg|png|gif',
		  'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
		  {
			  $error = $this->upload->display_errors();
			  echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
		  }
		  else
		  {
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/items/'.$config['file_name'].'.'.$type;
		  }
		}elseif(!empty($item->image)){
      $image = $item->image;
    }else{
		  $image = 'public/dummy_image.jpg';
		}
  
    $data = array(
    'name' => $name,
    'image' => $image,
    'price' => $price,
    'discription' => $discription
    );

    $update = $this->Order_model->update_items($data,array('id'=>$id));
    if($update)
		{
			echo json_encode(['status'=>200, 'message'=>'Item update successfully!']);
      exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>show_error()]);
      exit();
		}
  }

  public function delete(){
    $id = base64_decode($this->input->post('id'));
    $status = $this->input->post('status');
    $data =  array(
      'status' => $status
    );
    $update = $this->Order_model->update_items($data,array('id'=>$id));
    
    if($update){
      echo true;
    }
  }

  public function orders()
	{	
		$data['page_title'] = 'Orders';
		$data['orders'] = $this->Order_model->get_all_orders(array());
		$this->admin_template('order',$data);
	}

	public function booked_course()
	{	
		$data['page_title'] = 'Booked Courses';
		$data['orders'] = $this->course_model->get_student_booked_course(array('booked_courses.status'=>1));
		$this->admin_template('booked-course',$data);
	}



    public function view_invoice()
    {
        $order_id = base64_decode($this->uri->segment(2));
        $data['page_title'] = 'Orders';
        $data['order_details'] = $this->Order_model->get_order_details(array('orders.id'=>$order_id));
		    $data['order_items'] = $this->Order_model->get_order_items(array('item_order.orderID'=>$order_id));
        // //echo $data['order_details']->address;
        // $data['shipping_details'] = $this->Order_model->get_shipping_details(array('id'=>$data['order_details']->address));
        // print_r($data['shipping_details']);die;
		    $this->admin_template('view_invoice',$data);
    }

	public function edit()
    {
        $order_id       = $this->input->post('order_id');
        $order_status  = $this->input->post('order_status');

        if(empty($order_status)){
            echo json_encode(['status'=>403, 'message'=>'Invalid Request']);
                exit();
        }

        $data = array(
			'status' => $order_status,	
			//'modify_date' 	=> date('Y-m-d H:i:s')
		);

    $update = $this->Order_model->update_order($data, $order_id);
		if($update)
		{
			echo json_encode(['status'=>200, 'message'=>'Order status updated successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'Record not updated. please try again!']);
      		exit();
		}

    }



	public function setSchoolFilter(){
		$school = $this->input->post('val');
		$this->session->set_userdata('school', $school);
	}

	public function resetSchoolFilter(){
		$this->session->unset_userdata('school');
	}

	public function setFromDateFilter(){
		$fromDate = $this->input->post('val');
		$this->session->set_userdata('fromDate', $fromDate);
	}

	public function resetFromDateFilter(){
		$this->session->unset_userdata('fromDate');
	}

	public function setToDateFilter(){
		$toDate = $this->input->post('val');
		$this->session->set_userdata('toDate', $toDate);
	}

	public function resetToDateFilter(){
		$this->session->unset_userdata('toDate');
	}
		
}