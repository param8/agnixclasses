<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		
		$this->load->model('user_model');
		$this->load->model('Auth_model');
		$this->load->model('setting_model');
		$this->load->helper('mail_helper');
	}

	public function index()
	{	
		$this->not_admin_logged_in();
		$data['page_title'] = 'Teachers';
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Teacher'));
		$this->admin_template('users',$data);
	}

	public function students()
	{
		$this->not_admin_logged_in();
		$data['page_title'] = 'Students';
    $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Student'));
		$this->admin_template('users',$data);
	}

	public function doctors()
	{	
		$this->not_admin_logged_in();
		$data['page_title'] = 'Doctors';
    $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Doctor'));
		$this->admin_template('doctors',$data);
	}

	public function store_doctor(){
		$this->not_admin_logged_in();
		$siteinfo=$this->siteinfo();
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('contact');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type = 'Doctor';

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter doctor name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter doctor email address']); 	
			exit();
		}
		$checkEmail = $this->user_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}


		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter doctor mobile']); 	
			exit();
		}

		$checkContact = $this->user_model->get_user(array('contact'=>$contact));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This mobile no. is already in use']);
			exit();
		}

		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter doctor address']); 	
			exit();
		}

		$this->load->library('upload');
		if($_FILES['profile_pic']['name'] != '')
			{
		$config = array(
		  'upload_path' 	=> 'uploads/users',
		  'file_name' 	=> str_replace(' ','',$name).uniqid(),
		  'allowed_types' => 'jpg|jpeg|png|gif',
		  'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('profile_pic'))
		  {
			  $error = $this->upload->display_errors();
			  echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
		  }
		  else
		  {
			$type = explode('.',$_FILES['profile_pic']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/users/'.$config['file_name'].'.'.$type;
		  }
		}else{
		  $image = 'public/dummy_image.jpg';
		}
    
	    $ran_id = rand(time(), 100000000);
		$data = array(
			'unique_id'   => $ran_id,
			'name'        => $name,
			'email'       => $email,
			'password'    => md5($password),
			'contact'     => $contact ,
			'address'     => $address,
			'user_type'   => $user_type,
            'profile_pic' => $image,
			'status' =>1,
			'login_status' =>'Offline now',
			'otp' =>0,
		);
		$register = $this->Auth_model->register($data);

		if($register){
			$doctorData = array(
				'adminID'        => $register,
				'site_name'      => $name,
				'site_email'     => $email,
				'site_contact'   => $contact ,
				'site_address'   => $address ,
				'site_logo'      => $siteinfo->site_logo,
				'footer_contant' => $siteinfo->footer_contant,
			  );
			  $this->setting_model->store_siteInfo($doctorData);
			echo json_encode(['status'=>200, 'message'=>'Doctor register successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
		}
	}

	public function update_status()
	{
		$this->not_admin_logged_in();
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';

        $userdata =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
       if($user_status==1){
          $message = "Admin approved you";
          $data = array(
            'userID' => $user_id,
            'send_by' => $this->session->userdata('id'),
            'message' => $message
          );
          $store_notification = $this->notification_model->store_notification($data);
        }
	}

	public function viewUser(){
		$this->not_admin_logged_in();
		$userid = $this->input->post('userid');
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
		?>
			<div class="box mb-0">
				<div class="box-body box-profile">            
					<div class="row">
						<div class="col-12">
							<h4 class="box-inverse p-2">Personal Information</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=$user->contact;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=$user->address;?> </span></p>
								<p><strong>State</strong> :<span class="text-gray pl-10"><?=$user->stateName;?></span></p>
								<p><strong>City</strong> :<span class="text-gray pl-10"><?=$user->cityName;?></span></p>
							</div>
							
							
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		<?php
	  }

	public function send_otp(){
		$email = $this->input->post('email');
		$user = $this->user_model->get_user(array('users.email' => $email,'users.user_type'=>'Admin'));
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter email']); 	
			exit();
		}
		if($user){
      $otp = rand(11111,999999);
			$html = "Dear ".$user->name.',<br><br> Your reset password OTP is <b>'.$otp .'</b><br> Thanks.. <br> Agnixclasses';
			$subject = 'Reset Password ';
			$sendmail = sendEmail($email,$subject,$html);
			if($sendmail){
				$this->session->set_userdata('otp',$otp);
				echo json_encode(['status'=>200, 'message'=>'Reset OTP send your email please check your email address','email'=>base64_encode($email)]); 	
		   	exit();
			}else{
				echo json_encode(['status'=>403, 'message'=>'Mail failed']); 	
			exit();
			}
		}else{
			echo json_encode(['status'=>403, 'message'=>'Please enter valid email']); 	
			exit();
		}

	}


	public function change_password(){
		$data['page_title'] = 'Change Password';
		$data['siteinfo'] = $this->siteinfo();
		$data['email'] = base64_decode($this->uri->segment(2));
		$this->load->view('admin/layout/login_head',$data);
		$this->load->view('admin/change-password');
		$this->load->view('admin/layout/login_footer');
	}

	public function update_password(){
		$this->session->userdata('otp');
		$email = $this->input->post('email');
		$otp = $this->input->post('otp');
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');
		$user = $this->user_model->get_user(array('users.email' => $email,'users.user_type'=>'Admin'));
		if(empty($otp)){
			echo json_encode(['status'=>403, 'message'=>'Please enter OTP']); 	
			exit();
		}
     
		if(empty($new_password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter new password']); 	
			exit();
		}

		if(empty($confirm_password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
			exit();
		}

		if($otp!=$this->session->userdata('otp')){
			echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
			exit();
		}

		if($confirm_password!=$new_password){
			echo json_encode(['status'=>403, 'message'=>'Please enter new password or  confirm password is not match']);  	
			exit();
		}
      $data = array(
       'password' => md5($new_password),
			);
			$update =  $this->user_model->update_user_status($data,array('email'=>$email));
			if($update){
				$this->session->unset_userdata('otp');
				echo json_encode(['status'=>200, 'message'=>'New password has been changed']);  	
		   	exit();
			}else{
				echo json_encode(['status'=>403, 'message'=>'Something went wrong']); 	
			exit();
			}

	}
	
}