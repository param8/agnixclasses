<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;
$route['my-account'] = 'Authantication/index';
$route['signup'] = 'Authantication/signup';
$route['admin'] = 'Authantication/adminIndex';
$route['institute-home'] = 'home/institute_home';
//$route['create-course'] = 'Courses/createCourse';
$route['create-category'] = 'home/createCategory';
$route['courses/(:any)'] = 'Courses/index/$1';
$route['courseStore'] = 'Courses/store';
$route['store'] = 'Items/index';
$route['doctor'] = 'users/doctor';
$route['my-books'] = 'Courses/my_books';
$route['study-material/(:any)'] = 'Courses/study_material/$1';
$route['course-search'] = 'Courses/setSession';
$route['course-details/(:any)'] = 'Courses/course_detail/$1';
$route['teachers'] = 'Teachers/index';
$route['booked-courses'] = 'Courses/booked_course';
$route['create-teacher'] = 'Teachers/createTeacher';
$route['course-details/(:any)'] = 'Courses/course_detail/$1';
$route['site-info'] = 'setting/site_setting';
$route['profile'] = 'Users';
$route['contact-us'] = 'Home/contact_us';
$route['about-us'] = 'Home/about_us';
$route['notification'] = 'Notification/index';
$route['cart'] = 'Cart/index';
$route['checkout'] = 'Cart/checkout';
$route['responce'] = 'Cart/responce';
$route['refer-earn'] = 'Notification/refer_earn';
$route['redeem-now'] = 'Notification/redeem_now';
$route['ref/(:any)'] = 'Authantication/ref_registration/$1';
$route['password-change/(:any)'] = 'Users/change_password/$1';
// Admin Routs

$route['dashboard'] = 'admin/dashboard';
$route['institutes'] = 'admin/user';
$route['books-items'] = 'admin/Order/index';
$route['students'] = 'admin/user/students';
$route['experts'] = 'admin/user/experts';
$route['courses-list'] = 'admin/courses';
$route['subjects'] = 'admin/subject';
$route['create-subject'] = 'admin/subject/create_subject';
$route['edit-subject/(:any)'] = 'admin/subject/edit_subject/$1';
$route['approved-courses'] = 'admin/courses/approved_courses';
$route['category'] = 'admin/Category';
$route['doctors'] = 'admin/User/doctors';
$route['about'] = 'admin/setting/about';
$route['orders'] = 'admin/order/orders';
$route['booked-course'] = 'admin/order/booked_course';
$route['order-invoice/(:any)'] = 'admin/order/view_invoice/$1';
$route['enquiry-list'] = 'admin/setting/enquiry';
$route['reward-setting'] = 'admin/setting/reward_setting';
$route['change-password/(:any)'] = 'admin/User/change_password/$1';