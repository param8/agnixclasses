<?php 
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function get_site_info(){
    //$adminID = $this->session->userdata('user_type')=='Institute' ? $this->session->userdata('id') : 1;
    $this->db->where('adminID', 1);
    return $this->db->get('site_info')->row();
  }

  public function get_about(){
   return $this->db->get('about_us')->row();
  }
  
  public function get_states()
  {
      return $this->db->get('states')->result();

  }

  public function get_cities()
  {
      return $this->db->get('cities')->result();
  }

  public function get_state_wise_city($stateID){
    $this->db->where('state_id',$stateID);
    return $this->db->get('cities')->result();
}

public function get_batch()
{
    return $this->db->get('batch')->result();
}

public function store_aboutus($data){
  $this->db->where('id',1);
  return $this->db->update('about_us',$data);
}

public function get_all_enquiry(){
  return $this->db->get('contact_us')->result();
}

}
