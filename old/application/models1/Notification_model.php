<?php 
class Notification_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_notifications($condition){
    
    $this->db->where($condition);
    return $this->db->get('notification')->result();
  }

  public function store_notification($data){
    return $this->db->insert('notification',$data);
  }

  public function update_notification($data,$id){
    $this->db->where('userID',$id);
    return $this->db->update('notification',$data);
  }

}