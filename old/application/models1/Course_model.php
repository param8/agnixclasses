<?php 
class Course_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_courses($condition){
    $this->db->select('*');
    $this->db->where($condition);
    return $this->db->get('courses')->result();
  }
  
  public function get_course($condition){
    $this->db->where($condition);
    return $this->db->get('courses')->row();
  }



  // public function get_course_with_subject_counter($condition){ 

  //   if($this->session->userdata('user_type')=='Teacher'){
  //     $this->db->select('subjects.*,courses.course');
  //     $this->db->from('subjects');
  //     $this->db->join('courses', 'courses.id = subjects.courseID','left');
  //     $this->db->where($condition);
  //     $this->db->where_in('subjects.id',explode(',',$this->session->userdata('subjectID')));
  //     $this->db->group_by('subjects.courseID'); 
  //     return $this->db->get()->result();
  //     foreach($query as $key => $value)
  //     {
  //       $this->db->select('count(id) as totalCourses');
  //       $this->db->where('categoryID', $value['id']);
  //       $query[$key]['course_count'] = $this->db->get('courses')->row_array();
  //     }  
  //     }else{
  //       $this->db->where($condition);
  //       return $this->db->get('courses')->result();
  //       foreach($query as $key => $value)
  //     {
  //       $this->db->select('count(id) as totalCourses');
  //       $this->db->where('categoryID', $value['id']);
  //       $query[$key]['course_count'] = $this->db->get('courses')->row_array();
  //     }  
  //     }

    // $this->db->select('category.*,users.name');
    // $this->db->join('users', 'users.id = category.userID','left');
    // $this->db->where($condition);
    // $query = $this->db->get('category')->result_array();
    // foreach($query as $key => $value)
    // {
    //    $this->db->select('count(id) as totalCourses');
    //    $this->db->where('categoryID', $value['id']);
    //   $query[$key]['course_count'] = $this->db->get('courses')->row_array();
    // }  
    //  return $query;

   //}


  public function store_course($data){
     $this->db->insert('courses', $data);
     return $this->db->insert_id();
  }

  public function update_course($data,$condition){
    $this->db->where($condition);
    return $this->db->update('courses',$data);
  }


}