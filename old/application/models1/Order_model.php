<?php 

class Order_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
		$this->load->library('encryption');
	}

	public function get_all_items(){
		$this->db->order_by('id','desc');
		return $this->db->get('items')->result();
	}

	public function get_item($condition){
     $this->db->where($condition);
	 return $this->db->get('items')->row();
	}
   
	public function store_items($data){
		return $this->db->insert('items',$data);
	}

	public function update_items($data,$condition){
		$this->db->where($condition);
		return $this->db->update('items',$data);
	}

	public function get_shipping_address(){
        $this->db->select('shipping_details.*');
        $this->db->from('shipping_details');
        //$this->db->where('shipping_details.status',1);
        $this->db->where('shipping_details.uid',$this->session->userdata('id'));
		$this->db->order_by('shipping_details.id','desc');
        return $this->db->get()->result();
      }

	  public function get_order_items($condition){
		$this->db->select('item_order.*,items.image');
		$this->db->join('items','items.id=item_order.productID','left');
		$this->db->where($condition);
		return $this->db->get('item_order')->result_array();
	  }
	  
      public function store_shippingAddress($data){
        return $this->db->insert('shipping_details',$data);
      }

	  public function storeOrder($data){
		$this->db->insert('orders',$data);
		return $this->db->insert_id();
	  }

	  public function storeOrderItems($Orderitems){
		return $this->db->insert('item_order',$Orderitems);
	  }
    
	public function get_all_orders($condition)
	{
		$this->db->select('orders.*, users.name,users.email as userEmail,users.contact as userConatct,shipping_details.name as shipping_name,
		shipping_details.mobile as shipping_contact, shipping_details.email as shipping_email, shipping_details.address as shipping_address,shipping_details.street as shipping_street,
		shipping_details.pin_code as shipping_pin_code, shipping_details.city as shipping_city, shipping_details.state as shipping_state'); 
		$this->db->join('users', 'orders.uid = users.id','left');
		$this->db->join('shipping_details', 'shipping_details.id = orders.address','left');
		$this->db->where($condition);
		if($this->session->userdata('fromDate')){
			$this->db->where('DATE_FORMAT(orders.created_at,"%d-%m-%Y")>=',date('d-m-Y',strtotime($this->session->userdata('fromDate'))));
			if($this->session->userdata('toDate')){
				$this->db->where('DATE_FORMAT(orders.created_at,"%d-%m-%Y")<=',date('d-m-Y',strtotime($this->session->userdata('toDate'))));
			}
		}	
	    $this->db->order_by('orders.id','desc');
		return $this->db->get('orders')->result();	
		//echo $this->db->last_query();die;
	}



    public function get_order_details($condition)
	{
		$this->db->select('orders.*, users.name as userName,users.email as userEmail,users.contact as userConatct,shipping_details.name as shipping_name,
		shipping_details.mobile as shipping_contact, shipping_details.email as shipping_email, shipping_details.address as shipping_address,shipping_details.street as shipping_street,
		shipping_details.pin_code as shipping_pin_code, shipping_details.city as shipping_city, shipping_details.state as shipping_state'); 
		$this->db->join('users', 'orders.uid = users.id','left');
		$this->db->join('shipping_details', 'shipping_details.id = orders.address','left');
		$this->db->where($condition);	
	    $this->db->order_by('orders.id','desc');
        return $this->db->get('orders')->row();	
	}
	
	// public function get_order_items($wh)
	// {
	// 	$this->db->select('order_items.*, book_set.*, school_details.name as school_name, school_class.class as school_class');
	// 	$this->db->join('book_set', 'order_items.book_set_id = book_set.id');
	// 	$this->db->join('school_details', 'book_set.s_id = school_details.id'); 
	// 	$this->db->join('school_class', 'book_set.sc_id = school_class.id'); 
	// 	$this->db->where($wh);
	//     $this->db->order_by('order_items.id','asc');
	// 	return $this->db->get('order_items')->result();
	// }
	
	public function get_shipping_details($wh)
	{
		$this->db->select('*'); 
		$this->db->where($wh);
	    return $this->db->get('shipping_details')->row();
	}

	public function update_order($data, $order_id)
	{
		$this->db->where('id', $order_id);
        return $this->db->update('orders', $data);
	}
	
}