<?php 
class Subject_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function get_subjects($condition){  
    $this->db->select('subjects.*,courses.course');
    $this->db->from('subjects');
    $this->db->join('courses', 'courses.id = subjects.courseID','left');

    $this->db->where($condition);
    if($this->session->userdata('course')){
        $this->db->like('course', $this->session->userdata('course'));
    }
    if($this->session->userdata('mode')){
        $this->db->where('mode', $this->session->userdata('mode'));
    }
    if($this->session->userdata('start_date')){
        $this->db->where('start_date', $this->session->userdata('start_date'));
    }
    $this->db->order_by('subjects.id','desc');
    //$this->db->limit($limit, $start);
    return $this->db->get()->result();
    // echo $this->db->last_query();die;
}

public function get_subject($condition){
  $this->db->select('subjects.*,courses.course');
  $this->db->from('subjects');
  $this->db->join('courses', 'courses.id = subjects.courseID','left');
  $this->db->where($condition);
   return $this->db->get()->row();
   //echo $this->db->last_query();
}

public function get_subject_courses($condition){

  if($this->session->userdata('user_type')=='Teacher'){
  $this->db->select('subjects.*,courses.course');
  $this->db->from('subjects');
  $this->db->join('courses', 'courses.id = subjects.courseID','left');
  $this->db->where($condition);
  $this->db->where_in('subjects.id',explode(',',$this->session->userdata('subjectID')));
  $this->db->group_by('subjects.courseID'); 
  return $this->db->get()->result();
  }else{
    $this->db->where($condition);
    return $this->db->get('courses')->result();
  }
  
 
}

public function get_teacher_courses($condition){  
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,subjects.subject as subject_name');
  $this->db->from('subjects');
  $this->db->join('courses', 'courses.id = subjects.courseID','left');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users', 'users.id = courses.userID','left');
  $this->db->where($condition);
  if($this->session->userdata('course')){
      $this->db->like('course', $this->session->userdata('course'));
  }
  if($this->session->userdata('mode')){
      $this->db->where('mode', $this->session->userdata('mode'));
  }
  if($this->session->userdata('start_date')){
      $this->db->where('start_date', $this->session->userdata('start_date'));
  }
  $this->db->order_by('courses.id','desc');
  //$this->db->limit($limit, $start);
  return $this->db->get()->result();
}


public function get_courses_header($condition){
  $this->db->select('courses.*,category.category ,users.name as user_name,users.email as user_email,cities.city as city_name');
  $this->db->from('courses');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users', 'users.id = courses.userID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->where($condition);
  $this->db->order_by('courses.id','desc');
  return $this->db->get()->result();
}

public function get_courses_count($condition){
           $this->db->where($condition);
    return $this->db->count_all("courses");
}

// public function load_more_courses($condition){
//     $this->db->select('courses.*,category.category ,users.name as user_name,cities.city as city_name');
//     $this->db->from('courses');
//     $this->db->join('category', 'category.id = courses.categoryID','left');
//     $this->db->join('users', 'users.id = courses.userID','left');
//     $this->db->join('cities', 'cities.id = courses.locationID','left');
//     $this->db->where($condition); 
//     $this->db->limit(2);
//     return $this->db->get()->result();
// }



  public function get_more_courses($condition){
    $this->db->select('courses.*,category.category ,users.name as user_name,cities.city as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->order_by('rand()');
    $this->db->limit(3);
    return $this->db->get()->result();
}



public function get_courses_home($condition,$page_type = NULL){  
    $this->db->select('courses.*,category.category ,users.name as user_name,cities.city as city_name');
    $this->db->from('courses');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users', 'users.id = courses.userID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->where($condition);
    $this->db->limit(4);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
}

  public function store_subject($data){
    return $this->db->insert('subjects',$data);
  }

 public function store_rating($data){
    return $this->db->insert('course_rating',$data);
 }


 public function get_ratings($condition){
  $this->db->select('course_rating.*,users.name,users.profile_pic');
  $this->db->from('course_rating');
  $this->db->join('users', 'users.id = course_rating.userID','left');
  $this->db->where($condition);
  $this->db->order_by('course_rating.id','desc');
  return $this->db->get()->result();
 }

 public function get_dubliccate_ratings($condition){
    $this->db->select('course_rating.*,users.name,users.profile_pic');
    $this->db->from('course_rating');
    $this->db->join('users', 'users.id = course_rating.userID','left');
    $this->db->where($condition);
    $this->db->order_by('course_rating.id','desc');
    return $this->db->get()->result();
   }

 public function get_rating_home(){
    $this->db->select('course_rating.*,users.name,users.profile_pic,users.user_type');
    $this->db->from('course_rating');
    $this->db->join('users', 'users.id = course_rating.userID','left');
    $this->db->group_by('course_rating.userID');
    $this->db->limit(4);
    $this->db->order_by('rand()');
    return $this->db->get()->result();
 }


 public function store_student_course($data)
 {
    return $this->db->insert('booked_courses',$data);
 }

 public function get_student_booked_course($condition){
    $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.name as student_name,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.name as institute_name,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
    $this->db->from('booked_courses');
    $this->db->join('users', 'users.id = booked_courses.studentID','left');
    $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
    $this->db->join('cities', 'cities.id = courses.locationID','left');
    $this->db->join('category', 'category.id = courses.categoryID','left');
    $this->db->join('users u', 'u.id = courses.userID','left');
    $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
    $this->db->where($condition);
    if($this->session->userdata('courses_booked')){
        $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
    }
    $this->db->order_by('booked_courses.id','desc');
    return $this->db->get()->result();
}

public function get_count_booked_course($condition){
  $this->db->where($condition);
  return $this->db->get('booked_courses')->result();
}

public function get_home_booked_course($condition){
  $this->db->select('booked_courses.*,courses.course as course_name,courses.start_date,courses.end_date,courses.fees,courses.description as course_discription,courses.discount,courses.mode,users.name as student_name,users.email as student_email,users.contact as student_contact,users.profile_pic,category.category,u.name as institute_name,u.email as institute_email,u.contact as institute_contact,batch.batch as course_batch ,batch.time_formate');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->join('courses', 'courses.id = booked_courses.courseID','left');
  $this->db->join('cities', 'cities.id = courses.locationID','left');
  $this->db->join('category', 'category.id = courses.categoryID','left');
  $this->db->join('users u', 'u.id = courses.userID','left');
  $this->db->join('batch ', 'batch.id = booked_courses.batch','left');
  $this->db->where($condition);
  if($this->session->userdata('courses_booked')){
      $this->db->where('courses.id',$this->session->userdata('courses_booked'));  
  }
  $this->db->order_by('rand()');
  $this->db->limit('4');
  return $this->db->get()->result();
}

public function get_booked_course($condition){
    $this->db->where($condition);
    return $this->db->get('booked_courses')->row();
}
 
public function updateStudentCourse($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('booked_courses',$data);
}

public function update_subject($data,$id){
    $this->db->where('id',$id);
    return $this->db->update('subjects',$data);
     //echo $this->db->last_query();die;
}

public function get_all_courses($condition){
		$this->db->where($condition);
    $query = $this->db->get('users')->result_array();
    //print_r($query);die;
    foreach($query as $key => $que){
      $this->db->select('count(courses.id) as courseTotal');
      $this->db->where('userID',$que['id']);
      $query[$key]['object'] = $this->db->get('courses')->row_array();
    }

   // print_r($query);die;
    return $query;
}

public function get_booked_course_by_student(){
  $this->db->select('courses.*');
  $this->db->join('courses', 'courses.id=booked_courses.courseID', 'left');
  $this->db->group_by('booked_courses.courseID'); 
  $query = $this->db->get('booked_courses')->result_array();
  
  foreach($query as $key => $row){
    $this->db->select('count(studentID) as totalStudent');
    $this->db->where('courseID',$row['id']);
    $this->db->group_by('studentID');
    $query[$key]['object'] = $this->db->get('booked_courses')->result_array();
  }
 // echo $this->db->last_query();die;
  return $query;
}

public function getcomments($condition){
  $this->db->select('course_rating.*,users.profile_pic');
  $this->db->join('users','users.id=course_rating.userID','left');
  $this->db->where($condition);
  $this->db->limit(3);
  return $this->db->get('course_rating')->result();
}

public function get_student_booked_courses($condition){
  $this->db->select('booked_courses.*,users.id as userID,users.email');
  $this->db->from('booked_courses');
  $this->db->join('users', 'users.id = booked_courses.studentID','left');
  $this->db->where($condition);
  $this->db->group_by('booked_courses.studentID');
  return $this->db->get()->result();
}


}