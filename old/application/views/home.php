	<?php $category_icon = array(
			'0' => "fat fa-megaphone",
			'1' => "fat fa-database",
			'2' => "fat fa-code",
			'3' => "fat fa-layer-group",
			'4' => "fat fa-camera",
			'5' => "fat fa-pen-ruler",
			'6' => "fat fa-object-group");
         ?>
	<section class="as-hero-wrapper hero-wrap3 bg-smoke">
	  <div class="hero-shape1">
	    <img src="<?=base_url('public/website/assets/img/hero/hero-3-1.png')?>" alt="hero image">
	  </div>
	  <div class="hero-shape2 jump">
	    <img src="<?=base_url('public/website/assets/img/icon/hero-icon-3-1-1.png')?>" alt="hero image">
	  </div>
	  <div class="hero-shape3">
	    <img src="<?=base_url('public/website/assets/img/icon/hero-icon-3-1-2.png')?>" alt="hero image">
	  </div>
	  <div class="hero-shape4 jump-reverse">
	    <img src="<?=base_url('public/website/assets/img/icon/hero-icon-3-1-3.png')?>" alt="hero image">
	  </div>
	  <div class="hero-shape5">
	    <img src="<?=base_url('public/website/assets/img/icon/hero-icon-3-1-4.png')?>" alt="hero image">
	  </div>
	  <div class="container z-index-common">
	    <div class="row justify-content-center text-center">
	      <div class="col-lg-10 col-xl-9 col-xxl-7">
	        <h3 class="hero-title2">Start learning from <br>top university courses & <span class="text-theme2">books</span>
	        </h3>
	        <form action="#" class="hero-form">
	          <div class="row">
	            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
	              <div class="form-group">
	                <input type="text" class="form-control" id="course" name="course" placeholder="Course Name">
	              </div>
	            </div>
	            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
	              <div class="form-group">
	                <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control"
	                  id="start_date" name="end_date" placeholder="Start Date">
	              </div>
	            </div>
	            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
	              <div class="form-group">
	                <select type="text" class="form-control" id="mode" name="mode" placeholder="Select Mode">
	                  <option value=""> Select Mode</option>
	                  <option value="Online"> Online</option>
	                  <option value="Offline"> Offline</option>
	                  <option value="Both"> Both</option>
	                </select>
	              </div>
	            </div>


	            <div class="col-xl-3 col-lg-4 col-md-3 col-sm-6 col-6">
	              <div class="form-group">
	                <button class="as-btn style2" name="submit" type="button" onclick="setCourseSession()">Search Course <i
	                    class="fas fa-long-arrow-right ms-2"></i></button>
	              </div>
	            </div>
	          </div>
	        </form>
	        <h3 class="hero-title-style2 h4">Explore <span class="text-theme2"><?=count($totalCourses)?>+</span> Courses
	          within Subject </h3>
	      </div>
	    </div>
	  </div>
	</section>
	<?php if($this->session->userdata('user_type')!='Doctor'){?>
	<section class="space-top">
	  <div class="container z-index-common">
	    <div class="title-area text-center">
	      <h3 class="sec-title">Top Courses For Popular Courses</h3>
	    </div>
	    <div class="row as-carousel slider-shadow" data-slide-show="6" data-xl-slide-show="5" data-ml-slide-show="4"
	      data-lg-slide-show="4" data-md-slide-show="3" data-sm-slide-show="2" data-xs-slide-show="2">
	      <?php
              $i=0; 
             foreach($totalCourses as $course){
               //($i<=6) ? ($category_class = $category_icon[$i]) : $i=0;
             ?>
	      <div class="col-xl-2">
	        <a href="javascript:void(0)" onclick="get_subjects(<?=$course->id?>)">
	          <div class="category-style4" data-bg-src="<?=base_url('public/website/assets/img/bg/cat-bg-4-1.jpg')?>">
	            <div class="category-icon"><i class="fal fa-list-alt"></i></div>
	            <h3 class="category-title"><?=$course->course; ?></h3>
	            <p class="category-count">Over
	              <?//=$category['course_count']['totalCourses']; ?> Courses
	            </p>
	          </div>
	        </a>
	      </div>

	      <?php  $i ++ ;} ?>
	    </div>


	  </div>
	</section>
	<?php } ?>

	<section class="space" id="subject_section">
	</section>

	<section class="space" data-bg-src="<?=base_url('public/website/assets/img/bg/cta-bg-6-1.jpg')?>">
	  <div class="container">
	    <div class="row justify-content-center text-center">
	      <div class="col-xxl-6 col-lg-8 col-md-10">
	        <div class="title-area">
	          <span class="subtitle">Become a Courses</span>
	          <h2 class="sec-title text-white">Get Free Pro Membership For Your Academic Online Class</h2>
	        </div>
	        <a href="<?=base_url('contact-us')?>" class="as-btn">Let’s Contact With Us<i
	            class="fas fa-long-arrow-right ms-2"></i></a>
	      </div>
	    </div>
	  </div>
	  <div class="shape-mockup moving d-none d-lg-block" data-top="0%" data-left="3%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-plane-4.png')?>" alt="shapes"></div>
	  <div class="shape-mockup moving-reverse d-none d-lg-block" data-top="0%" data-right="3%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-plane-3.png')?>" alt="shapes"></div>
	</section>

	<?php if($this->session->userdata('user_type')!='Teacher' && $this->session->userdata('user_type')!='Doctor'){?>
	<section class="bg-smoke space">
	  <div class="container">
	    <div class="title-area">
	      <div class="row justify-content-center justify-content-lg-between align-items-center text-center text-lg-start">
	        <div class="col-lg-8 col-xl-5">
	          <span class="subtitle left">Stors</span>
	          <h2 class="sec-title">Top Rated Stors</h2>
	        </div>
	        <div class="col-lg-auto d-none d-lg-block">
	          <div class="icon-box"><button class="slick-arrow slick-prev default" data-slick-prev="#courseSlide1"><i
	                class="fal fa-long-arrow-left"></i></button> <button class="slick-arrow slick-next default"
	              data-slick-next="#courseSlide1"><i class="fal fa-long-arrow-right"></i></button></div>
	        </div>
	      </div>
	    </div>
	    <div class="row slider-shadow as-carousel" id="courseSlide1" data-slide-show="4" data-ml-slide-show="3"
	      data-lg-slide-show="3" data-md-slide-show="2" data-xs-slide-show="2">
	      <?php foreach($books as $book){  ?>
	      <div class="col-md-6 col-lg-4 col-xxl-3">
	        <div class="as-box as-box--style4">
	          <div class="as-box__img">
	            <img src="<?=base_url($book->image)?>" alt="book Image" class="w-100" style="width:332px;height:232px;">
	            <div class="as-box__category"><a href="javascript:void(0)">₹ <?=$book->price; ?></a></div>
	          </div>
	          <div class="as-box__middle">
	            <div class="as-box__author"><a href="javascript:void(0)"><?=$book->name?></a></div>
	            <!-- <div class="as-box__rating">
                         5.00
                         <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></div>
                      </div> -->
	          </div>
	          <h3 class="as-box__title"><a href="javascript:void(0)"
	              title="<?=$book->discription?>"><?=substr($book->discription,0,100)?>...</a></h3>
	          <div class="as-box__bottom">
	            <?php if($this->session->userdata('email')){?>
	            <div class="text-center"><a href="javascript:void(0)" class="as-btn " onclick="addCart(<?=$book->id?>,'Book',<?=$book->price; ?>)"> <i
	                  class="fal fa-cart-plus"></i> Cart</a></div>
	            <?php }else{?>
	            <div class="text-center"><a href="javascript:void(0)" class="as-btn" onclick="redirectToLogin()"> <i
	                  class="fal fa-cart-plus"></i> Cart</a></div>
	            <?php } ?>
	          </div>
	        </div>
	      </div>
	      <?php } ?>
	    </div>
	    <p class="mb-4 mt-4 text-center fw-semibold"><strong class="text-theme"><?=count($books)?>+</strong> More Skillful
	      Courses You Can Explore</p>
	    <div class="text-center"><a href="<?=base_url('store')?>" class="as-btn">View All Store<i
	          class="fas fa-long-arrow-right ms-2"></i></a></div>
	  </div>
	  <div class="shape-mockup jump d-none d-sm-block" data-top="14%" data-left="45%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-plane-1.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump-reverse d-none d-sm-block" data-bottom="3%" data-left="2%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-plane-2.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump d-none d-sm-block" data-bottom="7%" data-right="0%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-1.png')?>" alt="shapes"></div>
	</section>
	<?php } ?>


	<section class="space-top space-extra-bottom" id="testi-sec">
	  <?php if(count($ratings) > 0) { ?>
	  <div class="container">
	    <div class="row justify-content-center text-center">
	      <div class="col-xl-5">
	        <div class="title-area">
	          <span class="subtitle">Reviews</span>
	          <h2 class="sec-title">People Who Already Love Us</h2>
	        </div>
	      </div>
	    </div>
	    <div class="row slider-shadow as-carousel" data-slide-show="2" data-lg-slide-show="1" data-dots="true"
	      data-xl-dots="true" data-ml-dots="true">
	      <?php foreach($ratings as $rating){?>
	      <div class="col-lg-6">
	        <div class="testi-box">
	          <div class="testi-box_content">
	            <div class="testi-box_img"><img src="<?=base_url($rating->profile_pic)?>" alt="Avater"
	                style="width:70px;height:70px;"></div>
	            <p class="testi-box_text">“<?=$rating->description?>”</p>
	          </div>
	          <div class="testi-box_bottom">
	            <div>
	              <h3 class="testi-box_name"><?=$rating->title?></h3>
	              <span class="testi-box_desig"><?=$rating->user_type?></span>
	            </div>
	            <div class="testi-box_review">
	              <?php 
                  $userRating = array();
                  for($i=1; $i<=$rating->rating; $i++){
                     $userRating[] = $i;
                  }
                  for($j=1; $j<=5; $j++){
                     if(in_array($j,$userRating)){
                  ?>
	              <i class="icon-23"></i>
	              <?php }else{
                                                ?>
	              <i class="fa-solid fa-star-sharp"></i>
	              <?php
                         } } ?>
	            </div>
	          </div>
	        </div>
	      </div>
	      <?php } ?>

	    </div>
	  </div>
	  <?php } ?>
	  <div class="shape-mockup jump d-none d-sm-block" data-top="18%" data-left="5%"><img
	      src="<?=base_url('public/website/assets/img/shape/triangle-1.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump-reverse d-none d-sm-block" data-top="18%" data-left="5%"><img
	      src="<?=base_url('public/website/assets/img/shape/dots-2.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump d-none d-sm-block" data-top="15%" data-right="6%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-3.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump-reverse d-none d-sm-block" data-bottom="13%" data-left="5%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-4.png')?>" alt="shapes"></div>
	  <div class="shape-mockup jump d-none d-sm-block" data-bottom="14%" data-right="5%"><img
	      src="<?=base_url('public/website/assets/img/shape/line-5.png')?>" alt="shapes"></div>
	</section>

	<script>
function get_subjects(courseID) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_subjects_home')?>',
    type: 'POST',
    data: {
      courseID
    },
    dataType: 'html',
    success: function(data) {
      $('#subject_section').html(data);
    },
    error: function() {}
  });
}
	</script>