
    <style>
     .daysClass {
    display: inline-block;
    line-height: 1;
    font-size: 14px;
    font-weight: 700;
    color: var(--white-color)!important;
    background-color: var(--theme-color);
    padding: 8px 15px;
    border-radius: 9999px;
}
    </style>

    <section class="course-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-12 col-xl-12">
            <div class="as-sort-bar mb-30">
              <div class="row justify-content-between align-items-center">
                <!-- <div class="col-md-auto">
                  <div class="nav" role="tablist"><a href="#" class="active" id="tab-shop-grid" data-bs-toggle="tab" data-bs-target="#tab-grid" role="tab" aria-controls="tab-grid" aria-selected="true"><i class="fas fa-th"></i>Grid</a> <a href="#" id="tab-shop-list" data-bs-toggle="tab" data-bs-target="#tab-list" role="tab" aria-controls="tab-grid" aria-selected="false"><i class="fas fa-list"></i>List</a></div>
                </div> -->
                <div class="col-md">
                  <p class="woocommerce-result-count"><strong class="text-title"><?=count($books)?></strong> <?=$page_title?></p>
                </div>
                <!-- <div class="col-md-auto">
                  <form class="woocommerce-ordering" method="get">
                    <select name="orderby" class="orderby" aria-label="Shop order">
                      <option value="menu_order" selected="selected">Default Sorting</option>
                      <option value="popularity">Sort by popularity</option>
                      <option value="rating">Sort by average rating</option>
                      <option value="date">Sort by latest</option>
                      <option value="price">Sort by price: low to high</option>
                      <option value="price-desc">Sort by price: high to low</option>
                    </select>
                  </form>
                </div> -->
              </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade active show" id="tab-grid" role="tabpanel" aria-labelledby="tab-shop-grid">
                <div class="row">
                <?php 
               if(count($books) > 0){
          foreach($books as $book){
 
        ?>
                  <div class="col-md-6 col-lg-6 col-xxl-3">
                    <div class="as-box as-box--style4">
                      <div class="as-box__img">
                        <img src="<?=base_url($book->image)?>" alt="course Image" class="w-100" style="width:332px;height:232px;">
                        <div class="as-box__category"><a href="#">₹ <?=$book->price?></a></div>
                      </div>
                      <div class="as-box__middle">
                        <div class="as-box__author"><span class="mr-5 " ><?=$book->name?></span></div>
                       
                      </div>
                      <h3 class="as-box__title"><a href="#" title="<?=$book->discription?>"><?=substr($book->discription,0,100)?>...</a></h3>
                      
                      <div class="as-box__bottom ">
                      <?php if($this->session->userdata('email')){?>
                        <div class="text-center"><a href="#" class="as-btn " onclick="addCart(<?=$book->id?>,'Book',<?=$book->price?>)"> <i class="fal fa-cart-plus"></i> Cart</a></div>
                      <?php }else{?>
                        <div class="text-center"><a href="#" class="as-btn" onclick="redirectToLogin()"> <i class="fal fa-cart-plus"></i> Cart</a></div>
                     <?php } ?>
                      </div>
                    </div>
                  </div>
              <?php }  } else{?>
                <h1 class="text-center mt-5 text-danger">No Record found</h1>
                <?php } ?>
              
                </div>
              </div>

            </div>
            <!-- <div class="as-pagination pt-20">
              <ul>
                <li><a href="blog.html">1</a></li>
                <li><a href="blog.html">2</a></li>
                <li><a href="blog.html">Next<i class="far fa-long-arrow-right"></i></a></li>
              </ul>
            </div> -->
          </div>

        </div>
      </div>
    </section>
    <script>
function redirectToLogin(){
        Swal.fire({
        icon: 'error',
        title: 'Please Login first',
        text: "Login first to buy book",
        //footer: '<a href="">Why do I have this issue?</a>'
      })
    setTimeout(function(){
        window.location="my-account";
    }, 2000);
}
</script>