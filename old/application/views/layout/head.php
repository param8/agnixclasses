<!doctype html>
<html class="no-js" lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title><?=$page_title?></title>
      <meta name="author" content="Angfuzsoft">
      <meta name="description" content="Agnix Class">
      <meta name="keywords" content="Agnix Class">
      <meta name="robots" content="INDEX,FOLLOW">
      <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
      <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url($siteinfo->site_logo)?>" style="width:57px;height:57px;">
      <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url($siteinfo->site_logo)?>" style="width:60px;height:60px;">
      <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url($siteinfo->site_logo)?>" style="width:72px;height:72px;">
      <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url($siteinfo->site_logo)?>" style="width:76px;height:76px;">
      <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url($siteinfo->site_logo)?>" style="width:114px;height:114px;">
      <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url($siteinfo->site_logo)?>" style="width:120px;height:120px;">
      <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url($siteinfo->site_logo)?>" style="width:144px;height:144px;">
      <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url($siteinfo->site_logo)?>" style="width:152px;height:152px;">
      <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url($siteinfo->site_logo)?>" style="width:180px;height:180px;">
      <link rel="icon" type="image/png" sizes="192x192" href="<?=base_url($siteinfo->site_logo)?>" style="width:192px;height:192px;">
      <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url($siteinfo->site_logo)?>" style="width:32px;height:32px;">
      <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url($siteinfo->site_logo)?>" style="width:96px;height:96px;">
      <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url($siteinfo->site_logo)?>" style="width:16px;height:16px;">
      <link rel="manifest" href="<?=base_url('public/website/assets/img/favicons/manifest.json')?>">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="<?=base_url('public/website/assets/img/favicons/ms-icon-144x144.png')?>">
      <meta name="theme-color" content="#ffffff">
      <link rel="preconnect" href="https://fonts.googleapis.com/">
      <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Khula:wght@400;600;700&amp;family=Nunito+Sans:wght@400;700&amp;display=swap" rel="stylesheet">
      <link rel="stylesheet" href="<?=base_url('public/website/assets/css/app.min.css')?>">
      <link rel="stylesheet" href="<?=base_url('public/website/assets/css/fontawesome.min.css')?>">
      <link rel="stylesheet" href="<?=base_url('public/website/assets/css/style.css')?>">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
      <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
      <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" ></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" ></script> -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style>
      .p_auto {
         padding: 0 5px;
      }
      .p_auto .dropdown-content {
         /* display: none; */
         position: absolute;
         background-color: #f9f9f9;
         /* min-width: 98px; */
         box-shadow: 0px 8px 16px 0px rgb(0 0 0 / 20%);
         padding: 12px 16px;
         z-index: 1;
         width: 158px!important;
         right: -28px!important;
      }

      /* .select2-container--default .select2-selection--multiple {
    width: 450px;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 20px;
    cursor: text;
} */
div#myFrame {
    /* max-height: 400px; */
    min-height: 270px;
    display: block;
    height: 348px;
}
   </style>
   </head>