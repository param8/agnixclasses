<div class="breadcumb-wrapper" data-bg-src="<?=base_url('public/website/assets/img/breadcumb/breadcumb-bg.jpg')?>">
      <div class="container z-index-common">
        <div class="breadcumb-content text-center">
          <h1 class="breadcumb-title"><?=$page_title?></h1>
          <div class="breadcumb-menu-wrap">
            <ul class="breadcumb-menu">
              <li><a href="<?=base_url('home')?>">Home</a></li>
              <li><?=$page_title?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>