<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=           Cart Area Start         =-->
<!--=====================================-->
<section class="cart-page-area edu-section-gap">
  <div class="container">
    <div class="table-responsive">
      <table class="table cart-table">
        <thead>
          <tr>
            <th scope="" class="">SNO</th>
            <th scope="" class="">Message</th>
            <th scope="" class="">Created Date</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($notification as $key=>$noti){?>
          <tr>
            <td class="">
              <?=$key+1?>
            </td>
            <td class="">
            <?=$noti->message?>
            </td>
            <td class="">
            <?=date('d-m-Y',strtotime($noti->created_at))?>
            </td>>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>

    
  </div>
</section>