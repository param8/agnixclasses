       <div class="edu-breadcrumb-area breadcrumb-style-3">
            <div class="container">
                <div class="breadcrumb-inner">
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="index-one.html">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                    <div class="page-title">
                        <h1 class="title"><?=$course->course?></h1>
                    </div>
                    <ul class="course-meta">
                        <li><i class="icon-58"></i>by Edward Norton</li>
                        <li><i class="icon-59"></i><?=$course->subject_name?></li>
                        <li class="course-rating">
                            <div class="rating">
                                <i class="icon-23"></i>
                                <i class="icon-23"></i>
                                <i class="icon-23"></i>
                                <i class="icon-23"></i>
                                <i class="icon-23"></i>
                            </div>
                            <span class="rating-count">(720 Rating)</span>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

        <!--=====================================-->
        <!--=     Courses Details Area Start    =-->
        <!--=====================================-->
        <?php 
        $earlier = new DateTime($course->start_date);
        $later = new DateTime($course->end_date);
        $totalDays =  $later->diff($earlier)->format("%a");
        if($course->fees > 0 && $course->discount > 0){
            $price =$course->fees - ($course->discount*$course->fees)/100;
        }else{
            $price =$course->fees;
        }
        ?>
        <section class="edu-section-gap course-details-area">
            <div class="container">
                <div class="row row--30">
                    <div class="col-lg-8">
                        <div class="course-details-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="overview-tab" data-bs-toggle="tab" data-bs-target="#overview" type="button" role="tab" aria-controls="overview" aria-selected="true">Overview</button>
                                </li>
                                <!-- <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="carriculam-tab" data-bs-toggle="tab" data-bs-target="#carriculam" type="button" role="tab" aria-controls="carriculam" aria-selected="false">Carriculam</button>
                                </li> -->
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="instructor-tab" data-bs-toggle="tab" data-bs-target="#instructor" type="button" role="tab" aria-controls="instructor" aria-selected="false">Instructor</button>
                                </li>

                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="review-tab" data-bs-toggle="tab" data-bs-target="#review" type="button" role="tab" aria-controls="review" aria-selected="false">Reviews</button>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                                    <div class="course-tab-content">
                                        <div class="course-overview">
                                            <h3 class="heading-title">Course Description</h3>
                                            <p><?=$course->description?></p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="tab-pane fade" id="instructor" role="tabpanel" aria-labelledby="instructor-tab">
                                    <div class="course-tab-content">
                                        <div class="course-instructor">
                                            <div class="thumbnail">
                                                <img src="assets/images/course/author-01.png" alt="Author Images">
                                            </div>
                                            <div class="author-content">
                                                <h6 class="title">Edward Norton</h6>
                                                <span class="subtitle">Founder & CEO</span>
                                                <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua enim minim veniam quis nostrud exercitation ulla mco laboris nisi ut aliquip ex ea commodo consequat. duis aute irure dolor in reprehenderit in voluptate.</p>
                                                <ul class="social-share">
                                                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                                                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                                                    <li><a href="#"><i class="icon-linkedin2"></i></a></li>
                                                    <li><a href="#"><i class="icon-youtube"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                                    <div class="course-tab-content">
                                        <div class="course-review">
                                            <h3 class="heading-title">Course Rating</h3>
                                            <p>5.00 average rating based on 7 rating</p>
                                            <div class="row g-0 align-items-center">
                                                <div class="col-sm-4">
                                                    <div class="rating-box">
                                                        <div class="rating-number">5.0</div>
                                                        <div class="rating">
                                                            <i class="icon-23"></i>
                                                            <i class="icon-23"></i>
                                                            <i class="icon-23"></i>
                                                            <i class="icon-23"></i>
                                                            <i class="icon-23"></i>
                                                        </div>
                                                        <span>(7 Review)</span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="review-wrapper">

                                                        <div class="single-progress-bar">
                                                            <div class="rating-text">
                                                                5 <i class="icon-23"></i>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            <span class="rating-value">7</span>
                                                        </div>

                                                        <div class="single-progress-bar">
                                                            <div class="rating-text">
                                                                4 <i class="icon-23"></i>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            <span class="rating-value">0</span>
                                                        </div>

                                                        <div class="single-progress-bar">
                                                            <div class="rating-text">
                                                                4 <i class="icon-23"></i>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            <span class="rating-value">0</span>
                                                        </div>

                                                        <div class="single-progress-bar">
                                                            <div class="rating-text">
                                                                4 <i class="icon-23"></i>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            <span class="rating-value">0</span>
                                                        </div>

                                                        <div class="single-progress-bar">
                                                            <div class="rating-text">
                                                                4 <i class="icon-23"></i>
                                                            </div>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                            <span class="rating-value">0</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Start Comment Area  -->
                                            <div class="comment-area">
                                                <h3 class="heading-title">Reviews</h3>
                                                <div class="comment-list-wrapper">
                                                    <!-- Start Single Comment  -->
                                                    <div class="comment">
                                                        <div class="thumbnail">
                                                            <img src="assets/images/blog/comment-01.jpg" alt="Comment Images">
                                                        </div>
                                                        <div class="comment-content">
                                                            <div class="rating">
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                            </div>
                                                            <h5 class="title">Haley Bennet</h5>
                                                            <span class="date">Oct 10, 2021</span>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                        </div>
                                                    </div>
                                                    <!-- End Single Comment  -->
                                                    <!-- Start Single Comment  -->
                                                    <div class="comment">
                                                        <div class="thumbnail">
                                                            <img src="assets/images/blog/comment-02.jpg" alt="Comment Images">
                                                        </div>
                                                        <div class="comment-content">
                                                            <div class="rating">
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                            </div>
                                                            <h5 class="title">Simon Baker</h5>
                                                            <span class="date">Oct 10, 2021</span>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                        </div>
                                                    </div>
                                                    <!-- End Single Comment  -->
                                                    <!-- Start Single Comment  -->
                                                    <div class="comment">
                                                        <div class="thumbnail">
                                                            <img src="assets/images/blog/comment-03.jpg" alt="Comment Images">
                                                        </div>
                                                        <div class="comment-content">
                                                            <div class="rating">
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                                <i class="icon-23"></i>
                                                            </div>
                                                            <h6 class="title">Richard Gere</h6>
                                                            <span class="date">Oct 10, 2021</span>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                        </div>
                                                    </div>
                                                    <!-- End Single Comment  -->
                                                </div>
                                            </div>
                                            <!-- End Comment Area  -->
                                            <?php if($this->session->userdata('user_type')=='Student'){?>
                                            <div class="comment-form-area">
                                                <h3 class="heading-title">Write a Review</h3>
                                                <div class="rating-icon">
                                                    <h6 class="title">Rating Here</h6>
                                                    <div class="rating">
                                                        <i class="icon-23"></i>
                                                        <i class="icon-23"></i>
                                                        <i class="icon-23"></i>
                                                        <i class="icon-23"></i>
                                                        <i class="icon-23"></i>
                                                    </div>
                                                </div>
                                                <form class="comment-form">
                                                    <div class="row g-5">
                                                        <div class="form-group col-lg-6">
                                                            <input type="text" name="comm-title" id="comm-title" placeholder="Review Title">
                                                        </div>
                                                        <div class="form-group col-lg-6">
                                                            <input type="text" name="comm-name" id="comm-name" placeholder="Reviewer name">
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <input type="email" name="comm-email" id="comm-email" placeholder="Reviewer email">
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <textarea name="comm-message" id="comm-message" cols="30" rows="5" placeholder="Review summary"></textarea>
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <button type="submit" class="edu-btn submit-btn">Submit Review <i class="icon-4"></i></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="course-sidebar-3 sidebar-top-position">
                            <div class="edu-course-widget widget-course-summery">
                                <div class="inner">
                                    <div class="thumbnail">
                                        <img src="<?=base_url($course->image)?>" width="340" height="250" alt="Courses">
                                        <!-- <a href="https://www.youtube.com/watch?v=PICj5tr9hcc" class="play-btn video-popup-activation"><i class="icon-18"></i></a> -->
                                    </div>
                                    <div class="content">
                                        <h4 class="widget-title">Course Includes:</h4>
                                        <ul class="course-item">
                                            <li>
                                                <span class="label"><i class="icon-60"></i>Price:</span>
                                                <span class="value price">₹ <?=$price?></span>
                                            </li>
                                            <li>
                                                <span class="label"><i class="icon-62"></i>Instrutor:</span>
                                                <span class="value">Edward Norton</span>
                                            </li>
                                            <li>
                                                <span class="label"><i class="icon-61"></i>Duration:</span>
                                                <span class="value"><?=$totalDays?> Days</span>
                                            </li>
                                            <!-- <li>
                                                <span class="label">
                                                    <img class="svgInject" src="assets/images/svg-icons/books.svg" alt="book icon">
                                                    Lessons:</span>
                                                <span class="value">8</span>
                                            </li> -->
                                            <li>
                                                <span class="label"><i class="icon-63"></i>Enrolled:</span>
                                                <span class="value">65 students</span>
                                            </li>
                                            <li>
                                                <span class="label"><i class="icon-59"></i>Subject:</span>
                                                <span class="value"><?=$course->subject_name?></span>
                                            </li>
                                            <li>
                                                <span class="label"><i class="icon-64"></i>Certificate:</span>
                                                <span class="value">Yes</span>
                                            </li>
                                        </ul>
                                        <div class="read-more-btn">
                                            <a href="#" class="edu-btn">Start Now <i class="icon-4"></i></a>
                                        </div>
                                        <div class="share-area">
                                            <h4 class="title">Share On:</h4>
                                            <ul class="social-share">
                                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                                <li><a href="#"><i class="icon-linkedin2"></i></a></li>
                                                <li><a href="#"><i class="icon-youtube"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--=====================================-->
        <!--=     More Courses Area Start    =-->
        <!--=====================================-->
        <!-- Start Course Area  -->
        <div class="gap-bottom-equal">
            <div class="container">
                <div class="section-title section-left" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                    <h3 class="title">More Courses for You</h3>
                </div>
                <div class="row g-5">
                    <!-- Start Single Course  -->
                   <?php 
                     foreach($courses as $moreCourse){
                        $earlier_more = new DateTime($moreCourse->start_date);
                        $later_more = new DateTime($moreCourse->end_date);
                        $totalDays_more =  $later_more->diff($earlier_more)->format("%a");
                        if($moreCourse->fees > 0 && $moreCourse->discount > 0){
                            $price_more =$moreCourse->fees - ($moreCourse->discount*$moreCourse->fees)/100;
                        }else{
                            $price_more =$moreCourse->fees;
                        }
                    ?>
                    <div class="col-12 col-xl-4 col-lg-6 col-md-6" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                        <div class="edu-course course-style-5 inline" data-tipped-options="inline: 'inline-tooltip-<?=$moreCourse->id?>'">
                            <div class="inner">
                                <div class="thumbnail">
                                    <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>">
                                        <img src="<?=base_url($moreCourse->image)?>" alt="Course Meta">
                                    </a>
                                </div>
                                <div class="content">
                                    <div class="course-price price-round">₹ <?=$price_more?></div>
                                    <h5 class="title">
                                        <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>"><?=$moreCourse->course?></a>
                                    </h5>
                                    <div class="course-rating">
                                        <div class="rating">
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                        </div>
                                        <span class="rating-count">(5)</span>
                                    </div>
                                    <p><?=$moreCourse->description?></p>
                                    <ul class="course-meta">
                                        <li><i class="icon-24"></i>15 Lessons</li>
                                        <li><i class="icon-25"></i>42 Students</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div id="inline-tooltip-<?=$moreCourse->id?>" style="display:none">
                            <div class="course-layout-five-tooltip-content">
                                <div class="content">
                                    <!-- <span class="course-level">Cooking</span> -->
                                    <h5 class="title">
                                        <a href="<?=base_url('course-details/'.base64_encode($moreCourse->id))?>"><p><?=$moreCourse->course?></p></a>
                                    </h5>
                                    <div class="course-rating">
                                        <div class="rating">
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                            <i class="icon-23"></i>
                                        </div>
                                        <span class="rating-count">(5)</span>
                                    </div>
                                    <ul class="course-meta">
                                        <li>15 Lessons</li>
                                        <li>35 hrs</li>
                                        <li>Beginner</li>
                                    </ul>
                                    <div class="course-feature">
                                    <p><?=$moreCourse->description?></p>
                                    
                                    </div>
                                    <div class="button-group">
                                        <a href="#" class="edu-btn btn-medium">Add to Cart</a>
                                        <a href="#" class="wishlist-btn btn-outline-dark"><i class="icon-22"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <!-- End Single Course  -->
                </div>
            </div>
        </div>
        