
    <section class="space-top" data-sec-pos="bottom-half" data-pos-for="#ctav2">
      <div class="container">
        <div class="bg-smoke z-index-common rounded-3">
          <div class="row gx-0">
            <div class="col-md-6 col-lg-6 feature-style1">
              <div class="feature-icon">
                <div class="icon-shape"></div>
                <div class="icon-border"></div>
                <div class="icon-inner"><img src="<?=base_url('public/website/assets/img/icon/rupee-icon.jpg')?>" alt="icon"></div>
              </div>
              <div class="media-body">
                <h3 class="feature-title h4">Total Rewards Earns</h3>
                <!-- <p class="feature-text">0</p> -->
                <div><span>Points</span> <?=$wallet->point?></div>
                 <!-- <a href="<?//=base_url('redeem-now')?>" class="btn btn-primary">Redeem Now-></a> -->
              </div>
            </div>
            <div class="col-md-6 col-lg-6 feature-style1">
              <div class="feature-icon">
                <div class="icon-shape"></div>
                <div class="icon-border"></div>
                <div class="icon-inner"><img src="<?=base_url('public/website/assets/img/icon/feature-1-2.png')?>" alt="icon"></div>
              </div>
              <div class="media-body">
                    <h3 class="feature-title h4">Share Your Referral Code</h3>
                    <div>
                    <p class="feature-text"><strong>AGNIX-<span class="text-success" id="ref_code"><?=$this->session->userdata('unique_id')?></span></strong> <span><a href="javascript:void(0)" onclick="copyToClipboard('#ref_code')" class="btn btn-primary btn-round"><i class="fal fa-copy"></i></a></span>
                    <!-- <h6 class="feature-title h6">Invite via</h6> -->
                    <?php $link = 'I recommend you to try AGNIX Class - Use my referral code is '.$this->session->userdata('unique_id').' ' .base_url('ref/'.base64_encode($this->session->userdata('unique_id')))?>
                    <span><a href="whatsapp://send?text=<?=$link?>" data-link="share/whatsapp/share" target="_blank" class="btn btn-success btn-round" style="color:inherit;"><i class="fa fa-whatsapp" style="font-size:24px"></i></a></span>
                    <!-- <span><a href="mailto:email@email.com"> <i class="fa fa-envelope" style="font-size:24px"></i></a></span> -->
                     </div>
                    <!-- <a href="whatsapp://send?text=This is WhatsApp sharing example using link"data-action="share/whatsapp/share"  
                        target="_blank"> Share to WhatsApp </a> -->
                        <!-- <a class="button" href="mailto:email@email.de">Kontakt</a> -->
                     </p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <section class="bg-dark space" data-bg-src="<?=base_url('public/website/assets/img/bg/cta-bg-2-1.jpg')?>" id="ctav2">
      <div class="container">
        <div class="row justify-content-center text-center">
          <div class="col-lg-8 col-xl-7">

           
           
           
          </div>
        </div>
      </div>
    </section>
    <script>
      function copyToClipboard(element) {
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val($(element).text()).select();
      document.execCommand("copy");
      $temp.remove();
      toastr.success('Copied');
    }
    </script>
   
   