
<div class="container h-p100">
  <div class="row align-items-center justify-content-md-center h-p100">

    <div class="col-12">
      <div class="row justify-content-center no-gutters">
        <div class="col-lg-5 col-md-5 col-12">
          <div class="bg-white rounded30 shadow-lg">
            <div class="content-top-agile p-20 pb-0">
              <h2 class="text-primary"><?=$siteinfo->site_name?></h2>
              <p class="mb-0">Change Password .</p>
            </div>
            <div class="p-40">
            <form action="<?=base_url('admin/User/update_password')?>" id="updatePasswordForm" method="POST"
          enctype="multipart/form-data">
          <div class="modal-body">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label for="site_name" class="col-form-label">OTP:</label>
                <input type="text" class="form-control" name="otp" id="otp">
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label for="site_contact" class="col-form-label">New Password:</label>
                <input type="password" class="form-control" name="new_password" id="new_password">
              </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label for="site_contact" class="col-form-label">Confirm Password:</label>
                <input type="password" class="form-control" name="confirm_password" id="confirm_password">
              </div>
            </div>

            <div class="text-center">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
            
          </div>
        </div>
  
      </div>
    </div>
  </div>
</div>
</div>

<script>
$("form#updatePasswordForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("email", '<?=$email?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        setTimeout(function() {
          location.href = "<?=base_url('admin')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error('Unable to add site info');
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>


