<div class="container h-p100">
  <div class="row align-items-center justify-content-md-center h-p100">

    <div class="col-12">
      <div class="row justify-content-center no-gutters">
        <div class="col-lg-5 col-md-5 col-12">
          <div class="bg-white rounded30 shadow-lg">
            <div class="content-top-agile p-20 pb-0">
              <h2 class="text-primary"><?=$siteinfo->site_name?></h2>
              <p class="mb-0">Sign in to continue .</p>
            </div>
            <div class="p-40">
              <form action="<?=base_url('Authantication/adminLogin')?>" id="login" method="post">
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
                    </div>
                    <input type="text" class="form-control pl-15 bg-transparent" name="username" id="username"
                      placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
                    </div>
                    <input type="password" class="form-control pl-15 bg-transparent" name="password" id="password"
                      placeholder="Password">
                  </div>
                </div>
                <!-- <div class="row">
										<div class="col-6">
										  <div class="checkbox">
											<input type="checkbox" id="basic_checkbox_1" >
											<label for="basic_checkbox_1">Remember Me</label>
										  </div>
										</div> -->
                <!-- /.col -->
                <!-- <div class="col-6">
										 <div class="fog-pwd text-right">
											<a href="javascript:void(0)" class="hover-warning"><i class="ion ion-locked"></i> Forgot pwd?</a><br>
										  </div>
										</div> -->
                <!-- /.col -->
                <div class="col-12 text-center">
                  <button type="submit" class="btn btn-danger mt-10">SIGN IN</button>
                </div>
                <!-- /.col -->
            </div>
            </form>
            <div class="text-center">
              <p class="mt-15 mb-0">Don't have an account? <a href="javascrip:void(0)" data-toggle="modal"
                  data-target="#exampleModal" data-whatever="@mdo" class="text-warning ml-5">Forget Password</a></p>
            </div>
          </div>
        </div>
        <!-- <div class="text-center">
						  <p class="mt-20 text-white">- Sign With -</p>
						  <p class="gap-items-2 mb-20">
							  <a class="btn btn-social-icon btn-round btn-facebook" href="#"><i class="fa fa-facebook"></i></a>
							  <a class="btn btn-social-icon btn-round btn-twitter" href="#"><i class="fa fa-twitter"></i></a>
							  <a class="btn btn-social-icon btn-round btn-instagram" href="#"><i class="fa fa-instagram"></i></a>
							</p>	
						</div> -->
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Forget Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="<?=base_url('admin/User/send_otp')?>" method="POST" id="sendOTPForm">
				<div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">email:</label>
            <input type="email" class="form-control" name="email" id="email">
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Send</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$("form#login").submit(function(e) {

  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?=base_url('dashboard')?>";
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$("form#sendOTPForm").submit(function(e) {

  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?=base_url('change-password/')?>"+data.email;
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>


<!-- Vendor JS -->