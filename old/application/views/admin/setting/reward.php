<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="">
        <form action="<?=base_url('admin/setting/update_reward')?>" id="updateRewardForm" method="POST"
          enctype="multipart/form-data">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="site_name" class="col-form-label">Referal Reward Point:</label>
                <input type="text" class="form-control" name="refreral_point_value" id="refreral_point_value"
                  value="<?=$reward->refreral_point_value?>"
                  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group">
                <label for="site_contact" class="col-form-label">Point Value(₹):</label>
                <input type="text" class="form-control" maxlength="10" minlength="10" name="point_value"
                  id="point_value" value="<?=$reward->point_value?>"
                  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$("form#updateRewardForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastr.success(data.message);
        setTimeout(function() {
          location.reload();
        }, 1000)

      } else if (data.status == 403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastr.error('Unable to add site info');
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>