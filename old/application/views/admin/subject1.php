 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-graduation-cap"></i> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  <div class="box">
				<div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="box-header with-border">
              <h3 class="box-title">All <?=$page_title?></h3>
             
             </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 ">
            <div class="box-header with-border float-right">
              
            <a href="javascript:void(0)" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addSubjectModal" data-whatever="@mdo">Add <?=$page_title?> <i class="fa fa-plus"></i></a>
              
            </div>
         </div>
         </div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>Action</th>
								<th>Subject</th>
								<th>Created Date</th>

							</tr>
						</thead>
						<tbody>
              <?php foreach($subjects as $key=>$subject){?>
							<tr>
								<td><?=$key+1;?></td>
                <td><a href="javascript:void(0)" onclick="openEditModal(<?//=$subject->id?>)"><i class="fa fa-edit"></i></a></td>
								<td><?= $subject->subject?></td>
								
                <td><?= date('d-m-Y',strtotime($subject->created_at));?></td>
							</tr>
            <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>

  <div class="modal fade" id="addSubjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/courses/store_subject')?>" id="addSubject" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
     
        <div class="form-group">
          <label for="name" class="col-form-label">Subject Name:</label>
          <input type="text" class="form-control" name="subject" id="subject">
        </div>
   
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>

      
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="editSubjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add <?=$page_title?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/courses/store_subject')?>" id="addSubject" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editSubjectModalData">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>

      
      </form>
    </div>
  </div>
</div>


<script>

$("form#addSubject").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      window.location="<?=base_url('subjects')?>";
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
function updateCourseStatus(user_id){
     var messageText  = "You want to Active this course?";
     var confirmText =  'Yes, Change it!';
     var message  ="Course status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/courses/update_status')?>', 
                method: 'POST',
                data: {userid: user_id},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }


  function openEditModal(subjectID){
    $.ajax({
       url: '<?=base_url('admin/courses/edit_subject_form')?>',
       type: 'POST',
       data: {subjectID},
       success: function (data) {
        $('#editSubjectModal').modal('show');
         $('#editSubjectModalData').html(data);
       }
     });
  }

  $(document).ready(function() {  
  
  $(".add-more").click(function(){  
      var html = $(".copy").html();  
      $(".after-add-more").after(html);  
  });  
  
  $("body").on("click",".remove",function(){   
      $(this).parents(".control-group").remove();  
  });  
  
  }); 
</script>