
 <div class="as-cart-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="woocommerce-notices-wrapper">
          <div class="woocommerce-message">Your Orders.</div>
        </div>
        <form action="#" class="woocommerce-cart-form">
          <table class="cart_table table table-striped"  style="width:100%" id="orderDataTable">
            <thead>
              <tr class="text-center">
                <th class="text-center" >Order Id</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Amount</th>
                <th class="text-center">Status</th>
                <th class="text-center">Payment Status</th>
                <th class="text-center">Date</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 	
				foreach($orders as $order){
					$status = $order->status==0 ? '<span class="text-danger">Pending</span>' : ($order->status==1 ? '<span class="text-warning">In-Progress</span>' : '<span class="text-success">Delivered</span>' ); 
          $url = $order->payment_status == 'UnPaid' ? 'href="'.base_url('Cart/pay/'.base64_encode($order->id)).'"' : 'href="javascript:void(0)"';
          $button_css = $order->payment_status == 'UnPaid' ? 'btn btn-danger' : 'btn btn-success';
					?>
				<tr>
					<td ><?= $order->orderID;?></td>
					<td ><?= $order->qty;?></td>
					<td ><?= $order->price?></td>
					<td><?=$status?></td>
          <td><?=$order->payment_status?></td>
					<td><?=date('d-m-Y',strtotime($order->created_at))?></td>
					<td><span class="mr-2"><a <?=$url?> class="<?=$button_css?>"><?=$order->payment_status == 'UnPaid' ? 'Pay' : $order->payment_status?></a></span><a href="javascript:void(0)" onclick="cartDetail('<?=$order->id?>')"class=""><i class="fa fa-eye"></i></a></td>
				</tr>
				<?php } ?>
				
				</tbody>
                </table>
             </form>
					</div>
				</div>
			</div>
	</section>
</div>

<div class="modal fade" id="cart_detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" >
        <div id="show_cart_detail"></div>

      <div class="modal-footer">
        <button type="button" onclick="CloseModal()" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </div>
      
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css">
<script>

  $( document ).ready(function() {
    new DataTable('#orderDataTable');
});
   function cartDetail(orderID){
    $.ajax({
        url: '<?=base_url('cart/cart_detail')?>',
        type: 'POST',
        data:{orderID},
        success: function (data) {
            $('#cart_detailModal').modal('show');
            $('#show_cart_detail').html(data);
        },
      });
   }
   function CloseModal(){
    $('#show_cart_detail').html('');
    $('#cart_detailModal').modal('hide');
   }
</script>