<section class="space-top space-extra-bottom">
     <div class="row justify-content-center">
     <div class="container">
     <div class="row">
     <?php foreach($doctors as $doctor){ 
        //print_r($doctor);
        ?>
          <div class="col-xl-3 col-lg-4 col-md-6">
            <div class="team-style2">
              <div class="team-img"><img src="<?=base_url($doctor->profile_pic);?>" alt="doctor" style="width:100%;height:308px;"></div>
              <div class="team-body">
              <span class="team-degi"><?=$doctor->user_type ?></span>
                <h3 class="team-name h5"><i class="fal fa-user"></i> <?=$doctor->name?></h3>
                <!-- <a href="mailto:<?=$doctor->email?>" class="team-degi"><i class="fal fa-envelope"></i> <?=$doctor->email ?></a>
                <a href="tel:<?=$siteinfo->site_contact?>" class="team-degi"><i class="fal fa-mobile"></i> <?=$doctor->contact ?></a> -->
                <p data-toggle="tooltip" title="<?=$doctor->discription ?>"><?=substr($doctor->discription,0,40) ?>...</p>
                <div class="team-social"><a href="<?=!empty($doctor->facebook_url) ? $doctor->facebook_url : 'javascript:void(0)'?>"><i class="fab fa-facebook-f"></i></a> <a href="<?=!empty($doctor->twitter_url) ? $doctor->twitter_url : 'javascript:void(0)'?>"><i class="fab fa-twitter"></i></a> <a href="<?=!empty($doctor->insta_url) ? $doctor->insta_url : 'javascript:void(0)' ?>"><i class="fab fa-instagram"></i></a> <a href="<?=!empty($doctor->linkedin_url) ? $doctor->linkedin_url : 'javascript:void(0)' ?>"><i class="fab fa-linkedin-in"></i></a></div>
              </div>
            </div>
          </div>
          <?php }?>
     </div>
     </div>
     </div>
    </section>

    <script>
//   function myFunction() {
//   var dots = document.getElementById("dots");
//   var moreText = document.getElementById("more");
//   var btnText = document.getElementById("myBtn");

//   if (dots.style.display === "none") {
//     dots.style.display = "inline";
//     btnText.innerHTML = "Read more"; 
//     moreText.style.display = "none";
//   } else {
//     dots.style.display = "none";
//     btnText.innerHTML = "Read less"; 
//     moreText.style.display = "inline";
//   }
// }
</script>
   