<!--=====================================-->
<!--=       Breadcrumb Area Start      =-->
<!--=====================================-->
<style>
  .notification{
  height: 41px!important;
  line-height: 29px!important;
  padding: 0 24px!important;
  }
</style>
<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=          Login Area Start         =-->
<!--=====================================-->
<section class="account-page-area section-gap-equal">
  <div class="container position-relative">
    <div class="row g-5 justify-content-center">
      <div class="col-lg-8">
        <div class="login-form-box">
          <h3 class="title">Reset Password</h3>
          <!-- <p>Don’t have an account? <a href="#">Sign up</a></p> -->
          <form action="<?=base_url('Ajax_controller/resetPassword')?>" id="resetPasswordForm">
            <div class="form-group">
              <label for="current-log-email">OTP*</label>
              <input type="text" name="otp" id="otp" placeholder="Enter OTP" onkeyup="checkEmail(this.value)"  maxlength="6" minlength="6"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              <p id="otp_message"></p>
            </div>
            <div class="form-group">
              <label for="current-log-password">New Password*</label>
              <input type="password" name="password" id="password" placeholder="Password">
              <span class="password-show"><i class="icon-76"></i></span>
            </div>
            <div class="form-group">
              <label for="current-log-password">Confirm Password*</label>
              <input type="password" name="confirmpassword" id="confirmpassword" placeholder="Password" onkeyup="checkPassword(this.value)">
              <span class="password-show"><i class="icon-76"></i></span>
              <p id="password_message"></p>
            </div>
            <div class="form-group">
              <button type="submit" class="edu-btn btn-medium">Change Password <i class="icon-4"></i></button>
            </div>
          </form>
        </div>
      </div>

    <ul class="shape-group">
      <li class="shape-1 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="Shape"></li>
      <li class="shape-2 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="Shape"></li>
      <li class="shape-3 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/counterup/shape-02.png')?>" alt="Shape"></li>
    </ul>
  </div>
</section>

<script>

  function checkEmail(otp){
  $.ajax({
  url: '<?=base_url('Ajax_controller/checkEmail')?>',
  type: 'POST',
  data: {otp},
  dataType: 'json',
  success: function (data) {
    if(data.status==200){
        $(':input[type="submit"]').prop('disabled', false);
      $('#otp_message').html('<span class="text-success">'+data.message+'</span>');
    }else{
        $(':input[type="submit"]').prop('disabled', true);
        $('#otp_message').html('<span class="text-danger">'+data.message+'</span>');
    }

  }
  });
  }


  function checkPassword(confirmPassword){
    var password = $('#password').val();
    if(confirmPassword == password){
        $(':input[type="submit"]').prop('disabled', false);
        $('#password_message').html('<span class="text-success">Password match</span>');
    }else{
        $(':input[type="submit"]').prop('disabled', true);
        $('#password_message').html('<span class="text-danger">Password not match</span>');
    }
  }


  $("form#resetPasswordForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  formData.append("email", '<?= $this->session->userdata('otp_email')?>');
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  setTimeout(function(){
            location.href="<?=base_url('my-account')?>";
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

    
</script>