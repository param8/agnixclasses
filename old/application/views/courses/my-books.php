
    <section class="course-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-8 col-xl-9" id="my_book_show">
 
            <!-- <div class="as-pagination pt-20">
              <ul>
                <li><a href="blog.html">1</a></li>
                <li><a href="blog.html">2</a></li>
                <li><a href="blog.html">Next<i class="far fa-long-arrow-right"></i></a></li>
              </ul>
            </div> -->
          </div>
          <div class="col-lg-4 col-xl-3">
            <aside class="course-sidebar">
              <!-- <div class="widget widget_search">
                <form class="search-form"><input type="text" placeholder="Search Here"> <button type="submit"><i class="far fa-search"></i></button></form>
              </div> -->
              <div class="widget">
                <h3 class="widget_title">Courses</h3>
                <div class="filter-list">
                  <ul>
                    <li>
                      <ul class="children">
                        <?php foreach($courses as $course){?>
                        <li>
                           <a href="javascript:void(0)" onclick="get_subject(<?=$course->id?>)" for="Design"><?=$course->course?></a> <i class="far fa-long-arrow-right"></i>
                           
                        </li>
                        <hr>
                        <?php } ?>
              
                      </ul>
                    </li>
                   
                  </ul>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </div>
    </section>
    <script>
      function get_subject(courseID){
        $.ajax({
        url: '<?=base_url('ajax_controller/get_subjects')?>',
        type: 'POST',
        data:{courseID},
        dataType: 'html',
          success: function (data) {
        $('#my_book_show').html(data);
          },
          error: function(){} 
        });
    }
    </script>
    