
       <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

       
        <!--=====================================-->
        <!--=      Contact Form Area Start      =-->
        <!--=====================================-->
        <section class="edu-section-gap contact-form-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form">
                            <div class="section-title section-center">
                                <h3 class="title">Basic Informations</h3>
                            </div>
                            <form class="rnt-contact-form" id="addCourses"  action="<?=base_url('courseStore')?>">
                                <div class="row row--10">
                                 
                                    <div class="form-group col-lg-6">
                                        <input class="form-control" type="text" name="course" id="course" placeholder="Course Name">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <select class="" name="category" id="category" onchange="categoryCheck(this.value)">
                                          <option value="">Select Category</option>
                                          <option value="Other">Other</option>
                                          <?php foreach($categories as $category){?>
                                            <option value="<?=$category->id?>"><?=ucwords($category->category)?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                         
                                    <div class="form-group col-lg-6" id="category_div" style="display:none;">
                                        <input class="form-control" type="text" name="category_name" id="category_name" onchange="checkCategory(this.value)" placeholder="Category  Name">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input class="form-control" type="text" name="subject" id="subject" placeholder="subject">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input class="form-control" type="number" name="free_demo" id="free_demo" max="10" min="1" placeholder="free demo" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                              
                                    <div class="form-group col-lg-6">
                                    
                                    <select class="" name="mode" id="mode">
                                    <option value="">Select Mode</option>
                                      <?php 
                                        $modes = array('Offline','Online','Both');
                                        foreach($modes as $mode){
                                      ?>
                                        <option value="<?=$mode?>"><?=$mode?></option>
                                        <?php } ?>
                                    </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="seats" id="seats" placeholder="Enter Seats" maxlength="4"   oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" onfocus="(this.type='date')" class="" min="<?= date('Y-m-d'); ?>" name="start_date" id="start_date" onchange="endDateValidate(this.value)" placeholder="Start Date">
                                    </div>
                                    <div class="form-group col-lg-6" id="endDate_div">
                                        <input type="text" class="form-control"  placeholder="End Date">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="fees" id="fees" placeholder="Enter fees"    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter discount in (%)"    oninput="this.value = this.value.replace(/[^0-9 .]/g, '').replace(/(\..*)\./g, '$1');">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="text" class="form-control" name="teacher" id="teacher" placeholder="Enter Teacher/Instrutor" >
                                    </div>
                                    <div class="form-group col-6">
                                        <input type="file" class="form-control" name="image" id="image" placeholder="Upload image">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <select class="js-example-placeholder-multiple form-control"  name="batch[]" id="batch" multiple="multiple" placeholder="select Batch">
                                          <?php foreach($batchs as $batch){?>
                                            <option value="<?=$batch->id?>"><?=$batch->batch .' '. $batch->time_formate?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <textarea name="description" id="description" cols="30" rows="6" placeholder="Type your description"></textarea>
                                    </div>
                             
                                    <div class="form-group col-12 text-center">
                                        <button class="rn-btn edu-btn submit-btn" name="submit" type="submit">Submit Now <i class="icon-4"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/cta/shape-04.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><span data-depth="1"></span></li>
                <li class="shape-4 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
            </ul>
        </section>
        <!--=====================================-->
        <!--=        Footer Area Start          =-->
        <!--=====================================-->
        <!-- Start Footer Area  -->

  <script>

    $("form#addCourses").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function (data) {
        if(data.status==200) {
        //$('.modal').modal('hide');
        //toastr.success(data.message);
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function(){
              location.reload();
        }, 1000) 
    
        }else if(data.status==403) {
        //toastr.error(data.message);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        $(':input[type="submit"]').prop('disabled', false);
        }else{
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function(){} 
      });
    });

    function categoryCheck(category){
      if(category=='Other'){
        $('#category_div').show();
      }else{
        $('#category_div').hide();
      }
    }

    function checkCategory(category){
      $.ajax({
      url: "<?=base_url('setting/checkCategory')?>",
      type: 'POST',
      data: {category},
      // cache: false,
      // contentType: false,
      // processData: false,
      dataType: 'json',
      success: function (data) {
        if(data.status==200) {
          $(':input[type="submit"]').prop('disabled', false);
        }else if(data.status==403) {
          Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
        $(':input[type="submit"]').prop('disabled', true);
        //toastr.error(data.message);
        }else{
        toastr.error('Something went wrong');
        }
      },
      error: function(){} 
      });
    }

    function endDateValidate(start_date){
      $('#endDate_div').html('<input type="date" class="" min="'+start_date+'" name="end_date" id="end_date" placeholder="End Date">');
    }
   </script>
      