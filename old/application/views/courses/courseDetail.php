<?php  
  $countFive = 0;
  $countFoure = 0;
  $countThree = 0;
  $countTwo = 0;
  $countOne = 0;
  $totalCountFive = 0;
  $totalCountFoure = 0;
  $totalCountThree = 0;
  $totalCountTwo = 0;
  $totalCountOne = 0;
  $percentageFive = 0;
  $percentageFoure = 0;
  $percentageThree = 0;
  $percentageTwo = 0;
  $percentageOne = 0;
  if(count($ratings_five) > 0){
   $countFive = count($ratings_five);
   $percentageFive = ($countFive * 100)/count($ratings);
   $totalCountFive = $countFive*5;
  }
  if(count($ratings_four) > 0){
   $countFoure = count($ratings_four);
   $percentageFoure = ($countFoure * 100)/count($ratings);
   $totalCountFoure = $countFoure*4;
  }
  if(count($ratings_three) > 0){
   $countThree = count($ratings_three);
   $percentageThree = ($countThree * 100)/count($ratings);
   $totalCountThree = $countThree*3;
  }
  if(count($ratings_two) > 0){
   $countTwo = count($ratings_two);
   $percentageTwo = ($countTwo * 100)/count($ratings);
   $totalCountTwo = $countTwo*2;
  }
  if(count($ratings_one) > 0){
   $countOne = count($ratings_one);
   $percentageOne = ($countOne * 100)/count($ratings);
   $totalCountOne = $countOne*1;
  }
  
  $sumTotalCount = $totalCountFive + $totalCountFoure + $totalCountThree + $totalCountTwo + $totalCountOne;
  $sumCount = $countFive + $countFoure + $countThree + $countTwo + $countOne;
  
  if($sumTotalCount > 0 && $sumCount > 0){
    $averageRating = $sumTotalCount/$sumCount;
  }else{
    $averageRating = 0;
  }
  $Totalrating = $averageRating;
  
  $RoundedRating = $Totalrating;
  
  
    
    $stars="";
    $newwhole = floor($RoundedRating);
    $fraction = $RoundedRating - $newwhole;
    if($newwhole > 0){
    for($s=1;$s<=$newwhole;$s++){
      $stars .= '<i class="icon-23"></i>';	
    }
    if($fraction >= 0.25){
      $stars .= '<i class="icon-23 n50"></i>';	
    }
    for($l=1;$l<=5-$newwhole;$l++){
      $stars .= '<i class="icon-23 text-secondary"></i>';
     }
    }else{
      for($s=1;$s<=5;$s++){
       $stars .= '<i class="icon-23 text-secondary"></i>';
      }
    }


    $earlier = new DateTime($course->start_date);
    $later = new DateTime($course->end_date);
    $totalDays =  $later->diff($earlier)->format("%a");
    if($course->fees > 0 && $course->discount > 0){
        $price =$course->fees - ($course->discount*$course->fees)/100;
        $discount =($course->discount*$course->fees)/100;
    }else{
        $price =$course->fees;
        $discount = 0;
        
    }

    $student_booked_course = array();
    foreach($booked_courses as $bookedCourse){
       $student_booked_course[] = $bookedCourse->studentID; 
    }
    //print_r($student_booked_course);die;
    // $booked_student_course = array();
    // foreach($booked as $bookedStudent){
    //     $booked_student_course[] = $bookedStudent->courseID; 
    // }
  ?>
    <section class="space-top space-extra-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="course-about">
              <div class="course-meta"><span><i class="fal fa-user"></i>Students <?=count($booked_courses) > 0 ? count($booked_courses) : 0?></span> 
              <!-- <span><i class="fal fa-eye"></i>View: 12K</span> -->
            </div>
              <h2 class="course-title"><?=$course->subject?></h2>
              <div class="course-info">
                <div class="row">
                  <!-- <div class="col-sm-6 col-xl course-info__box">
                    <div class="course-info__author">
                      <div class="media-body">
                        <span class="course-info__label">Instructor:</span>
                        <div class="course-info__info">Kevin Perry</div>
                      </div>
                    </div>
                  </div> -->
                  <div class="col-sm-6 col-xl course-info__box">
                    <span class="course-info__label">Course:</span>
                    <div class="course-info__info"><?=$course->course?></div>
                  </div>
                  <div class="col-sm-6 col-xl course-info__box">
                    <span class="course-info__label">Start Date:</span>
                    <div class="course-info__info"><?=date('d M, Y',strtotime($course->start_date))?></div>
                  </div>
                  <div class="col-sm-6 col-xl course-info__box">
                    <span class="course-info__label">Last Date:</span>
                    <div class="course-info__info"><?=date('d M, Y',strtotime($course->end_date))?></div>
                  </div>
                  <div class="col-sm-6 col-xl course-info__box">
                    <span class="course-info__label">Review:</span>
                    <div class="course-info__info">
                      <div class="star-rating" role="img" aria-label="Rated <?=$Totalrating?>"><span style="width:<?=$Totalrating * 20?>%">Rated <strong class="rating"><?=$Totalrating?></strong></span></div>
                      (<?=$Totalrating?>)
                    </div>
                  </div>
                </div>
              </div>
              <div class="course-img"><img src="<?=base_url($course->image)?>" alt="Course image" style="width:100%;height:400px;"></div>
            </div>
            <div class="course-tab-wrap">
              <div class="nav course-tab" id="v-pills-tab" role="tablist" aria-orientation="horizontal">
                <button class="nav-link active" id="Overview-tab" data-bs-toggle="pill" data-bs-target="#Overview" type="button" role="tab" aria-controls="Overview" aria-selected="true"><i class="fal fa-bookmark"></i>Overview</button> 
                <button class="nav-link" id="Curriculam-tab" data-bs-toggle="pill" data-bs-target="#Curriculam" type="button" role="tab" aria-controls="Curriculam" aria-selected="false"><i class="fal fa-book"></i>Curriculam</button>
                <!-- <button class="nav-link" id="Instructor-tab" data-bs-toggle="pill" data-bs-target="#Instructor" type="button" role="tab" aria-controls="Instructor" aria-selected="false"><i class="fal fa-user"></i>Instructor</button> -->
                <button class="nav-link" id="Reviews-tab" data-bs-toggle="pill" data-bs-target="#Reviews" type="button" role="tab" aria-controls="Reviews" aria-selected="false"><i class="fal fa-star"></i>Reviews</button>
              </div>
              <div class="tab-content course-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="Overview" role="tabpanel" aria-labelledby="Overview-tab">
                  <p><?=$course->description?></p>
                 
                 
               
                </div>
                <div class="tab-pane fade" id="Curriculam" role="tabpanel" aria-labelledby="Curriculam-tab">
                  <div class="lesson-box show">
                    <button class="lesson-box__toggler active"><i class="fal fa-info-circle"></i>Introduction</button>
                    <div class="lesson-box__body">
                      <ul class="lesson-box__list">
                        <?php 
                          if(!empty($course->subject_upload_pdf)){
                           $curriculams = json_decode($course->subject_upload_pdf);
                           foreach($curriculams as $curriculam){
                           foreach( $curriculam as $pdfTitle => $pdf){
                        ?>
                      
                        <!-- <li><a href="<?//=base_url('uploads/courses_pdf/'.$pdf)?>" class=""><i class="fas fa-play-circle"></i><?//=$pdfTitle?><i class="fal fa-lock"></i></a></li> -->
                        <li><a href="javascript:void(0)" onclick = "openPdf('<?=base_url('uploads/courses_pdf/'.$pdf)?>')" class="" ><i class="fas fa-file"></i> <?=$pdfTitle?><i class="fal fa-lock"></i></a></li>
                        <?php } } }else{?>
                          <li><h3 href="javascript:void(0)" class="text-danger text-center">No Curriculam found</3></li>
                          <?php } ?>
                      </ul>
                    </div>
                  </div>

            
                </div>
                <!-- <div class="tab-pane fade" id="Instructor" role="tabpanel" aria-labelledby="Instructor-tab">
                  <div class="course-author">
                    <div class="row align-items-center">
                      <div class="col-xl-auto">
                        <div class="author-img"><img src="assets/img/course/course-d-1-2.jpg" alt="Course"></div>
                      </div>
                      <div class="col-xl">
                        <div class="row justify-content-between align-items-center gy-3">
                          <div class="col-lg-auto">
                            <h4 class="author-name">Kory Anderson</h4>
                            <p class="author-degi">A certified instructor from Aduki</p>
                          </div>
                          <div class="col-auto">
                            <div class="team-social"><a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a> <a href="https://www.twitter.com/"><i class="fab fa-twitter"></i></a> <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a> <a href="https://www.linkedin.com/"><i class="fab fa-linkedin-in"></i></a></div>
                          </div>
                        </div>
                        <div class="course-author-middle">
                          <div class="row justify-content-between gy-3">
                            <div class="col-sm-6 col-md-auto">
                              <p class="team-info">Course: 4</p>
                              <p class="team-info">Students: 50</p>
                            </div>
                            <div class="col-sm-6 col-md-auto">
                              <p class="team-info"><strong>Reviews:</strong></p>
                              <p class="team-info"><span class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5</span></span>5.00 (70)</p>
                            </div>
                            <div class="col-sm-6 col-md-auto">
                              <p class="team-info"><strong>Experiance:</strong></p>
                              <p class="team-info">10 Years</p>
                            </div>
                          </div>
                        </div>
                        <p class="author-text">Conveniently harness impactful testing procedures via synergize distributed best practices without process-centric solutions. Distinctively simplify magnetic quality vectors through intermandated channels.</p>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="tab-pane fade" id="Reviews" role="tabpanel" aria-labelledby="Reviews-tab">
                  <div class="reviews-summary">
                    <div class="reviews-summary__top">
                      <div class="reviews-summary__average"><?=$averageRating?></div>
                      <div class="reviews-summary__rating">
                        <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:<?=$Totalrating * 20?>%">Rated <strong class="rating">5.00</strong> out of 5 based on <span class="rating">1</span> customer rating</span></div>
                      </div>
                      <div class="reviews-summary__count">Total <?=count($ratings)?> Ratings</div>
                    </div>
                    <div class="reviews-summary__bars">

                    <?php 
                          for($k=1; $k<=5; $k++){
                          
                          ?>
                      <div class="reviews-bar">
                        <label class="reviews-bar__title" title="5 Stars"><i class="fal fa-star"></i><?=$k?></label>
                        <div class="reviews-bar__graph" title="<?= $k==5 ? $percentageFive : ($k==4 ? $percentageFoure : ($k==3 ? $percentageThree: ($k==2 ? $percentageTwo : $percentageOne ) ));?>">
                          <div class="bar" style="width: <?= $k==5 ? $percentageFive : ($k==4 ? $percentageFoure : ($k==3 ? $percentageThree: ($k==2 ? $percentageTwo : $percentageOne ) ));?>%"></div>
                        </div>
                        <div class="reviews-bar__percentage"><?= $k==5 ? $countFive : ($k==4 ? $countFoure : ($k==3 ? $countThree: ($k==2 ? $countTwo : $countOne ) ));?> rating</div>
                      </div>
                 <?php } ?>
                  
                  
                
                    </div>
                  </div>
                  <div class="as-comments-wrap course-reviews">
                    <ul class="comment-list">
                    <?php 
                        if(count($ratings)>0){
                         foreach($ratings as $rating){
                        ?>
                      <li class="review as-comment-item">
                        <div class="as-post-comment">
                          <div class="comment-top">
                            <div class="comment-avater"><img src="<?=base_url($rating->profile_pic)?>" alt="Comment Author" style="width:100px;height:100px;"></div>
                          </div>
                          <div class="comment-content">
                            <h4 class="name h4"><?=$rating->name?></h4>
                            <span class="commented-on"><?=date('M d, Y',strtotime($rating->created_at))?></span>
                            <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:<?=$rating->rating * 20?>%">Rated <strong class="rating">5.00</strong> out of 5 based on <span class="rating">1</span> customer rating</span></div>
                            <p class="text"><?=$rating->description?></p>
                          </div>
                        </div>
                      </li>
                      <?php } } ?>
                    </ul>
                  </div>
                  <?php if($this->session->userdata('user_type')!='Teacher'){?>
                  <div class="comment-form-area">
                    <h3 class="heading-title">Write a Review</h3>
                    <form class="comment-form" id="addRating" action="<?=base_url('Courses/store_rating')?>" method="POST">
                      <div class="">
                        <h6 class="title">Rating Here</h6>
                        <div class="input-group">
                        <!-- <div class="star-rating" role="img" ><span style="width:100%"></span></div> -->
                          <?php 
                            $rating_stars = array(1,2,3,4,5) ;
                             foreach($rating_stars as $stars){
                            ?>
                          <span  class="" id="rating_course<?=$stars?>"  onclick="checkedStar(<?=$stars?>)"><i class="fal fa-star rating-color<?=$stars?>" style="color:red"></i></span>
                          <?php } ?>
                          <input type="hidden" name="rating_course" id="rating_course" value="">
                        </div>
                      </div>
                      <div class="row g-5">
                        <!-- <div class="form-group col-lg-6">
                          <input type="text" name="title" id="title" placeholder="Review Title">
                          </div> -->
                        <div class="form-group col-12">
                          <textarea name="description" id="description" cols="30" rows="5" placeholder="Review summary"></textarea>
                        </div>
                        <?php  if(count($dublicateRatings)<=0){?>
                        <div class="form-group col-12">
                          <?=$this->session->userdata('email') ? '<button  type="submit" class="as-btn  btn-sm">Submit Review <i class="icon-4"></i></button>' : '<a  href="'.base_url('my-account').'" class="edu-btn submit-btn">Submit Review <i class="icon-4"></i></a>'; ?>
                        </div>
                        <?php } ?>
                      </div>
                    </form>
                  </div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- <div class="course-about">
              <h3 class="h6 fw-semibold mt-n2">What you will learn?</h3>
              <div class="list-style3">
                <ul>
                  <li>Learn New Things</li>
                  <li>Skills Update</li>
                  <li>Confident using Any software</li>
                  <li>Anyone who is finding a chance to get the promotion</li>
                </ul>
              </div>
            </div> -->
          </div>
          <div class="col-lg-4">
            <div class="course-about">
              <!-- <div class="course-img"><img src="assets/img/course/course-s-1-1.jpg" alt="Course image"> <a href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" class="play-btn popup-video style2"><i class="fas fa-play"></i></a></div> -->
              <h4 class="price text-theme">₹ <?=round($price)?> /<del>₹ <?=$course->fees?></del></h4>
              <?php if($this->session->userdata('user_type')=='Student'){
                   if(in_array($this->session->userdata('id'),$student_booked_course)){
                    ?>
                    <div class="read-more-btn">
                         <a href="javascript:void(0)"  class="as-btn text-danger">Alraedy Booked <i class="icon-3"></i></a>
                    </div>
                    <?php
                  }else{
                 ?>
               
                  <?php if($course->remaning_seats<=0){?>
                  <a href="javascript:void(0)"  class="as-btn style2 btn-danger">Out of stock </a>
                  <?php } else{?>
                  <a href="javascript:void(0)" onclick="addCart(<?=$course->id?>,'Course',<?=$price?>)" class="as-btn">Book Now </a>
                  <?php }  } } ?>
                <?php if(empty($this->session->userdata('user_type'))){?>
                
                  <a href="javascript:void(0)" onclick="redirectSigin()"  class="as-btn">Book Now </a> 
              
                <?php } ?>
              
              <h4 class="fw-semibold h6">Course Information</h4>
              <div class="list-style4">
                <ul>
                  <!-- <li><span class="list-title"><i class="fal fa-user"></i>Instructor:</span>Kevin Perry</li> -->
                  <li><span class="list-title"><i class="fal fa-file-alt"></i>Free Demo:</span><?=$course->free_demo?></li>
                  <li><span class="list-title"><i class="fal fa-clock"></i>Duration:</span><?=$totalDays?> Days</li>
                  <li><span class="list-title"><i class="fal fa-tag"></i>Mode:</span><?=$course->mode?></li>
                  <li><span class="list-title"><i class="fal fa-globe"></i>Batch:</span><?=$course->batch?></li>
                  <li><span class="list-title"><i class="fal fa-book"></i>Subject:</span><?=$course->subject?></li>
                  <li><span class="list-title"><i class="fal fa-puzzle-piece"></i>Total Seats:</span><?=$course->seats?></li>
                  <li><span class="list-title"><i class="fal fa-puzzle-piece"></i>Remaning Seats:</span><?=$course->remaning_seats?></li>
                </ul>
              </div>
              <!-- <a href="https://www.facebook.com/" class="as-btn style6"><i class="fal fa-share-alt me-2"></i>Share This Course</a> -->
            </div>
            <div id="myFrame" class="mt-5"></div>
          </div>
        </div>
      </div>
    </section>

    <script>
  function checkedStar(val){
    var i;
    var j;
    for(j = 1; j <= 5; j++){
         $('.rating-color'+j).css('color','red');
      }

      for(i = 1; i <= val; i++){

         $('.rating-color'+i).css('color','#3767b8');
        
      }

      $('#rating_course').val(val);
      //console.log(count);
  }
  
  $("form#addRating").submit(function(e) {
    $(':input[type="submit"]').prop('disabled', true);
    e.preventDefault();    
    var formData = new FormData(this);
    formData.append("courseID", <?php echo $course->id?>);
    $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
      //$('.modal').modal('hide');
      //toastr.success(data.message);
      Swal.fire({
        icon: 'success',
        title: 'Success...',
        text: data.message,
        //footer: '<a href="">Why do I have this issue?</a>'
      })
      $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function(){
            location.reload();
      }, 1000) 
  
      }else if(data.status==403) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
     // toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong',
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function(){} 
    });
  });
  
  
  function addStudentCourse(){
  var courseID = <?=$course->id?>;
  var price = <?=$price?>;
  var batch  = '<?=$course->batch?>';
  
  $.ajax({
    url: "<?=base_url('Courses/addStudentCourse')?>",
    type: 'POST',
    data: {courseID,price,batch},
    // cache: false,
    // contentType: false,
    // processData: false,
    dataType: 'json',
    success: function (data) {
      if(data.status==200) {
        Swal.fire({
        icon: 'success',
        title: 'Success...',
        text: data.message,
        //footer: '<a href="">Why do I have this issue?</a>'
      })
      $(':input[type="submit"]').prop('disabled', false);
          setTimeout(function(){
            location.reload();
      }, 1000) 
  
      }else if(data.status==403) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      //toastr.error(data.message);
      $(':input[type="submit"]').prop('disabled', false);
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong',
          //footer: '<a href="">Why do I have this issue?</a>'
        })
      $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function(){} 
    });
  
  }
  
  function redirectSigin(){
  Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Please login to book courses thanks',
  //footer: '<a href="">Why do I have this issue?</a>'
  })
  setTimeout(function(){
      window.location="<?=base_url('my-account')?>";
  }, 2000) 
  }

  function openPdf(pdf)
  {
   $('#myFrame').html('<embed  src="'+pdf+'"  style="width:100%;height:100%">')
  }
</script>
    