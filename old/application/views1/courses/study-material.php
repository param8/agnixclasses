 <section class="space-top space-extra-bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
           
            <div class="course-tab-wrap">
              <div class="nav course-tab" id="v-pills-tab" role="tablist" aria-orientation="horizontal"> 
                <button class="nav-link active" id="Curriculam-tab" data-bs-toggle="pill" data-bs-target="#Curriculam" type="button" role="tab" aria-controls="Curriculam" aria-selected="false"><i class="fal fa-book"></i><?=$subject->course?></button>
              </div>
              <div class="tab-content course-content" id="v-pills-tabContent">
       
                <div class="tab-pane fade show active" id="Curriculam" role="tabpanel" aria-labelledby="Curriculam-tab">
                  <div class="lesson-box show ">
                    <button class="lesson-box__toggler active"><i class="fal fa-info-circle"></i><?=$subject->subject?></button>
                    <div class="lesson-box__body">
                      <ul class="lesson-box__list">
                        <?php 
                          if($subject->subject_upload_pdf!=""){
                           $curriculams = json_decode($subject->subject_upload_pdf);
                           foreach($curriculams as $curriculam){
                           foreach( $curriculam as $pdfTitle => $pdf){
                        ?>
                      
                        <li><a href="javascript:void(0)" onclick = "openPdf('<?=base_url('uploads/courses_pdf/'.$pdf)?>')" class="" ><i class="fas fa-file"></i> <?=$pdfTitle?><i class="fal fa-lock"></i></a></li>
                        <?php } } }else{?>
                          <li><h3 href="javascript:void(0)" class="text-danger text-center">No Study Material found</3></li>
                          <?php } ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
       
          </div>
          <div class="col-lg-6">
           
            <div class="" id="myFrame"></div>
       
          </div>
          
        </div>
      </div>
    </section>
    <script>
      function openPdf(pdf)
        {

          $('#myFrame').html('<embed  src="'+pdf+'"  style="width:100%;height:100%">')
        }
    </script>

   