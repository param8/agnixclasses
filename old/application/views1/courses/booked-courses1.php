
        <!--=====================================-->
        <!--=       Breadcrumb Area Start      =-->
        <!--=====================================-->


        <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
                       
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        <!--=====================================-->
        <!--=        Courses Area Start         =-->
        <!--=====================================-->
        <div class="edu-course-area course-area-1 gap-tb-text">
            <div class="container">


                <div class="edu-sorting-area">
                    <div class="sorting-left">
                        <h6 class="showing-text">We found <span><?=count($booked_courses)?></span> <?=strtolower($page_title)?> available </h6>
                    </div>
      
                </div>

                <div class="row g-5">
                    <!-- Start Single Course  -->
                    <?php 
                      foreach($booked_courses as $booked){
                        // $earlier = new DateTime($course->start_date);
                        // $later = new DateTime($course->end_date);
                        // $totalDays =  $later->diff($earlier)->format("%a");
                        // if($course->fees > 0 && $course->discount > 0){
                        //     $price =$course->fees - ($course->discount*$course->fees)/100;
                        // }else{
                        //     $price =$course->fees;
                        // }
                        
                         
                    ?>
                    <div class="col-md-6 col-lg-4 col-xl-3" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
                        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
                            <div class="inner">
                                <div class="thumbnail">
                                    <a href="#">
                                        <img src="<?=base_url($booked->profile_pic)?>" alt="Course Meta" style="height:269px;">
                                    </a>
                                    <!-- <div class="time-top">
                                        <span class="duration"><i class="icon-61"></i><?//=$totalDays?> Days</span>
                                    </div> -->
                                </div>
                                <div class="content">
                                    <!-- <span class="course-level">Beginner</span> -->
                                    <?php if($this->session->userdata('user_type')!='Student'){?>
                                    <h6 class="title">
                                        <a href="#">Student :- <?=$booked->student_name?></a>
                                    </h6>
                                    <?php } if($this->session->userdata('user_type')!='Institute'){?>
                                    <h6 >
                                        <a href="#">Institute :- <?=$booked->institute_name?></a>
                                    </h6>
                                    <?php } ?>
                                    <h6 >
                                        <a href="#">Category :- <?=$booked->category?></a>
                                    </h6>
                                    <h6 >
                                        <a href="#">Course :- <?=$booked->course_name?></a>
                                    </h6>
                               
                                    <div class="course-price">Price :- ₹ <?=$booked->price?></div>

                                    <h6 class="title">Date :- <?=date('d-m-Y',strtotime($booked->created_at))?></h6>
                                <div>
                                    <?php if($booked->status==0){?>
                                    <button class="btn btn-danger" <?=$this->session->userdata('user_type')=='Institute' ? 'onclick="changeStatus('.$booked->id.')"' : '' ;?> >Pending</button>
                                    <?php }else{?>
                                    <button class="btn btn-success">Active</button>
                                    <?php } ?>
                                </div>
                                </div>
                            </div>
                          
             
                        </div>
                    </div>
                    <?php } ?>
                    <!-- End Single Course  -->    
                </div>
                <!-- <div class="load-more-btn" data-sal-delay="100" data-sal="slide-up" data-sal-duration="1200">
                    <a href="course-one.html" class="edu-btn">Load More <i class="icon-56"></i></a>
                </div> -->
            </div>
        </div>
        <!-- End Course Area -->
        <!--=====================================-->
        <!--=        Footer Area Start          =-->
        <!--=====================================-->
        <!-- Start Footer Area  -->

        <script>
            function changeStatus(id){
                $.ajax({
            url: "<?=base_url('Courses/studentCourseStatus')?>",
            type: 'POST',
            data: {id},
            // cache: false,
            // contentType: false,
            // processData: false,
            dataType: 'json',
            success: function (data) {
              if(data.status==200) {
              //$('.modal').modal('hide');
              toastr.success(data.message);
              $(':input[type="submit"]').prop('disabled', false);
                  setTimeout(function(){
                    location.reload();
              }, 1000) 
          
              }else if(data.status==403) {
              toastr.error(data.message);
              $(':input[type="submit"]').prop('disabled', false);
              }else{
              toastr.error('Something went wrong');
              $(':input[type="submit"]').prop('disabled', false);
              }
            },
            error: function(){} 
            });
            }
        </script>
       