<style>

.notification{
  height: 41px!important;
    line-height: 29px!important;
    padding: 0 24px!important;

}
</style>

<div class="edu-breadcrumb-area">
  <div class="container">
    <div class="breadcrumb-inner">
      <div class="page-title">
        <h1 class="title"><?=$page_title?></h1>
      </div>
      <ul class="edu-breadcrumb">
        <li class="breadcrumb-item"><a href="index-one.html">Home</a></li>
        <li class="separator"><i class="icon-angle-right"></i></li>
        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
      </ul>
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1">
      <span></span>
    </li>
    <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
    <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
    <li class="shape-4">
      <span></span>
    </li>
    <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
  </ul>
</div>
<!--=====================================-->
<!--=        Courses Area Start         =-->
<!--=====================================-->
<div class="edu-course-area course-area-1 section-gap-equal">
  <div class="container">
    <div class="row ">
      <div class="col-lg-4">
        <select id="courses_booked" class="dropdown_css" name="courses_booked" onchange="setBookedCourses(this.value)">
        <option value="">Select Course</option>
        <?php foreach($courses as $course){?>
          <option value="<?=$course->id?>"  <?=$this->session->userdata('courses_booked') == $course->id ? 'selected' : ''?>><?=$course->course?></option>
          <?php } ?>
        </select>
        <p class="text-danger" onclick="resetCourseBooked()" style="cursor:pointer"> Reset Course</p>
      </div>
       <?php if($this->session->userdata('user_type')=='Institute'){?>
      <div class="col-lg-4">
      <button type="button" class=" edu-btn btn-medium btn btn-primary notification" data-toggle="modal" data-target="#notification">Send Notification</button>
      </div>
      <?php } ?>
    </div>
    <div class="row g-5">
      <div class="col-lg-2 order-lg-2">
        <!-- <div class="course-sidebar-2">
          <div class="edu-course-widget widget-category">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Categories</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check1" checked>
                          <label for="cat-check1">Art &amp; Design <span>(7)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check2">
                          <label for="cat-check2">Development <span>(2)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check3">
                          <label for="cat-check3">Business <span>(3)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check4">
                          <label for="cat-check4">Marketing <span>(6)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check5">
                          <label for="cat-check5">Academics <span>(2)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="cat-check6">
                          <label for="cat-check6">Data Science <span>(9)</span></label>
                      </div>
                  </div>
              </div>
          </div>
          <div class="edu-course-widget widget-instructor">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Instructor</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="inst-check1">
                          <label for="inst-check1">Madge Alvarez <span>(2)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="inst-check2">
                          <label for="inst-check2">Tyler Hardy <span>(14)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="inst-check3">
                          <label for="inst-check3">Dabiv Matina <span>(10)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="inst-check4">
                          <label for="inst-check4">Robbin Lee <span>(5)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="inst-check5">
                          <label for="inst-check5">Donald Logan <span>(2)</span></label>
                      </div>
                  </div>
              </div>
          </div>
          <div class="edu-course-widget widget-level">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Level</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="level-check1">
                          <label for="level-check1">All Levels <span>(23)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="level-check2">
                          <label for="level-check2">Beginner <span>(7)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="level-check3">
                          <label for="level-check3">High <span>(10)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="level-check4">
                          <label for="level-check4">Intermediate <span>(13)</span></label>
                      </div>
                  </div>
              </div>
          </div>
          <div class="edu-course-widget widget-language">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Language</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="lang-check1">
                          <label for="lang-check1">English <span>(12)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="lang-check2">
                          <label for="lang-check2">Spanish <span>(7)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="lang-check3">
                          <label for="lang-check3">German <span>(5)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="lang-check4">
                          <label for="lang-check4">Russian <span>(3)</span></label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="lang-check5">
                          <label for="lang-check5">Korean <span>(2)</span></label>
                      </div>
                  </div>
              </div>
          </div>
          <div class="edu-course-widget widget-price">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Price</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="price-check1">
                          <label for="price-check1">All Price</label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="price-check2">
                          <label for="price-check2">Free</label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="price-check3">
                          <label for="price-check3">Low to High</label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="price-check4">
                          <label for="price-check4">High to Low</label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="price-check5">
                          <label for="price-check5">Paid</label>
                      </div>
                  </div>
              </div>
          </div>
          <div class="edu-course-widget widget-rating">
              <div class="inner">
                  <h5 class="widget-title widget-toggle">Rating</h5>
                  <div class="content">
                      <div class="edu-form-check">
                          <input type="checkbox" id="rating-check1">
                          <label for="rating-check1">
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <span>(7)</span>
                          </label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="rating-check2">
                          <label for="rating-check2">
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="off icon-23"></i>
                              <span>(2)</span>
                          </label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="rating-check3">
                          <label for="rating-check3">
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <span>(3)</span>
                          </label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="rating-check4">
                          <label for="rating-check4">
                              <i class="icon-23"></i>
                              <i class="icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <span>(6)</span>
                          </label>
                      </div>
                      <div class="edu-form-check">
                          <input type="checkbox" id="rating-check5">
                          <label for="rating-check5">
                              <i class="icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <i class="off icon-23"></i>
                              <span>(2)</span>
                          </label>
                      </div>
                  </div>
              </div>
          </div>
          </div> -->
      </div>
      <div class="col-lg-10 col-pr--35 order-lg-1">
        <div class="edu-sorting-area">
          <div class="sorting-left">
            <h6 class="showing-text">We found <span><span><?=count($booked_courses)?></span> <?=strtolower($page_title)?> available</h6>
          </div>
          <!-- <div class="sorting-right">
            <div class="layout-switcher">
                <label>Grid</label>
                <ul class="switcher-btn">
                    <li><a href="course-one.html" class=""><i class="icon-53"></i></a></li>
                    <li><a href="course-four.html" class="active"><i class="icon-54"></i></a></li>
                </ul>
            </div>
            <div class="edu-sorting">
                <div class="icon"><i class="icon-55"></i></div>
                <select class="edu-select">
                    <option>Filters</option>
                    <option>Low To High</option>
                    <option>High Low To</option>
                    <option>Last Viewed</option>
                </select>
            </div>
            </div> -->
        </div>
        <?php 
          foreach($booked_courses as $booked){
            $earlier = new DateTime($booked->start_date);
            $later = new DateTime($booked->end_date);
            $totalDays =  $later->diff($earlier)->format("%a");
            if($booked->fees > 0 && $booked->discount > 0){
                $price =$booked->fees - ($booked->discount*$booked->fees)/100;
            }else{
                $price =$booked->fees;
            }
            
            $this->db->where(array('course_rating.courseID' => $booked->id));
          $allOverRating =  $this->db->get('course_rating')->result();
          
          $this->db->where(array('course_rating.courseID' => $booked->id,'course_rating.rating'=>5));
          $five =  $this->db->get('course_rating')->result();
          
          $this->db->where(array('course_rating.courseID' => $booked->id,'course_rating.rating'=>4));
          $four =  $this->db->get('course_rating')->result();
          
          $this->db->where(array('course_rating.courseID' => $booked->id,'course_rating.rating'=>3));
          $three =  $this->db->get('course_rating')->result();
          
          $this->db->where(array('course_rating.courseID' => $booked->id,'course_rating.rating'=>2));
          $two =  $this->db->get('course_rating')->result();
          
          
          $this->db->where(array('course_rating.courseID' => $booked->id,'course_rating.rating'=>1));
          $one =  $this->db->get('course_rating')->result();
          $count_Five = 0;
          $count_Foure = 0;
          $count_Three = 0;
          $count_Two = 0;
          $count_One = 0;
          $total_CountFive = 0;
          $total_CountFoure = 0;
          $total_CountThree = 0;
          $total_CountTwo = 0;
          $total_CountOne = 0;
          if(count($five) > 0){
          $count_Five = count($five);
          $total_CountFive = $count_Five*5;
          }
          if(count($four) > 0){
          $count_Foure = count($four);
          $total_CountFoure = $count_Foure*4;
          }
          if(count($three) > 0){
          $count_Three = count($three);
          $total_CountThree = $count_Three*3;
          }
          if(count($two) > 0){
          $count_Two = count($two);
          $total_CountTwo = $count_Two*2;
          }
          if(count($one) > 0){
          $count_One = count($one);
          $total_CountOne = $count_One*1;
          }
          $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
          $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
          if($sumTotalCountRating > 0 && $sumCountRating > 0){
          $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
          }else{
          $TotalratingAvg = 0;
          }
          
          $stars_rating="";
          $newwholeRating = floor($TotalratingAvg);
          $fractionRating = $TotalratingAvg - $newwholeRating;
          if($newwholeRating > 0){
          for($s=1;$s<=$newwholeRating;$s++){
          $stars_rating .= '<i class="icon-23"></i>';	
          }
          if($fractionRating >= 0.25){
          $stars_rating .= '<i class="icon-23 n50"></i>';	
          }
          }
          else{
          for($s=1;$s<=5;$s++){
          $stars_rating .= '<i class="icon-23 text-secondary"></i>';
          }
          }  
             
          ?>
        <div class="edu-course course-style-4 course-style-9">
          <div class="inner">
            <div class="thumbnail">
              <a href="#">
              <img src="<?=base_url($booked->profile_pic)?>" alt="Course Meta" style="height:200px;widht:200px;">
              </a>
            </div>
            <div class="content all_d"> 
              <span class="detail_f">
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$booked->fees?></del></div>
              <h6 class="title">
                <a><i class="icon-25"></i> <?=$this->session->userdata('user_type')=='Student' ? $booked->institute_name : ($this->session->userdata('user_type')=='Institute' ? $booked->student_name : '') ?></a>
              </h6>
              <h6 class="title">
                <a><i class="icon-envelope"> </i><?=$this->session->userdata('user_type')=='Student' ? $booked->institute_email : ($this->session->userdata('user_type')=='Institute' ? $booked->student_email : '') ?></a>
              </h6>
              <h6 class="title">
                <a><i class="icon-phone"> </i><?=$this->session->userdata('user_type')=='Student' ? $booked->institute_contact : ($this->session->userdata('user_type')=='Institute' ? $booked->student_contact : '') ?></a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?//=$stars_rating?>
                </div>
                <!-- <span class="rating-count">(<?//=$TotalratingAvg .'/'.count($allOverRating)?> Rating)</span> -->
              </div>
        </span>
              <span class="detail_s">
              <p><b>Batch</b> <?=$booked->course_batch .' '.$booked->time_formate?></p>
              <p title="<?=$booked->course_discription?>"><?=substr($booked->course_discription,0,200)?></p>
              <ul class="course-meta">
                <li><i class="icon-24"></i> <?php if($booked->status==0){?>
                  <button class="btn btn-lg btn-danger" <?=$this->session->userdata('user_type')=='Institute' ? 'onclick="changeStatus('.$booked->id.')"' : '' ;?> >Pending</button>
                  <?php }else{?>
                  <button class="btn btn-success">Active</button>
                  <?php } ?>
                </li>
                <!-- <li><i class="icon-25"></i>20 Students</li> -->
              </ul>
              <span>

            </div>
          </div>
          <div class="hover-content-aside">
            <div class="content">
              <span class="course-level"><?=$booked->category?></span>
              <h5 class="title">
                <a href="#"><?=$booked->course_name?></a>
              </h5>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?//=$stars_rating?>
                </div>
                <!-- <span class="rating-count">(<?//=$TotalratingAvg ?>)</span> -->
              </div>
              <ul class="course-meta">
                <li class="text-success"><?=date('d M Y',strtotime($booked->start_date))?> <span >Batch Start</span></li>
                <li class="text-danger"><?=date('d M Y',strtotime($booked->end_date))?> <span>Batch End</span></li>
                <li class="text-warning"><?=$totalDays?> Days</li>
                <li class="text-primary">Mode :- <?=$booked->mode?> </li>
              </ul>
              <!-- <div class="course-feature">
                <h6 class="title">What You’ll Learn?</h6>
                <ul>
                    <li>Learn to use Python professionally, learning both Python 2 & Python 3!</li>
                    <li>Build 6 beautiful real-world projects for your portfolio (not boring toy </li>
                    <li>Understand the Theory behind Vue.js and use it in Real Projects</li>
                </ul>
                </div> -->
              <div class="button-group">
                <a href="<?=base_url('course-details/'.base64_encode($booked->id))?>" class="edu-btn btn-medium">Course Detail</a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
    <!-- <ul class="edu-pagination ">
      <li><a href="#" aria-label="Previous"><i class="icon-west"></i></a></li>
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li class="more-next"><a href="#"></a></li>
      <li><a href="#">8</a></li>
      <li><a href="#" aria-label="Next"><i class="icon-east"></i></a></li>
      </ul> -->
  </div>
</div>

<!-- ved-modal -->
<div class="modal fade" id="notification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Send Notification</h5>
        <button type="button" class="close btn-secondary" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('setting/sendNotification')?>" method="post" id="notificationForm">
          <div class="form-group">
          <label for="message-text" class="col-form-label"><b>Courses:</b></label>
          <select id="courses" class="dropdown_css" name="courses" >
          <option value="">Select Course</option>
          <?php foreach($courses as $course){?>
            <option value="<?=$course->id?>"  <?=$this->session->userdata('courses_booked') == $course->id ? 'selected' : ''?>><?=$course->course?></option>
            <?php } ?>
          </select>
          </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label"><b>Message:</b></label>
              <textarea class="form-control" id="message" name="message"></textarea>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-lg  " data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-lg  ">Send Notification</button>
        </div>
      </div>
  </form>
  </div>
</div>
<!-- ved -->



<script>
      function resetCourseBooked(){
      $.ajax({
       url: '<?=base_url("setting/resetCourseBooked")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setBookedCourses(courses_booked){
      $.ajax({
       url: '<?=base_url("setting/setBookedCourses")?>',
       type: 'POST',
       data: {courses_booked},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function changeStatus(id){
                $.ajax({
            url: "<?=base_url('Courses/studentCourseStatus')?>",
            type: 'POST',
            data: {id},
            // cache: false,
            // contentType: false,
            // processData: false,
            dataType: 'json',
            success: function (data) {
              if(data.status==200) {
                Swal.fire({
                icon: 'success',
                title: 'Success...',
                text: data.message,
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
                  setTimeout(function(){
                    location.reload();
              }, 1000) 
          
              }else if(data.status==403) {
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: data.message,
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
              }else{
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong',
                //footer: '<a href="">Why do I have this issue?</a>'
              })
              $(':input[type="submit"]').prop('disabled', false);
              }
            },
            error: function(){} 
            });
            }

            // function openModal(){
            //   $('#notificationModal').modal('show');
            // }
            // function closeModal(){
            //   $('#notificationModal').modal('hide');
            // }

     $("form#notificationForm").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
        Swal.fire({
          icon: 'success',
          title: 'Success...',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
        })
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.reload();
				}, 2000) 
		
				}else if(data.status==403) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: data.message,
            //footer: '<a href="">Why do I have this issue?</a>'
          })
				$(':input[type="submit"]').prop('disabled', false);
				}else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong',
            //footer: '<a href="">Why do I have this issue?</a>'
          })
				//toastr.error('Something went wrong');
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});
</script>