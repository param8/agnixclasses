
       <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

       
        <!--=====================================-->
        <!--=      Contact Form Area Start      =-->
        <!--=====================================-->
        <section class="edu-section-gap contact-form-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form">
                            <div class="section-title section-center">
                                <h3 class="title">Basic Informations</h3>
                            </div>
                            <form class="rnt-contact-form" id="addCourses"  action="<?=base_url('admin/category/store')?>">
                                <div class="row row--10">
                                    <div class="form-group col-lg-12">
                                        <input type="text" class="form-control" name="category" id="category" placeholder="Enter category" >
                                    </div>
                               
                             
                                    <div class="form-group col-12 text-center">
                                        <button class="rn-btn edu-btn submit-btn" name="submit" type="submit">Submit Now <i class="icon-4"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/cta/shape-04.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><span data-depth="1"></span></li>
                <li class="shape-4 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
            </ul>
        </section>
        <!--=====================================-->
        <!--=        Footer Area Start          =-->
        <!--=====================================-->
        <!-- Start Footer Area  -->

  <script>

    $("form#addCourses").submit(function(e) {
      $(':input[type="submit"]').prop('disabled', true);
      e.preventDefault();    
      var formData = new FormData(this);
      $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function (data) {
        if(data.status==200) {
        //$('.modal').modal('hide');
        toastr.success(data.message);
        $(':input[type="submit"]').prop('disabled', false);
            setTimeout(function(){
              location.reload();
        }, 1000) 
    
        }else if(data.status==403) {
        toastr.error(data.message);
        $(':input[type="submit"]').prop('disabled', false);
        }else{
        toastr.error('Something went wrong');
        $(':input[type="submit"]').prop('disabled', false);
        }
      },
      error: function(){} 
      });
    });
   </script>
      