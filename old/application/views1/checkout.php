<div class="as-checkout-wrapper space-top space-extra-bottom">
  <div class="container">
    <div class="row">
     
        <div class="col-lg-6">
        <form id="addAddress" action="<?=base_url('cart/store_shippingAddress')?>" method="post" class="woocommerce-checkout mt-40">
          <h2 class="h4">Billing Details</h2>
          <div class="row">
            <div class="col-md-12 form-group">
              <input type="text" class="form-control" name="name" id="name" placeholder="Enter Full Name">
            </div>
            <div class="col-md-6 form-group">
              <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
            </div>
            <div class="col-md-6 form-group">
              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Phone number">
            </div>
            <div class="col-12 form-group">
              <input type="text" class="form-control" name="street" id="street" placeholder="Street Address"> 
              <input type="text" class="form-control" name="address" id="address" placeholder="Enter full address">
            </div>
            <div class="col-12 form-group">
              <input type="text" class="form-control" name="city" id="city" placeholder="Town / City">
            </div>
            <div class="col-md-6 form-group">
              <input type="text" class="form-control" name="state" id="state" placeholder="State">
            </div>
            <div class="col-md-6 form-group">
              <input type="text" class="form-control" name="pin_code" id="pin_code" placeholder="Postcode / Zip">
            </div>
            <div class="col-12 form-group">
              <button type="submit" class="as-btn" id="accountNewCreate">Create shpping address</button>
            </div>
          </div>
          </form>
        </div>
     
      <div class="col-lg-6">
       <div class="row">
       <h2 class="h4">Shipping Address Details</h2>
          <?php 
            $totalAddress = count($addresses);
            foreach($addresses as $address){?>
          <div class="col-lg-12 col-md-12">
            <div class="form-group">
            <input type="radio" class="input-radio" name="addressID" id="addressID<?=$address->id?>" value="<?=$address->id?>" checked>&nbsp;&nbsp;<label for="addressID<?=$address->id?>" class="ml-2"><?= $address->name.', '.$address->mobile.', '.$address->email.', '.$address->address.', '.$address->street ?><span style="cursor:pointer" onclick="deleteAddress(<?=$address->id?>)" class="text-danger"><i class="fa fa-trash" style="margin-left: 25px;"></i></span></label>
            </div>
          </div>
          <hr>
          <?php } ?>
        </div>
      </div>
    </div>
    <h4 class="mt-4 pt-lg-2">Your Order</h4>
    <form action="#" class="woocommerce-cart-form">
      <table class="cart_table mb-20">
        <thead>
          <tr>
            <th class="cart-col-image">Image</th>
            <th class="cart-col-productname">Product Name</th>
            <th class="cart-col-price">Price</th>
            <th class="cart-col-quantity">Quantity</th>
            <th class="cart-col-total">Total</th>
          </tr>
        </thead>
        <tbody>
        <?php 
              $cartItems=$this->cart->contents();
              //print_r($cartItems);
              foreach($cartItems as $item){
              ?>
          <tr class="cart_item">
            <td data-title="Product"><a class="cart-productimage" href="#"><img width="91" height="91" src="<?= $item['image']?>" alt="Image"></a></td>
            <td data-title="Name"><a class="cart-productname" href="#"><?= $item['name']?></a></td>
            <td data-title="Price"><span class="amount"><bdi><span>₹</span><?= $item['price']?></bdi></span></td>
            <td data-title="Quantity"><strong class="product-quantity"><?= $item['qty']?></strong></td>
            <td data-title="Total"><span class="amount"><bdi><span>₹</span><?= $item['subtotal']?></bdi></span></td>
          </tr>
          <?php } ?>
        </tbody>
        <tfoot class="checkout-ordertable">
          <tr class="cart-subtotal">
            <th>Subtotal</th>
            <td data-title="Subtotal" colspan="4"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">₹ </span><?=$this->cart->total()?></bdi></span></td>
          </tr>
          <tr class="woocommerce-shipping-totals shipping">
            <th>Shipping</th>
            <td data-title="Shipping" colspan="4">₹ 0</td>
          </tr>
          <tr class="order-total">
            <th>Total</th>
            <td data-title="Total" colspan="4"><strong><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">₹ </span><?=$this->cart->total()?></bdi></span></strong></td>
          </tr>
        </tfoot>
      </table>
    </form>
    <div class="mt-lg-3 mb-30">
      <div class="woocommerce-checkout-payment">
        <ul class="wc_payment_methods payment_methods methods">
      
          <li class="wc_payment_method payment_method_cod">
            <input id="payment_method_cod" type="radio" class="input-radio payment_type" name="payment_method" value="cod" checked> <label for="payment_method_cod">Case On Delivery </label>
            <div class="payment_box payment_method_cod">
              <p>Pay with cash upon delivery.</p>
            </div>
          </li>
          <li class="wc_payment_method payment_method_paypal">
            <input id="payment_method_paypal" type="radio" class="input-radio payment_type" name="payment_method" value="paypal"> <label for="payment_method_paypal">Online Paypal<img src="<?=base_url('public/website/assets/img/cart/paypal.jpg')?>" alt="PayPal acceptance mark"></label>
            <div class="payment_box payment_method_paypal">
              <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
            </div>
          </li>
        </ul>
        <div class="form-row place-order"><button type="button" onclick ="order_place()" class="as-btn">Place order</button></div>
      </div>
    </div>
  </div>
</div>
<script>
  function deleteAddress(id){
      var messageText  = "You want delete this address!";
      var confirmText =  'Yes, delete it!';
      var message  ="Shipping address delete Successfully!";
      Swal.fire({
          title: 'Are you sure?',
          text: messageText,
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: confirmText
          }).then((result) => {
          if (result.isConfirmed) {
              $.ajax({
                  url: '<?=base_url('cart/delete_address')?>', 
                  method: 'POST',
                  data: {id},
                  success: function(result){
                  toastr.success(message);
                  setTimeout(function(){
                  window.location.reload();
                  }, 2000);
          }
      });
  
  }
  })
  }
  
  
  $("form#addAddress").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  toastr.success(data.message);
  $(':input[type="submit"]').prop('disabled', false);
      setTimeout(function(){
          location.reload();
  }, 1000) 
  
  }else if(data.status==403) {
  toastr.error(data.message);
  $(':input[type="submit"]').prop('disabled', false);
  }else{
  toastr.error('Something went wrong');
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
  
  function order_place(){
  var payment_type = $('input[type=radio][name="payment_method"]:checked').val();
  var address = 0;
  var address = <?=$totalAddress?> > 0  ? $('input[type=radio][name="addressID"]:checked').val() : 0;
  if($('input[type=radio][name="addressID"]:checked').length >  0){
  $.ajax({
  url: '<?=base_url('cart/placeOrder')?>', 
  method: 'POST',
  data: {payment_type,address},
  success: function(data){

    Swal.fire({
    icon: 'success',
    title: 'Order Added Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
    setTimeout(function(){
    window.location="store";
    }, 2000);
  
  }
  });
  }else{
  Swal.fire({
  icon: 'error',
  title: 'Please add address',
  text: 'Please select address',
  //footer: '<a href="">Why do I have this issue?</a>'
  })
  }
  }
</script>