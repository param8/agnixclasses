
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addClassItemModal" data-whatever="@mdo">Add Class Item <i class="fa fa-plus"></i></a>
            </div>
          </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
        <div class="box">
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>Image</th>
                <th>School Name</th>
								<th>Class Name</th>
								<th>Item Title</th>
								<th>Quantity</th>
								<th>Price</th>
                <th>Type</th>
                <th>Status</th>
								<th>Created Date</th>
                <th>Action</th>
							</tr>
						</thead>
						<tbody>
            <?php foreach($items as $key=>$item){?>
							<tr>
								<td><?=$key+1;?></td>
                <td><img src="<?=$item->image;?>"></td>
                <td><?= $item->schoolName?></td>
								<td><?= $item->className?></td>
								<td><?= $item->title?></td>
								<td><?= $item->quantity?></td>
                <td><?= $item->special_price?></td>
                <td><?= $item->type?></td>
								<td><?= $item->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">De-Active</span>'?></td>
                <td nowrap><?= date('d-m-Y',strtotime($item->create_date));?></td>
                <td nowrap>
                  <a href="#" onclick="editModalShow('<?=$item->sci_uid?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit Class Item"><i class="fa fa-edit"></i></a>
                  <a href="#" onclick="deleteClassItem('<?=$item->sci_uid?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Class Item"><i class="fa fa-trash"></i></a>
                </td>
							</tr>
             <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Schoo Modal Start -->
  <div class="modal fade" id="addClassItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Class Items</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/class_item/store')?>" id="addSchool" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="form-group">
          <label for="school" class="col-form-label">School:</label>
            <select class="form-control" name="school" id="school" onchange="getClasses(this.value)">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>"><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
          <label for="class" class="col-form-label">Class:</label>
            <select class="form-control class" name="class" id="class" >
              <option value="">Select Class</option>
            </select>
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Item Title:</label>
            <input  class="form-control" name="title" id="title"> 
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Quantity:</label>
            <input  class="form-control" name="quantity" id="quantity"> 
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Price:</label>
            <input  class="form-control" name="special_price" id="special_price"> 
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Discount:</label>
            <input  class="form-control" name="discount" id="discount"> 
          </div>
          <!-- <div class="form-group">
            <?php //$types = array('B','S','A'); ?>
            <label for="title" class="col-form-label">Type:</label>
            <select class="form-control" name="type" id="type" >
              <option value="">Select Type</option>
              <?php //foreach($types as $type){?>
                <option value="<?//=$type?>"><//?=$type?></option>
                <?php// } ?>
            </select>
          </div> -->
        
          <div class="form-group">
            <label for="image" class="col-form-label">Item Picture:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Modal End -->


  <!-- Edit Schoo Modal Start -->
  <div class="modal fade" id="editClassItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Class Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/class_item/update')?>" id="editSchool" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editFormData">
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Modal End -->

  
  <script type="text/javascript">
  function getClasses(schoolID){
    $.ajax({
       url: '<?=base_url('Ajax_controller/get_class')?>',
       type: 'POST',
       data: {schoolID},
       success: function (data) {
         $('.class').html(data);
       }
     });
  }

  function editModalShow(uid){
    $.ajax({
       url: '<?=base_url('admin/Class_item/editForm')?>',
       type: 'POST',
       data: {uid},
       success: function (data) {
        $('#editClassItemModal').modal('show');
         $('#editFormData').html(data);
       }
     });
  }

  $("form#addSchool").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add school');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   $("form#editSchool").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
             location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit school');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });


   function deleteSchool(uid){
     var messageText  = "You want delete this school!";
     var confirmText =  'Yes, delete it!';
     var message  ="School delete Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/class_item/delete')?>', 
                method: 'POST',
                data: {uid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }


 </script>  
  