
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title=='Institutes' ? '<i class="fa fa-institution"></i>' : '<i class="fa fa-users"></i>'?> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
			  <div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">All <?=$page_title?></h3>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                                <th>SNO</th>
								<th>Image</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Created Date</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
                            <?php foreach($users as $key=>$user){?>
							<tr>
								<td><?=$key+1;?></td>
								<td><img src="<?=base_url($user->profile_pic)?>" width="100" height="100"></td>
								<td><?= $user->name?></td>
								<td><?= $user->email?></td>
								<td><?= $user->contact?></td>
								<td><?= $user->address?></td>
                                <td><?= date('d-m-Y',strtotime($user->created_at));?></td>
								<td><a  href="javascript:void(0);" onclick="updateUserStatus('<?=$user->id?>', '<?=($user->status=='1')?'In-active':'Active'?>')"  data-toggle="tooltip" title="Click to Change Status"><?= $user->status == 1 ? '<span class="btn  btn-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
								<td>
									<a href="#" onclick="viewModalShow('<?=$user->id;?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="View User"><i class="fa fa-eye"></i></a>
								</td>
							</tr>
                            <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

<!-- View Class Modal Start -->
<div class="modal fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewUserData">
  
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Edit School Class Modal End -->

<script>
function updateUserStatus(user_id, status){
     var messageText  = "You want to "+status+" this user?";
     var confirmText =  'Yes, Change it!';
     var message  ="User status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/user/update_status')?>', 
                method: 'POST',
                data: {userid: user_id, user_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }

  function viewModalShow(userid){
    $.ajax({
       url: '<?=base_url('admin/user/viewUser')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#viewUserModal').modal('show');
         $('#viewUserData').html(data);
       }
     });
  }
</script>