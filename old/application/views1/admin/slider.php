
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Slider <i class="fa fa-plus"></i></a>
            </div>
          </div>
				  <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
				</div>
				<!-- /.box-header -->
        <div class="box">
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                                <th>SNO</th>
                                <th>Title</th>
                                <th>Link</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
							</tr>
						</thead>
						<tbody>
                <?php foreach($sliders as $key=>$slider){?>
							<tr>
								<td><?=$key+1;?></td>
								<td><?=$slider->title?></td>
                <td><?=$slider->btn_link?></td>
                <td><img src="<?=base_url('public/uploads/'.$slider->image);?>" width="100px;"></td>
                <td><a href="javascript:void(0);" onclick="updateSliderStatus('<?=$slider->id?>', '<?=($slider->status=='1')?'In-active':'Active'?>')"  data-toggle="tooltip" title="Click to Change Status"><?= $slider->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">De-Active</span>'?></a></td>
                <td>
                  <a href="#" onclick="editModalShow('<?=$slider->slider_uid?>')" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit Slider"><i class="fa fa-edit"></i></a>
                  <a href="#" onclick="deleteSlider('<?=$slider->slider_uid?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Slider"><i class="fa fa-trash"></i></a>
                </td>
							</tr>
             <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Slider Modal Start -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Slider</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/slider/store')?>" id="addSlider" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="s_id" class="col-form-label">School Name:</label>
            <select class="form-control school_id" name="s_id" id="s_id">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>"><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="title" class="col-form-label">Title:</label>
            <input type="text" class="form-control" name="title" id="title">
          </div>
          <div class="form-group">
            <label for="btn_link" class="col-form-label">Button Link:</label>
            <input type="text" class="form-control" name="btn_link" id="btn_link">
          </div>
          <div class="form-group">
            <label for="btn_text" class="col-form-label">Button Text:</label>
            <input type="text" class="form-control" name="btn_text" id="btn_text">
          </div>
          <div class="form-group">
            <label for="image" class="col-form-label">Slider Image:</label>
            <input type="file" class="form-control" name="image" id="image">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add Slider Modal End -->

  <!-- Edit Slider Modal Start -->
<div class="modal fade" id="editSliderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Slider</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/slider/update')?>" id="editSlider" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editFormData">
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Edit School Class Modal End -->
  
  <script type="text/javascript">
  $("form#addSlider").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add slider');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
  
   function editModalShow(uid){
    $.ajax({
       url: '<?=base_url('admin/slider/editForm')?>',
       type: 'POST',
       data: {uid},
       success: function (data) {
        $('#editSliderModal').modal('show');
         $('#editFormData').html(data);
       }
     });
  }

  $("form#editSlider").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to edit slider');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function deleteSlider(uid){
     var messageText  = "You want delete this slider!";
     var confirmText =  'Yes, delete it!';
     var message  ="Slider deleted Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/slider/delete')?>', 
                method: 'POST',
                data: {uid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
            }
          });      
          }
        })
  }

  function updateSliderStatus(slider_id, status){
     var messageText  = "You want to "+status+" this slider?";
     var confirmText =  'Yes, Change it!';
     var message  ="Slider status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/slider/update_status')?>', 
                method: 'POST',
                data: {sliderid: slider_id, slider_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }
</script>
  