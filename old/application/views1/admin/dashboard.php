
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		 <!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
					<div class="box bg-primary-light">
						<div class="box-body d-flex px-0">
							<div class="flex-grow-1 p-30 flex-grow-1 bg-img dask-bg bg-none-md" style="background-position: right bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-1.svg)">
								<div class="row ">
									<div class="col-12 col-xl-7">
										<h2>Welcome back, <strong><?php echo $this->session->userdata('name'); ?>!</strong></h2>

										<p class="text-dark my-10 font-size-16">
											Your students complated <strong class="text-warning">80%</strong> of the tasks.
										</p>
										<p class="text-dark my-10 font-size-16">
											Progress is <strong class="text-warning">very good!</strong>
										</p>
									</div>
									<div class="col-12 col-xl-5"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
									<div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-3.svg)">
										<h4 class="font-weight-400">Total Teacher</h4>
										<p class="font-size-20">
										<?=count($Institute);?>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
                               <h4 class="font-weight-400">Total Students</h4>
								<p class="font-size-20">
								<?=count($users);?>
								</p>
								</div>
								</div>
							</div>
						</div>
						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-primary p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">
									<h4 class="font-weight-400">Total Experts</h4>
									<p class="font-size-20">
									<?=count($experts);?>
								    </p>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xl-6">
							<div class="box">
								<div class="box-body d-flex p-0">
								<div class="flex-grow-1 bg-danger p-30 flex-grow-1 bg-img" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 100%; background-image: url(../images/svg-icon/color-svg/custom-4.svg)">

									<h4 class="font-weight-400">Total Cources</h4>

									<p class="font-size-20">
									<?//=$total_cource?>
								    </p>
								</div>
								</div>
							</div>
						</div>
					</div>
				 <div class="row">
				<div class="col-xl-6">
					<div class="box bg-transparent no-shadow mb-0">
						<div class="box-header no-border">
							<h4 class="box-title">Total Courses By Teacher</h4>							
							
						</div>
					</div>
					
					  <div class="box">
						<div class="box-body py-0">
							<div class="table-responsive">
							   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                               <div id="institute_courses"></div>
							</div>
						</div>
                      </div>
					</div>	
                

				<div class="col-xl-6">
					<div class="box bg-transparent no-shadow mb-0">
						<div class="box-header no-border">
							<h4 class="box-title">Total Booked Course By Students</h4>							
							
						</div>
					</div>
					
					 <div class="box">
						<div class="box-body py-1">
							<div class="table-responsive">
							   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                               <div id="booked_courses"></div>
							</div>
						</div>
                     </div>
				</div>	
                </div>
					
				</div>
			</div>
		 </section>
		 <!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script>
         google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Teacher', 'Courses', { role: 'style' } ],
        <?php foreach($get_cources as $course){
              echo $course;
            }
             ?>
      ]);

      var view = new google.visualization.DataView(data);

      var options = {
        title: "Total Teacher Course",
        titleTextStyle: {fontSize: '19',color:'#73879C'},
        bar: {groupWidth: "90%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("institute_courses"));
      chart.draw(view, options);
  }
      
     </script>

<script>
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Courses', 'Student', { role: 'style' } ],
        <?php foreach($booked_courses as $booked){
              echo $booked;
            }
             ?>
      ]);

      var view = new google.visualization.DataView(data);

      var options = {
        title: "Total Booked Course",
        titleTextStyle: {fontSize: '19',color:'#73879C'},
        bar: {groupWidth: "90%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("booked_courses"));
      chart.draw(view, options);
  }
      
     </script>
  

	
	

