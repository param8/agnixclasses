
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">
       <div class="col-md-6 col-lg-6">
			   <div class="box"> 
            <div class="box-header with-border">
                <h3 class="box-title">All <?=$page_title?></h3>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-6 ">
          <div class="box "> 
            <div class="box-header with-border">
              <a href="#" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#bookSetModal" data-whatever="@mdo">Add Book Set  <i class="fa fa-plus"></i></a>
            </div>
          </div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                  <th>SNO</th>
                  <th>Image</th>
								  <th>School Name</th>
								  <th>Class Name</th>
                  <th>Title</th>
                  <th>Price</th>
								  <th>Status</th>
								  <th>Created Date</th>
                  <th>Action</th>
							</tr>
						</thead>
						<tbody>
                <?php foreach($bookSets as $key=>$bookSet){?>
							<tr>
								<td><?=$key+1;?></td>
                <td><img src="<?=$bookSet->image; ?>" style="widht:100px; height:50px;"></td>
								<td><?= $bookSet->schoolName?></td>
								<td><?= $bookSet->className?></td>
								<td><?= $bookSet->title?></td>
                <td><?= $bookSet->price?></td>
								<td><?= $bookSet->status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">De-Active</span>'?></td>
                <td><?= date('d-m-Y',strtotime($bookSet->created_at));?></td>
                <td>
                  <!-- <a href="javascript:void(0);" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit School Class"><i class="fa fa-edit"></i></a> -->
                  <a href="#" class="btn btn-warning btn-sm " onclick="editBookSetFunction('<?=$bookSet->id?>','<?=$bookSet->sc_id?>','<?=$bookSet->s_id?>')" data-toggle="tooltip" title="Edit Book Set"><i class="fa fa-edit"></i></a>
                  <a href="#" onclick="deleteBookSet('<?=$bookSet->book_set_uid?>')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Book Set"><i class="fa fa-trash"></i></a>
                </td>
							</tr>
             <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

  <!-- Add Schoo Modal Start -->
  <div class="modal fade" id="bookSetModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Book Set</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/book_set/store')?>" id="addBookSet" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-group">
            <label for="title" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Title">
          </div>

          <div class="form-group">
            <label for="image" class="col-form-label">Image:</label>
            <input type="file" class="form-control" id="image" name="image" >
          </div>
          <div class="form-group">
            <label for="school" class="col-form-label">School Name:</label>
            <select class="form-control school_id" name="school" id="school" onchange="getClasses(this.value)">
              <option value="">Select School</option>
              <?php foreach($schools as $school){?>
                <option value="<?=$school->id?>"><?=$school->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="class" class="col-form-label">Class:</label>
            <select class="form-control class" name="class" id="class" onchange="getItems(this.value)">
              <option value="">Select class</option>
            </select>
          </div>
          <div class="class_wise_item" >
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Add School Modal End -->

  <div class="modal fade" id="editBookSetModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Book Set</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('admin/book_set/update')?>" id="editBookSet" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editForm">
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?//= in_array($item->id,$allItems) ? 'checked="checked"' : '';?> 

  <script type="text/javascript">
    function getClasses(schoolID){
    $.ajax({
       url: '<?=base_url('Ajax_controller/get_class')?>',
       type: 'POST',
       data: {schoolID},
       success: function (data) {
         $('.class').html(data);
       }
     });
  }

    function getItems(classID,bookSetID){
     $.ajax({
       url: '<?=base_url('admin/book_set/bookSetCreateForm')?>',
       type: 'POST',
       data: {classID,bookSetID},
       success: function (data) {
        $('.class_wise_item').html(data);
        get_total_amount();
       },
     }); 
    }

    function get_total_amount(){
      var totalamount = 0;
     $('.checkboxAmount').each(function(){
       if($(this).prop('checked')){
         var id =$(this).val();
         var singleValues = 0;
         var amount =  $('#itemAmount'+id).val();
         console.log(amount)
         totalamount += +amount;  
       } else{
        totalamount += +0;
       } //console.log(totalamount)
     });
     
      $('.totalAmount').val(totalamount);
     

   }
   

  $("form#addBookSet").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add book set');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function editBookSetFunction(bookSetID,classID){
    $.ajax({
       url: '<?=base_url('admin/book_set/bookSetEditForm')?>',
       type: 'POST',
       data: {bookSetID},
       success: function (data) {
        $('#editBookSetModal').modal('show');
        $('#editForm').html(data);
        getItems(classID,bookSetID);
         
       },
     }); 
   }


   $("form#editBookSet").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add book set');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });

   function deleteBookSet(uid){
     var messageText  = "All the items in this class will also deleted!";
     var confirmText =  'Yes, delete it!';
     var message  ="Class deleted Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('admin/book_set/delete')?>', 
                method: 'POST',
                data: {uid},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
            }
          });      
          }
        })
  }

</script>
  