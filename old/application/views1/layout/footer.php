<footer class="footer-wrapper footer-layout2">
       <div class="container">
          <div class="footer-top style2">
             <div class="row justify-content-between align-items-center">
                <div class="col-xl-4">
                   <h2 class="h3 footer-form-text">Get update by signup <span class="text-theme2 fw-normal">Newsletter</span></h2>
                </div>
                <div class="col-xl-5">
                   <form action="#" class="form-style2">
                      <div class="form-group"><input type="email" placeholder="Enter your mail"> <button type="submit" class="as-btn style2">subscribe</button></div>
                   </form>
                </div>
             </div>
          </div>
       </div>
       <div class="widget-area">
          <div class="container">
             <div class="row justify-content-between">
                <div class="col-md-6 col-xl-3">
                   <div class="widget footer-widget">
                      <div class="as-widget-about">
                        <?php //print_r($siteinfo);?>
                         <div class="footer-logo"><a href="index.html"><img src="<?=base_url($siteinfo->site_logo)?>" alt="Logo" style="width:100px;height:80px;"></a></div>
                         <p class="footer-text"><?=substr($siteinfo->discription,0,100)?>...</p>
                         <div class="footer-social">
                        <a href="<?=$siteinfo->facebook_url?>"><i class="fab fa-facebook-f"></i></a>
                        <a href="<?=$siteinfo->twitter_url?>"><i class="fab fa-twitter"></i></a> 
                        <a href="<?=$siteinfo->insta_url?>"><i class="fab fa-instagram"></i></a> 
                        <a href="<?=$siteinfo->linkedin_url?>"><i class="fab fa-linkedin-in"></i></a>
                     </div>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-xl-3">
                   <div class="widget widget_nav_menu footer-widget">
                      <h3 class="widget_title">Quick link</h3>
                      <div class="menu-all-pages-container footer-menu">
                         <ul class="menu">
                            <li><a href="<?=base_url('home')?>">Home</a></li>
                            <li><a href="<?=base_url('about-us')?>">About</a></li>
                            <li><a href="<?=base_url('contact-us')?>">Contact</a></li>
                            <li><a href="<?=base_url('courses')?>">Our Courses</a></li>
                            <li><a href="<?=base_url('store')?>">Our Shop</a></li>
                         </ul>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-xl-3 col-xxl-auto">
                   <div class="widget footer-widget">
                      <h3 class="widget_title">Contact Us</h3>
                      <div class="as-widget-about">
                         <p class="footer-info"><i class="fal fa-phone"></i><a class="text-inherit" href="tel:+88123456987231"><?=$siteinfo->site_contact?></a></p>
                         <p class="footer-info"><i class="fal fa-envelope"></i><a class="text-inherit" href="mailto:Aduki@email.com"><?=$siteinfo->site_email?></a></p>
                         <p class="footer-info"><i class="fal fa-map-marker-alt"></i><?=$siteinfo->site_address?></p>
                      </div>
                   </div>
                </div>
                <div class="col-md-6 col-xl-3">
                   <div class="widget footer-widget">
                      <h3 class="widget_title">Location</h3>
                      <div class="footer-map">
                         <p class="footer-text">Fusce varittus, dolor tempor interdum tristiquei bibendum</p>
                         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.15830883544!2d-74.1197638982804!3d40.69766374869574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sbd!4v1650265996065!5m2!1sen!2sbd"></iframe>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="copyright-wrap">
          <div class="container">
             <p class="copyright-text">Copyright <a href="index.html"><?=$siteinfo->footer_contant?> </a><a href="#">Web Cadence</a>.</p>
          </div>
       </div>
    </footer>
    <a href="#" class="scrollToTop scroll-btn"><i class="far fa-arrow-up"></i></a>

    <script>
document.addEventListener("keyup", function (e) {
    var keyCode = e.keyCode ? e.keyCode : e.which;
            if (keyCode == 44) {
                stopPrntScr();
            }
        });
function stopPrntScr() {

            var inpFld = document.createElement("input");
            inpFld.setAttribute("value", ".");
            inpFld.setAttribute("width", "0");
            inpFld.style.height = "0px";
            inpFld.style.width = "0px";
            inpFld.style.border = "0px";
            document.body.appendChild(inpFld);
            inpFld.select();
            document.execCommand("copy");
            inpFld.remove(inpFld);
        }
       function AccessClipboardData() {
            try {
                window.clipboardData.setData('text', "Access   Restricted");
            } catch (err) {
            }
        }
        setInterval("AccessClipboardData()", 300);
document.addEventListener('keyup', (e) => {
    if (e.key == 'PrintScreen') {
        navigator.clipboard.writeText('');
        alert('Screenshots disabled!');
    }
});

/** TO DISABLE PRINTS WHIT CTRL+P **/
document.addEventListener('keydown', (e) => {
    if (e.ctrlKey && e.key == 'p') {
        alert('This section is not allowed to print or export to PDF');
        e.cancelBubble = true;
        e.preventDefault();
        e.stopImmediatePropagation();
    }
});
    </script>
    <script src="<?=base_url('public/website/assets/js/vendor/jquery-3.6.0.min.js')?>">
    </script><script src="<?=base_url('public/website/assets/js/app.min.js')?>">
  </script><script src="<?=base_url('public/website/assets/js/main.js')?>"></script>
   <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
 <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

   <script>
     $(document).ready(function() {
      $(".js-example-placeholder-multiple").select2({
        placeholder: "Select Subjects"
    });
    
    });

		function closeModal(){
		  $('#modalLogin').modal('hide');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		function loginModal(){
		  $('#modalLogin').modal('show');
		  $('#modalRegister').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}
		
		function registerModal(){
		  $('#modalRegister').modal('show');
		  $('#modalLogin').modal('hide');
		  $('div').removeClass('modal-backdrop');
		}

    function getCity(stateID){
      $.ajax({
      url: '<?=base_url('Ajax_controller/get_city')?>',
      type: 'POST',
      data: {stateID},
      success: function (data) {
        $('#city').html(data);
              $('.city').html(data);
      }
      });
    }

    $(document).ready(function() {
      getCity(<?=$this->session->userdata('location_state')?>)
    
    });

    function resetCourse(){
      $.ajax({
       url: '<?=base_url("setting/resetCourse")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetMode(){
      $.ajax({
       url: '<?=base_url("setting/resetMode")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetStartDate(){
      $.ajax({
       url: '<?=base_url("setting/resetStartDate")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetState(){
      $.ajax({
       url: '<?=base_url("setting/resetState")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function resetCity(){
      $.ajax({
       url: '<?=base_url("setting/resetCity")?>',
       type: 'POST',
       data: {ResetSesession:'ResetSesession'},  
       success: function (data) {
        location.reload();
       },
     });
    }

    function setCourseSession(){
      var course= $('#course').val();
      var mode= $('#mode').val();
      var location_state= $('#state').val();
      var location_city= $('#city').val();
      var start_date= $('#start_date').val();
      $.ajax({
       url: '<?=base_url("setting/setSession")?>',
       type: 'POST',
       data: {course,mode,start_date,location_state,location_city},  
       success: function (data) {
        window.location="courses";
       },
     });
    }
	
    function notification(){
     $.ajax({
       url: '<?=base_url("notification/redirect_notification")?>',
       type: 'POST',
       data: {notification:'notification'},  
       success: function (data) {
        window.location="<?=base_url('notification')?>";
       },
     });
    }

    function addCart(bookID){
    $.ajax({
  url: '<?=base_url('cart/addCart')?>',
  type: 'POST',
  data: {bookID},
  dataType: 'json',
  success: function (data) {
    Swal.fire({
      icon: 'success',
      title: 'Book added successfully',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
    setTimeout(function(){
		location.reload();
			}, 2000) 
  }
  });
  }

  function update_cart(rowid,condition){
	var qty = $('#qty_'+rowid).val();
   //alert(condition)
	$.ajax({
       url: '<?=base_url('cart/update_cart')?>',
       type: 'POST',
	   data:{qty,rowid,condition},
	   dataType: 'json',
       success: function (data) {
		if(data.status==200){
		setTimeout(function(){
		location.reload();
			}, 1000) 
    	//$('#items_div').load(location.href + ' #items_div');		
	   }
       },
       error: function(){} 
     });
   }

 
   function removeCartItem(rowid){
	
      var messageText  = "You want to remove this item?";
     var confirmText =  'Yes, remove it!';
     var message  ="Item remove Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
         $.ajax({
         url: '<?=base_url('cart/removeCartItem')?>',
         type: 'POST',
         data:{rowid},
         dataType: 'json',
         success: function (data) {
         if(data.status==200){
            Swal.fire({
            icon: 'success',
            title: 'Remove successfully',
            text: "Item remove successfully",
            //footer: '<a href="">Why do I have this issue?</a>'
         })
         setTimeout(function(){
            window.location.reload();
         }, 2000);		
         }
         },
         error: function(){} 
      });
          
        }
        })

   }
   $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
	</script>
  </body>
</html>