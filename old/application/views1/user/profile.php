
        <!--=====================================-->
        <!--=       Breadcrumb Area Start      =-->
        <!--=====================================-->


        <!-- <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>

                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/img/shape/ab-shape-1-1.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/img/shape/line-3.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div> -->

        <!--=====================================-->
        <!--=          Login Area Start         =-->
        <!--=====================================-->
        <section class="account-page-area section-gap-equal">
            <div class="container position-relative">
                <div class="row g-5 justify-content-center">
            
                    <div class="col-lg-10">
                        <div class="login-form-box registration-form">
                            <!-- <h3 class="title text-center">Profile</h3> -->
                            <form action="<?=base_url('users/update')?>" id="editUsers">
                           <div class="row justify-content-center">
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                    <label for="name">Name*</label>
                                    <input type="text"  id="name" name="name" placeholder="Please Enter Name " value="<?=$user->name?>">
                                  </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="email">Email*</label>
                                  <input type="email"   placeholder="Please Enter Email " value="<?=$user->email?>" disabled>
                                </div>
                              </div>
                              
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                    <label for="name">Mobile*</label>
                                    <input type="text"   placeholder="Please Enter Mobile No." value="<?=$user->contact?>"  maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" disabled>
                                  </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="address">Address*</label>
                                  <textarea type="text"  class="form-control" id="address" name="address" placeholder="Please Enter Address"><?=$user->address?></textarea>
                                </div>
                              </div>
                             
                              <div class="col-lg-5">
                                <div class="form-group">
                                  <label for="state">State*</label>
                                      <select class="dropdown_css"  id="state" name="state" onchange="getCity(this.value)" readonly>
                                        <option value="<?=$user->state?>"><?=$user->stateName?></option>
                                        <?php foreach($states as $state){?>
                                        <option value="<?=$state->id?>"><?=$state->name?></option>
                                        <?php  } ?>
                                    </select>
                                  </div>
                              </div>
                              <div class="col-lg-5">
                                <div class="form-group">
                                  <label for="city">City*</label>
                                    <select class="city dropdown_css" id="city" name="city" disabled>
                                    <option value="<?=$user->city?>"><?=$user->cityName?></option>
                                    </select>
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="profile_pic">Profile Pic*</label>
                                  <input type="file" class="form-control" id="profile_pic" name="profile_pic" >
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5 text-center">
                                <div class="form-group">
                                 
                                  <img src="<?=base_url($user->profile_pic)?>" class="form-control" style="width:100px;height:100px;">
                                </div>
                              </div>
                              <?php if($this->session->userdata('user_type')=='Institute'){?>
                                <h3 class="title text-center mt-5 ">Edit Site info</h3>
                                <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="address">Discription*</label>
                                  <textarea type="text" class="form-control"  id="discription" name="discription" placeholder="Please Enter discription"> <?=$user->discription?></textarea>
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="profile_pic">Facebook URL*</label>
                                  <input type="text"  id="facebook_url" name="facebook_url" placeholder="Please Enter facebook url " value="<?=$user->facebook_url?>">
                                </div>
                              </div>

                              
                              <div class="col-lg-5">
                                <div class="form-group">
                                <label for="address">Insta URL*</label>
                                  <input type="text"  id="insta_url" name="insta_url" placeholder="Please Enter Insta url" value="<?=$user->insta_url?>">
                                </div>
                              </div>
                              <div class="col-lg-5">
                                <div class="form-group">
                                  <label for="profile_pic">Twitter URL*</label>
                                  <input type="text"  id="twitter_url" name="twitter_url" placeholder="Please Enter twitter url " value="<?=$user->twitter_url?>">
                                </div>
                              </div>

                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="address">Linkedin URL*</label>
                                  <input type="text"  id="linkedin_url" name="linkedin_url" placeholder="Please Enter Linkedin url" value="<?=$user->linkedin_url?>">
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="profile_pic">Youtube URL*</label>
                                  <input type="text"  id="youtube_url" name="youtube_url" placeholder="Please Enter youtube url " value="<?=$user->youtube_url?>">
                                </div>
                              </div>

                              <?php } ?>


                              <?php if($this->session->userdata('user_type')=='Student'){
                                ?>
                                <h3 class="title text-center mt-5 ">Please add detail</h3>
                                <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="address">Date of Birth*</label>
                                  <input type="date" class="form-control"  id="dob" name="dob" placeholder="Please Enter date of birth" value="<?=$student->dob?>"> 
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="profile_pic">Whatsapp No*</label>
                                  <input type="text"  id="whatsappNo" name="whatsappNo" placeholder="Please Enter whatsappno. " value="<?=$student->whatsappNo?>" maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                                </div>
                              </div>

                              
                              <div class="col-lg-5 mt-5" >
                                <div class="form-group">
                                <label for="address">Current Domain*</label>
                                <select class="dropdown_css" id="current_domain" name="current_domain" onchange="checkDomain(this.value)">
                                  <?php $domains = array('school','college','Other')?>
                                  <option value="">Select Domain</option>
                                  <?php foreach($domains as $domain){?>
                                   <option value="<?=$domain?>" <?=$domain==$student->current_domain ? 'selected' : '' ?>><?=$domain?></option>
                                  <?php } ?>
                                </select>
                    
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5" id="other_domain" style="display:none">
                                <div class="form-group">
                                  <label for="profile_pic">Name of School/College/other*</label>
                                  <input type="text"  id="school_collage_name" name="school_collage_name" placeholder="Please Enter Name of School/College/other " value="<?=$student->school_collage_name?>">
                                </div>
                              </div>

                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="interested_free_demo">Interest in free demo*</label>
                                <select class="dropdown_css" id="interested_free_demo" name="interested_free_demo">
                                  <?php $free_demo = array('Yes','No','Maybe');?>
                                  <option value="">Select Free demo</option>
                                  <?php foreach($free_demo as $demo){?>
                                   <option value="<?=$demo?>" <?=$demo==$student->interested_free_demo ? 'selected' : '' ?>><?=$demo?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="">Free Demo class Mode *</label>
                                  <select class="dropdown_css" id="free_demo_class_mode" name="free_demo_class_mode">
                                  <?php $class_mode = array('Online','Offline','Both')?>
                                  <option value="">Select Free demo Class Mode</option>
                                  <?php foreach($class_mode as $mode){?>
                                   <option value="<?=$mode?>" <?=$mode==$student->free_demo_class_mode ? 'selected' : '' ?>><?=$mode?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>

                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="">Target Exam *</label>
                                  <select class="dropdown_css" id="target_exam" name="target_exam" onchange="checkTargetExam(this.value)">
                                  <?php $target_exam = array('Gov Exam','Competitive exam','College Exam','School exam','Other')?>
                                  <option value="">Select Target Exam</option>
                                  <?php foreach($target_exam as $exam){?>
                                   <option value="<?=$exam?>" <?=$exam==$student->target_exam ? 'selected' : '' ?>><?=$exam?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>

                              <div class="col-lg-5 mt-5" id="other_exam" style="display:none">
                                <div class="form-group">
                                  <label for="profile_pic">Other Target Exam *</label>
                                  <input type="text"  id="target_exam_other" name="target_exam_other" placeholder="Other Target Exam " value="<?=$student->target_exam_other?>">
                                </div>
                              </div>
                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                  <label for="profile_pic">Please Mention Subject/Course/Skills  *</label>
                                  <textarea type="text" class="form-control"  id="skill" name="skill" placeholder="Please Mention Subject/Course/Skills "><?=$student->skill?></textarea>
                                </div>
                              </div>

                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="address">Interested For Scholarship*</label>
                                <select class="dropdown_css" id="scholarship" name="scholarship">
                                  <?php $scholarships = array('Yes','No','Maybe')?>
                                  <option value="">Select Interested For Scholarship</option>
                                  <?php foreach($scholarships as $scholarship){?>
                                   <option value="<?=$scholarship?>" <?=$scholarship==$student->scholarship ? 'selected' : '' ?>><?=$scholarship?></option>
                                  <?php } ?>
                                </select>
                                </div>
                              </div>

                              <?php } ?>

                              <div class="col-lg-5 mt-5">
                                <div class="form-group">
                                <label for="address"></label>
                                <button type="submit" class="edu-btn btn-medium">Edit Account <i class="icon-4"></i></button>
                                </div>
                              </div>
                           </div>

                           
                            </form>
                        </div>
                    </div>
                </div>
                <ul class="shape-group">
                    <li class="shape-1 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="Shape"></li>
                    <li class="shape-2 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="Shape"></li>
                    <li class="shape-3 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/counterup/shape-02.png')?>" alt="Shape"></li>
                </ul>
            </div>
        </section>

        <script>
      $("form#editUsers").submit(function(e) {
			$(':input[type="submit"]').prop('disabled', true);
			e.preventDefault();    
			var formData = new FormData(this);
			$.ajax({
			url: $(this).attr('action'),
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (data) {
				if(data.status==200) {
				//$('.modal').modal('hide');
				Swal.fire({
          icon: 'success',
          title: 'Success',
          text: data.message,
          //footer: '<a href="">Why do I have this issue?</a>'
          })
				$(':input[type="submit"]').prop('disabled', false);
						setTimeout(function(){
							location.reload();
				}, 1000) 
		
				}else if(data.status==403) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: data.message,
            //footer: '<a href="">Why do I have this issue?</a>'
          })
				$(':input[type="submit"]').prop('disabled', false);
				}else{
          Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong',
          //footer: '<a href="">Why do I have this issue?</a>'
        })
				$(':input[type="submit"]').prop('disabled', false);
				}
			},
			error: function(){} 
			});
		});

    function checkDomain(domain){
      if(domain=='Other'){
        $('#other_domain').show();
      }else{
        $('#other_domain').hide();
      }
    }

    function checkTargetExam(exam){
      if(exam=='Other'){
        $('#other_exam').show();
      }else{
        $('#other_exam').hide();
      }
    }

  </script>
       