<!--=====================================-->
<!--=       Hero Banner Area Start      =-->
<!--=====================================-->
<?php $categoryclasses = array(
  '0' => "color-primary-style",
  '1' => "color-secondary-style",
  '2' => "color-extra01-style",
  '3' => "color-tertiary-style",
  '4' => "color-extra02-style",
  '5' => "color-extra03-style",
  '6' => "color-extra04-style",
  '7' => "color-extra05-style",
  '8' => "color-extra06-style");?>
  <?php if($this->session->userdata('user_type')!='Institute'){?>
<div class="hero-style-1">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-lg-12">
        <div class="banner-content">
          <h1 class="title">Find Your Best Courses</h1>
          <div class="banner-search">
            <div class="row justify-content-center">
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="input-group">
                  <input type="text" class="form-control" id="course" name="course" placeholder="Course Name">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="input-group">
                  <input type="text" onfocus="(this.type='date')" min="<?= date('Y-m-d'); ?>" class="form-control" id="start_date" name="end_date" placeholder="Start Date">
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 mt-2">
                <div class="input-group">
                  <select type="text" class="form-control" id="mode" name="mode" placeholder="Select Mode">
                    <option value=""> Select Mode</option>
                    <option value="Online"> Online</option>
                    <option value="Offline"> Offline</option>
                    <option value="Both"> Both</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="input-group">
                  <select type="text" class="form-control" id="state" name="state" onchange="getCity(this.value)" placeholder="Select State">
                    <option value=""> Select State</option>
                    <?php foreach($states as $state){?>
                    <option value="<?=$state->id?>"> <?=$state->name?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 mt-2">
                <div class="input-group">
                  <select type="text" class="form-control city" id="city" name="city" placeholder="Select City">
                    <option value=""> Select City</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-12 mt-2">
                <div class="input-group">
                  <button class="rn-btn edu-btn btn-medium submit-btn" name="submit" type="button" onclick="setCourseSession()">Search <i class="icon-4"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--=====================================-->
<!--=       Features Area Start      =-->
<!--=====================================-->
<!-- Start Categories Area  -->
<div class="features-area-2">
  <div class="container">
    <div class="features-grid-wrap">
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/online-class.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><span><?=count($totalCourses)?></span> Online/Offline Courses</h5>
        </div>
      </div>
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/instructor.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><span>Top</span>Instructors</h5>
        </div>
      </div>
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon certificate">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/certificate.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><span>Online</span>Certifications</h5>
        </div>
      </div>
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/user.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><span><?=count($users)?></span>Members</h5>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }else{
  ?>
  <div class="features-area-2">
  <div class="container">
    <div class="features-grid-wrap ">
   
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/instructor.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><a href="<?=base_url('create-course')?>" class="edu-btn"><span>Post New Courses Hear</span></a></h5>
        </div>
      </div>
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon certificate">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/certificate.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><a href="<?=base_url('notification')?>" class="edu-btn"><span>Notifications</span></a></h5>
        </div>
      </div>
      <div class="features-box features-style-2 edublink-svg-animate">
        <div class="icon">
          <img class="svgInject" src="<?=base_url('public/website/assets/images/animated-svg-icons/user.svg') ?>" alt="animated icon">
        </div>
        <div class="content">
          <h5 class="title"><a href="<?=base_url('booked-courses')?>" class="edu-btn"><span>Manage Courses</span></a></h5>
        </div>
      </div>

    </div>
  </div>
</div>
  <?php
} ?>
<!-- End Categories Area  -->
<!--=====================================-->
<!--=       Categories Area Start      =-->
<!--=====================================-->
<!-- Start Categories Area  -->
<?php if($this->session->userdata('user_type')!='Institute'){?>
<div class="edu-categorie-area categorie-area-2 edu-section-gap">
  <div class="container">
    <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <h2 class="title">Top Categories</h2>
      <span class="shape-line"><i class="icon-19"></i></span>
      <p></p>
    </div>
    <div class="row g-5">
      <?php 
        $i=0;
        foreach($allCategories as $topcategory){ 
        ($i<=9) ? ($category_class = $categoryclasses[$i]) : $i=0;
        ?>
      <div class="col-lg-4 col-md-6" data-sal-delay="50" data-sal="slide-up" data-sal-duration="800">
        <div class="categorie-grid categorie-style-2 <?=$category_class ?> edublink-svg-animate">
          <div class="icon">
            <i class="icon-9"></i>
          </div>
          <div class="content">
            <a href="<?=base_url('courses/'.base64_encode($topcategory->id))?>">
              <h5 class="title"><?=$topcategory->category?></h5>
            </a>
          </div>
        </div>
      </div>
      <?php $i++; }?>
    </div>
  </div>
</div>
<?php } ?>
<!-- End Categories Area  -->
<!--=====================================-->
<!--=       About Us Area Start      	=-->
<!--=====================================-->
<!-- <div class="gap-bottom-equal edu-about-area about-style-1">
  <div class="container edublink-animated-shape">
      <div class="row g-5 align-items-center">
          <div class="col-lg-6">
              <div class="about-image-gallery">
                  <img class="main-img-1" src="<?//=base_url('public/website/assets/images/about/about-01.webp')?>" alt="About Image">
                  <div class="video-box" data-sal-delay="150" data-sal="slide-down" data-sal-duration="800">
                      <div class="inner">
                          <div class="thumb">
                              <img src="<?//=base_url('public/website/assets/images/about/about-02.webp')?>" alt="About Image">
                              <a href="https://www.youtube.com/watch?v=PICj5tr9hcc" class="popup-icon video-popup-activation">
                                  <i class="icon-18"></i>
                              </a>
                          </div>
                          <div class="loading-bar">
                              <span></span>
                              <span></span>
                          </div>
                      </div>
                  </div>
                  <div class="award-status bounce-slide">
                      <div class="inner">
                          <div class="icon">
                              <i class="icon-21"></i>
                          </div>
                          <div class="content">
                              <h6 class="title">29+</h6>
                              <span class="subtitle">Wonderful Awards</span>
                          </div>
                      </div>
                  </div>
                  <ul class="shape-group">
                      <li class="shape-1 scene" data-sal-delay="500" data-sal="fade" data-sal-duration="200">
                          <img data-depth="1" src="<?//=base_url('public/website/assets/images/about/shape-36.png')?>" alt="Shape">
                      </li>
                      <li class="shape-2 scene" data-sal-delay="500" data-sal="fade" data-sal-duration="200">
                          <img data-depth="-1" src="<?//=base_url('public/website/assets/images/about/shape-37.png') ?>" alt="Shape">
                      </li>
                      <li class="shape-3 scene" data-sal-delay="500" data-sal="fade" data-sal-duration="200">
                          <img data-depth="1" src="<?//=base_url('public/website/assets/images/about/shape-02.png')?>" alt="Shape">
                      </li>
                  </ul>
              </div>
          </div>
          <div class="col-lg-6" data-sal-delay="150" data-sal="slide-left" data-sal-duration="800">
              <div class="about-content">
                  <div class="section-title section-left">
                      <span class="pre-title">About Us</span>
                      <h2 class="title">Learn & Grow Your Skills From <span class="color-secondary">Anywhere</span></h2>
                      <span class="shape-line"><i class="icon-19"></i></span>
                      <p>Lorem ipsum dolor sit amet consectur adipiscing elit sed eiusmod ex tempor incididunt labore dolore magna aliquaenim minim veniam quis nostrud exercitation ullamco laboris.</p>
                  </div>
                  <ul class="features-list">
                      <li>Expert Trainers</li>
                      <li>Online Remote Learning</li>
                      <li>Lifetime Access</li>
                  </ul>
              </div>
          </div>
      </div>
      <ul class="shape-group">
          <li class="shape-1 circle scene" data-sal-delay="500" data-sal="fade" data-sal-duration="200">
              <span data-depth="-2.3"></span>
          </li>
      </ul>
  </div>
  </div> -->
<!--=====================================-->
<!--=       Course Area Start      		=-->
<!--=====================================-->
<!-- Start Course Area  -->
<div class="edu-course-area course-area-1 edu-section-gap bg-lighten01">
  <div class="container">
    <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <h2 class="title">Listed Batch</h2>
      <span class="shape-line"><i class="icon-19"></i></span>
    </div>
    <div class="row g-5">
      <?php 
        foreach($courses as $course){
          $earlier = new DateTime($course->start_date);
          $later = new DateTime($course->end_date);
          $totalDays =  $later->diff($earlier)->format("%a");
          if($course->fees > 0 && $course->discount > 0){
              $price =$course->fees - ($course->discount*$course->fees)/100;
          }else{
              $price =$course->fees;
          }
          $this->db->where(array('course_rating.courseID' => $course->id));
          $allOverRating =  $this->db->get('course_rating')->result();
        
          $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>5));
          $five =  $this->db->get('course_rating')->result();
        
          $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>4));
          $four =  $this->db->get('course_rating')->result();
        
          $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>3));
          $three =  $this->db->get('course_rating')->result();
        
          $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>2));
          $two =  $this->db->get('course_rating')->result();
        
          
          $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>1));
          $one =  $this->db->get('course_rating')->result();
          $count_Five = 0;
          $count_Foure = 0;
          $count_Three = 0;
          $count_Two = 0;
          $count_One = 0;
          $total_CountFive = 0;
          $total_CountFoure = 0;
          $total_CountThree = 0;
          $total_CountTwo = 0;
          $total_CountOne = 0;
          if(count($five) > 0){
           $count_Five = count($five);
           $total_CountFive = $count_Five*5;
          }
          if(count($four) > 0){
           $count_Foure = count($four);
           $total_CountFoure = $count_Foure*4;
          }
          if(count($three) > 0){
           $count_Three = count($three);
           $total_CountThree = $count_Three*3;
          }
          if(count($two) > 0){
           $count_Two = count($two);
           $total_CountTwo = $count_Two*2;
          }
          if(count($one) > 0){
           $count_One = count($one);
           $total_CountOne = $count_One*1;
          }
          $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
          $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
          if($sumTotalCountRating > 0 && $sumCountRating > 0){
          $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
          }else{
            $TotalratingAvg = 0;
          }
            
            $stars_rating="";
            $newwholeRating = floor($TotalratingAvg);
            $fractionRating = $TotalratingAvg - $newwholeRating;
            if($newwholeRating > 0){
            for($s=1;$s<=$newwholeRating;$s++){
              $stars_rating .= '<i class="icon-23"></i>';	
            }
            if($fractionRating >= 0.25){
              $stars_rating .= '<i class="icon-23 n50"></i>';	
            }
          }
            else{
              for($s=1;$s<=5;$s++){
               $stars_rating .= '<i class="icon-23 text-secondary"></i>';
              }
            }  
        ?>
      <div class="col-md-6 col-lg-4 col-xl-3" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
          <div class="inner">
            <div class="thumbnail">
              <a href="<?=base_url('course-details/'.base64_encode($course->id))?>">
              <img src="<?=base_url($course->image)?>" alt="Course Meta" style="height:269px">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i><?=$totalDays?> Days</span>
              </div>
            </div>
            <div class="content">
              <!-- <span class="course-level">Beginner</span> -->
              <h6 class="title">
                <a href="#"><?=$course->course?></a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?= $stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg .'/'.count($allOverRating)?> Rating)</span>
              </div>
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$course->fees?></del></div>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$course->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$course->seats?> Remaning Seats</li>
              </ul>
            </div>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content">
            <div class="content">
              <!-- <span class="course-level">Advanced</span> -->
              <h6 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($course->id))?>"><?=$course->course?></a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  <?= $stars_rating?>
                </div>
                <span class="rating-count">(<?=$TotalratingAvg .'/'.count($allOverRating)?> Rating)</span>
              </div>
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$course->fees?></del></div>
              <p><?=$course->description?></p>
              <ul class="course-meta">
                <li><i class="icon-24"></i><?=$course->seats?> Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i><?=$course->seats?> Remaning Seats</li>
              </ul>
              <a href="<?=base_url('course-details/'.base64_encode($course->id))?>" class="edu-btn btn-secondary btn-small"><?= $this->session->userdata('user_type')=='Institute'? 'Course Detail' :($this->session->userdata('user_type')=='Experts'? 'Course Detail' : 'Book Now') ?><i class="icon-4"></i></a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <div class="course-view-all" data-sal-delay="150" data-sal="slide-up" data-sal-duration="1200">
    <?php if(count($courses)>0){ ?>
      <a href="<?=base_url('courses')?>" class="edu-btn">Browse more courses <i class="icon-4"></i></a>
      <?php } else{?>
        <a href="#" class="edu-btn">No Course Found <i class="icon-4"></i></a>
        <?php } ?>
    </div>
  </div>
</div>
<!-- End Course Area -->

<!--=====================================-->
<!--=       Course Area Start      		=-->
<!--=====================================-->
<!-- Start Course Area  -->
<div class="edu-course-area course-area-1 edu-section-gap bg-lighten01">
  <div class="container">
    <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <!-- <span class="pre-title"></span> -->
      <h2 class="title">Booked Courses</h2>
      <span class="shape-line"><i class="icon-19"></i></span>
    </div>
    <div class="row g-5">
      <?php 
        foreach($booked_courses as $booked){
          $earlier = new DateTime($booked->start_date);
            $later = new DateTime($booked->end_date);
            $totalDays =  $later->diff($earlier)->format("%a");
            if($booked->fees > 0 && $booked->discount > 0){
                $price =$booked->fees - ($booked->discount*$booked->fees)/100;
            }else{
                $price =$booked->fees;
            }
        ?>
      <div class="col-md-6 col-lg-4 col-xl-3" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
          <div class="inner">
            <div class="thumbnail">
              <a href="<?=base_url('course-details/'.base64_encode($booked->id))?>">
              <img src="<?=base_url($booked->profile_pic)?>" alt="Course Meta" style="height:269px">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i><?=$totalDays?> Days</span>
              </div>
            </div>
            <div class="content">
              <!-- <span class="course-level">Beginner</span> -->
              <h6 class="title">
                <a><i class="icon-25"></i> <?=$this->session->userdata('user_type')=='Student' ? $booked->institute_name : ($this->session->userdata('user_type')=='Institute' ? $booked->student_name : '') ?></a>
              </h6>
              <h6 class="title">
                <a><i class="icon-envelope"> </i><?=$this->session->userdata('user_type')=='Student' ? $booked->institute_email : ($this->session->userdata('user_type')=='Institute' ? $booked->student_email : '') ?></a>
              </h6>
              <h6 class="title">
                <a><i class="icon-phone"> </i><?=$this->session->userdata('user_type')=='Student' ? $booked->institute_contact : ($this->session->userdata('user_type')=='Institute' ? $booked->student_contact : '') ?></a>
              </h6>
      
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$booked->fees?></del></div>
         
            </div>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content">
            <div class="content">
              <!-- <span class="course-level">Advanced</span> -->
              <h6 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($booked->id))?>"><?=$booked->category?></a>
              </h6>
              <h6 class="title">
                <a href="<?=base_url('course-details/'.base64_encode($booked->id))?>"><?=$booked->course_name?></a>
              </h6>
       
              <div class="course-price">₹ <?=$price?> / <del>₹ <?=$booked->fees?></del></div>
              <ul class="course-meta">
                <li class="title text-success"><?=date('d M Y',strtotime($booked->start_date))?> <span >Batch Start</span></li>
                <li class="title text-danger"><?=date('d M Y',strtotime($booked->end_date))?> <span>Batch End</span></li>
                <li class="title text-warning"><?=$totalDays?> Days</li>
                <li class="title text-primary">Mode :- <?=$booked->mode?> </li>
              </ul>
              <p><?=substr($booked->course_discription,0,100)?></p>
            
              <a href="<?=base_url('course-details/'.base64_encode($booked->id))?>" class="edu-btn btn-secondary btn-small"><?= $this->session->userdata('user_type')=='Institute'? 'Course Detail' :($this->session->userdata('user_type')=='Experts'? 'Course Detail' : 'Book Now') ?> <i class="icon-4"></i></a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    <div class="course-view-all" data-sal-delay="150" data-sal="slide-up" data-sal-duration="1200">
      <?php if(count($booked_courses)>0){?>
      <a href="<?=base_url('booked-courses')?>" class="edu-btn">Booking Request <i class="icon-4"></i></a>
      <?php } else{?>
        <a href="#" class="edu-btn">No Booking Request <i class="icon-4"></i></a>
      <?php } ?>
    </div>
  </div>
</div>
<!-- End Course Area -->
<!--=====================================-->
<!--=       CounterUp Area Start      	=-->
<!--=====================================-->
<div class="counterup-area-2">
  <div class="container">
    <div class="row g-5 justify-content-center">
      <div class="col-lg-8">
        <div class="counterup-box-wrap">
          <div class="counterup-box counterup-box-1">
            <div class="edu-counterup counterup-style-2">
              <h2 class="counter-item count-number primary-color">
                <span class="odometer" data-odometer-final="<?=count($totalStudents)?>">.</span><span></span>
              </h2>
              <h6 class="title">Student Enrolled</h6>
            </div>
            <div class="edu-counterup counterup-style-2">
              <h2 class="counter-item count-number secondary-color">
                <span class="odometer" data-odometer-final="32.4">.</span><span>K</span>
              </h2>
              <h6 class="title">Class Completed</h6>
            </div>
          </div>
          <div class="counterup-box counterup-box-2">
            <div class="edu-counterup counterup-style-2">
              <h2 class="counter-item count-number extra05-color">
                <span class="odometer" data-odometer-final="<?=count($totalInstitute)?>">.</span><span></span>
              </h2>
              <h6 class="title">Top Instructors</h6>
            </div>
            <div class="edu-counterup counterup-style-2">
              <h2 class="counter-item count-number extra02-color">
                <span class="odometer" data-odometer-final="99.9">.</span><span>%</span>
              </h2>
              <h6 class="title">Satisfaction Rate</h6>
            </div>
          </div>
          <ul class="shape-group">
            <li class="shape-1 scene">
              <img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="Shape">
            </li>
            <li class="shape-2">
              <img class="rotateit" src="<?=base_url('public/website/assets/images/counterup/shape-02.png')?>" alt="Shape">
            </li>
            <li class="shape-3 scene">
              <img data-depth="1.6" src="<?=base_url('public/website/assets/images/counterup/shape-04.png')?>" alt="Shape">
            </li>
            <li class="shape-4 scene">
              <img data-depth="-1.6" src="<?=base_url('public/website/assets/images/counterup/shape-05.png')?>" alt="Shape">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--=====================================-->
<!--=       Testimonial Area Start      =-->
<!--=====================================-->
<!-- Start Testimonial Area  -->
<div class="testimonial-area-1 section-gap-equal">
  <div class="container">
    <div class="row g-lg-5">
      <div class="col-lg-5">
        <div class="testimonial-heading-area">
          <div class="section-title section-left" data-sal-delay="50" data-sal="slide-up" data-sal-duration="800">
            <span class="pre-title">Testimonials</span>
            <h2 class="title">What Our Students Have To Say</h2>
            <span class="shape-line"><i class="icon-19"></i></span>
            <a href="#" class="edu-btn btn-large">View All<i class="icon-4"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-7">
        <div class="home-one-testimonial-activator swiper ">
          <div class="swiper-wrapper">
            <?php foreach($ratings as $rating){?>
            <div class="swiper-slide">
              <div class="testimonial-grid">
                <div class="thumbnail">
                  <img src="<?=base_url($rating->profile_pic)?>" alt="Testimonial" style="width:70px;height:70px;">
                  <span class="qoute-icon"><i class="icon-26"></i></span>
                </div>
                <div class="content">
                  <p><?=$rating->description?></p>
                  <div class="rating-icon">
                    <?php 
                      $userRating = array();
                      for($i=1; $i<=$rating->rating; $i++){
                          $userRating[] = $i;
                      }
                      for($j=1; $j<=5; $j++){
                          if(in_array($j,$userRating)){
                      ?>
                    <i class="icon-23"></i>
                    <?php }else{
                      ?>
                    <i class="icon-23 text-secondary"></i>
                    <?php
                      } } ?>
                  </div>
                  <h5 class="title"><?=$rating->name?></h5>
                  <span class="subtitle"><?=$rating->user_type?></span>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Testimonial Area  -->
<!--=====================================-->
<!--=      Call To Action Area Start   	=-->
<!--=====================================-->
<!-- Start CTA Area  -->
<div class="home-one-cta-two cta-area-1">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-8">
        <div class="home-one-cta edu-cta-box bg-image">
          <div class="inner">
            <div class="content text-md-end">
              <span class="subtitle">Get In Touch:</span>
              <h3 class="title"><a href="mailto:<?=$siteinfo->site_email?>"><?=$siteinfo->site_email?></a></h3>
            </div>
            <div class="sparator">
              <span>or</span>
            </div>
            <div class="content">
              <span class="subtitle">Call Us Via:</span>
              <h3 class="title"><a href="tel:+91 <?=$siteinfo->site_contact?>">+91 <?=$siteinfo->site_contact?></a></h3>
            </div>
          </div>
          <ul class="shape-group">
            <li class="shape-01 scene">
              <img data-depth="2" src="<?=base_url('public/website/assets/images/cta/shape-06.png')?>" alt="shape">
            </li>
            <li class="shape-02 scene">
              <img data-depth="-2" src="<?=base_url('public/website/assets/images/cta/shape-12.png')?>" alt="shape">
            </li>
            <li class="shape-03 scene">
              <img data-depth="-3" src="<?=base_url('public/website/assets/images/cta/shape-04.png')?>" alt="shape">
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End CTA Area  -->
<!--=====================================-->
<!--=      		Team Area Start   		=-->
<!--=====================================-->
<!-- Start Team Area  -->
<?php if(empty($this->session->userdata(('email')))){?>
<div class="edu-team-area team-area-1 gap-tb-text">
  <div class="container">
    <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
      <span class="pre-title">Instructors</span>
      <h2 class="title">Course Instructors</h2>
      <span class="shape-line"><i class="icon-19"></i></span>
    </div>
    <div class="row g-5">
      <!-- Start Instructor Grid  -->
      <?php foreach($totalInstitute as $institute){?>
      <div class="col-lg-3 col-sm-6 col-12" data-sal-delay="50" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-team-grid team-style-1">
          <div class="inner">
            <div class="thumbnail-wrap">
              <div class="thumbnail">
                <a href="#">
                <img src="<?=base_url($institute->profile_pic)?>" alt="team images" style="width:270px;height:320px;">
                </a>
              </div>
              <ul class="team-share-info">
                <li><a href="#"><i class="icon-share-alt"></i></a></li>
                <li><a href="<?=base_url($institute->facebook_url)?>" target="_blank"><i class="icon-facebook"></i></a></li>
                <li><a href="<?=base_url($institute->twitter_url)?>" target="_blank"><i class="icon-twitter"></i></a></li>
                <li><a href="<?=base_url($institute->linkedin_url)?>" target="_blank"><i class="icon-linkedin2"></i></a></li>
              </ul>
            </div>
            <div class="content">
              <h5 class="title"><a href="#"><?=$institute->name?></a></h5>
              <!-- <span class="designation">UI Designer</span> -->
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <!-- End Instructor Grid  -->
    </div>
  </div>
</div>
<?php } ?>
<!-- End Team Area  -->
<!--=====================================-->
<!--=      CTA Banner Area Start   		=-->
<!--=====================================-->
<!-- Start Ad Banner Area  -->
<!-- <div class="edu-cta-banner-area home-one-cta-wrapper bg-image">
  <div class="container">
      <div class="edu-cta-banner">
          <div class="row justify-content-center">
              <div class="col-lg-7">
                  <div class="section-title section-center" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                      <h2 class="title">Get Your Quality Skills <span class="color-secondary">Certificate</span> Through EduBlink</h2>
                      <a href="<?//=base_url('my-account')?>" class="edu-btn">Get started now <i class="icon-4"></i></a>
                  </div>
              </div>
          </div>
          <ul class="shape-group">
              <li class="shape-01 scene">
                  <img data-depth="2.5" src="<?//=base_url('public/website/assets/images/cta/shape-10.png')?>" alt="shape">
              </li>
              <li class="shape-02 scene">
                  <img data-depth="-2.5" src="<?//=base_url('public/website/assets/images/cta/shape-09.png')?>" alt="shape">
              </li>
              <li class="shape-03 scene">
                  <img data-depth="-2" src="<?//=base_url('public/website/assets/images/cta/shape-08.png')?>" alt="shape">
              </li>
              <li class="shape-04 scene">
                  <img data-depth="2" src="<?//=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape">
              </li>
          </ul>
      </div>
  </div>
  </div>-->
<!-- End Ad Banner Area  -->
<!--=====================================-->
<!--=      		Brand Area Start   		=-->
<!--=====================================-->
<!-- Start Brand Area  -->
<!-- <div class="edu-brand-area brand-area-1 gap-top-equal">
  <div class="container">
      <div class="row">
          <div class="col-lg-5">
              <div class="brand-section-heading">
                  <div class="section-title section-left" data-sal-delay="150" data-sal="slide-up" data-sal-duration="800">
                      <span class="pre-title">Our Partners</span>
                      <h2 class="title">Learn with Our Partners</h2>
                      <span class="shape-line"><i class="icon-19"></i></span>
                      <p>Lorem ipsum dolor sit amet consectur adipiscing elit sed eiusmod tempor incididunt.</p>
                  </div>
              </div>
          </div>
          <div class="col-lg-7">
              <div class="brand-grid-wrap">
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-01.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-02.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-03.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-04.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-05.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-06.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-07.png')?>" alt="Brand Logo">
                  </div>
                  <div class="brand-grid">
                      <img src="<?//=base_url('public/website/assets/images/brand/brand-08.png')?>" alt="Brand Logo">
                  </div>
              </div>
          </div>
      </div>
  </div>
  </div>  -->
<!-- End Brand Area  -->
<!--=====================================-->
<!--=      		Blog Area Start   		=-->
<!--=====================================-->
<!-- Start Blog Area  -->
<div class="edu-blog-area blog-area-1 edu-section-gap">
  <div class="container">
    <!-- <div class="section-title section-center" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
      <span class="pre-title">Latest Articles</span>
      <h2 class="title">Get News with EduBlink</h2>
      <span class="shape-line"><i class="icon-19"></i></span>
      </div> -->
    <div class="row g-5">
      <!-- Start Blog Grid  -->
      <!-- <div class="col-lg-4 col-md-6 col-12" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-blog blog-style-1">
            <div class="inner">
                <div class="thumbnail">
                    <a href="blog-details.html">
                        <img src="<?//=base_url('public/website/assets/images/blog/blog-01.jpg')?>" alt="Blog Images">
                    </a>
                </div>
                <div class="content position-top">
                    <div class="read-more-btn">
                        <a class="btn-icon-round" href="blog-details.html"><i class="icon-4"></i></a>
                    </div>
                    <div class="category-wrap">
                        <a href="#" class="blog-category">ONLINE</a>
                    </div>
                    <h5 class="title"><a href="blog-details.html">Become a Better Blogger: Content Planning</a></h5>
                    <ul class="blog-meta">
                        <li><i class="icon-27"></i>Oct 10, 2021</li>
                        <li><i class="icon-28"></i>Com 09</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet cons tetur adipisicing sed.</p>
                </div>
            </div>
        </div>
        </div> -->
      <!-- End Blog Grid  -->
      <!-- Start Blog Grid  -->
      <!-- <div class="col-lg-4 col-md-6 col-12" data-sal-delay="200" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-blog blog-style-1">
            <div class="inner">
                <div class="thumbnail">
                    <a href="blog-details.html">
                        <img src="<?//=base_url('public/website/assets/images/blog/blog-02.jpg')?>" alt="Blog Images">
                    </a>
                </div>
                <div class="content position-top">
                    <div class="read-more-btn">
                        <a class="btn-icon-round" href="blog-details.html"><i class="icon-4"></i></a>
                    </div>
                    <div class="category-wrap">
                        <a href="#" class="blog-category">LECTURE</a>
                    </div>
                    <h5 class="title"><a href="blog-details.html">How to Keep Workouts Fresh in the Morning</a></h5>
                    <ul class="blog-meta">
                        <li><i class="icon-27"></i>Oct 10, 2021</li>
                        <li><i class="icon-28"></i>Com 09</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet cons tetur adipisicing sed do eiusmod ux tempor incid idunt labore dol oremagna aliqua.</p>
                </div>
            </div>
        </div>
        </div> -->
      <!-- End Blog Grid  -->
      <!-- Start Blog Grid  -->
      <!-- <div class="col-lg-4 col-md-6 col-12" data-sal-delay="300" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-blog blog-style-1">
            <div class="inner">
                <div class="thumbnail">
                    <a href="blog-details.html">
                        <img src="<?//=base_url('public/website/assets/images/blog/blog-03.jpg')?>" alt="Blog Images">
                    </a>
                </div>
                <div class="content position-top">
                    <div class="read-more-btn">
                        <a class="btn-icon-round" href="blog-details.html"><i class="icon-4"></i></a>
                    </div>
                    <div class="category-wrap">
                        <a href="#" class="blog-category">BUSINESS</a>
                    </div>
                    <h5 class="title"><a href="blog-details.html">Four Ways to Keep Your Workout Routine Fresh</a></h5>
                    <ul class="blog-meta">
                        <li><i class="icon-27"></i>Oct 10, 2021</li>
                        <li><i class="icon-28"></i>Com 09</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet cons tetur adipisicing sed do eiusmod ux tempor incid idunt.</p>
                </div>
            </div>
        </div>
        </div> -->
      <!-- End Blog Grid  -->
    </div>
  </div>
  <ul class="shape-group">
    <li class="shape-1 scene">
      <img data-depth="-1.4" src="<?=base_url('public/website/assets/images/about/shape-02.png')?>" alt="Shape">
    </li>
    <li class="shape-2 scene">
      <span data-depth="2.5"></span>
    </li>
    <li class="shape-3 scene">
      <img data-depth="-2.3" src="<?=base_url('public/website/assets/images/counterup/shape-05.png')?>" alt="Shape">
    </li>
  </ul>
</div>
<!-- End Blog Area  -->
<!--=====================================-->
<!--=        Footer Area Start       	=-->
<!--=====================================-->
<!-- Start Footer Area  -->