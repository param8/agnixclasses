
        <!--=====================================-->
        <!--=       Breadcrumb Area Start      =-->
        <!--=====================================-->


        <div class="edu-breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-inner">
                    <div class="page-title">
                        <h1 class="title"><?=$page_title?></h1>
                    </div>
                    <ul class="edu-breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=base_url('home')?>">Home</a></li>
                        <li class="separator"><i class="icon-angle-right"></i></li>
                        <li class="breadcrumb-item"><a href="#"><?=$page_title?></a></li>
                    </ul>
                </div>
            </div>
            <ul class="shape-group">
                <li class="shape-1">
                    <span></span>
                </li>
                <li class="shape-2 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-13.png')?>" alt="shape"></li>
                <li class="shape-3 scene"><img data-depth="-2" src="<?=base_url('public/website/assets/images/about/shape-15.png')?>" alt="shape"></li>
                <li class="shape-4">
                    <span></span>
                </li>
                <li class="shape-5 scene"><img data-depth="2" src="<?=base_url('public/website/assets/images/about/shape-07.png')?>" alt="shape"></li>
            </ul>
        </div>

        <!--=====================================-->
        <!--=        Courses Area Start         =-->
        <!--=====================================-->
        <div class="edu-course-area course-area-1 section-gap-equal">
            <div class="container">
                <div class="row g-5">
                    
                    <div class="col-lg-10 col-pl--35">


                        <div class="edu-sorting-area">
                            <div class="sorting-left">
                                <h6 class="showing-text">We found <span><?=count($teachers)?></span> <?=strtolower($page_title)?> available </h6>
                            </div>
                            <?php if($this->session->userdata('user_type')=='Institute'){?>
                            <div class="sorting-right">
                                <div class="layout-switcher">
                                    <label>Add <?=$page_title?></label>
                                    <ul class="switcher-btn">
                                        <li><a href="<?=base_url('create-teacher')?>" class=""><i class="fi fi-minus-line"></i> +</a></li>
                                        <!-- <li><a href="course-four.html" class="active"><i class="icon-54"></i></a></li> -->
                                    </ul>
                                </div>
                                <!-- <div class="edu-sorting">
                                    <div class="icon"><i class="icon-55"></i></div>
                                    <select class="edu-select">
                                        <option>Filters</option>
                                        <option>Low To High</option>
                                        <option>High Low To</option>
                                        <option>Last Viewed</option>
                                    </select>
                                </div> -->
                            </div>
                            <?php } ?>
                        </div>
                     <?php foreach($teachers as $teacher){?>
                        <div class="edu-course course-style-4 course-style-8">
                            <div class="inner">
                                <div class="thumbnail">
                                        <img src="<?=$teacher->image?>" alt="Course Meta">
                                    
                                </div>
                                <div class="content">
                                    <!-- <div class="course-price">$29.00</div> -->
                                    <h6 class="title">
                                        <?=$teacher->name?>
                                    </h6>
                                    <div class="course-rating">
                                     
                                        <span class="rating"><i class="icon-48"></i> <?=$teacher->subject?></span>
                                    </div>

                                    <div class="course-rating">
                                     
                                        <span class="rating"><i class="icon-47"></i> <?=$teacher->qulification?></span>
                                    </div>
                                    
                                    <ul class="course-meta">
                                        <li><i class="icon-24"></i><?=$teacher->experience?> Experience</li>
                                        <li><i class="icon-25"></i><?=$teacher->students?> Students</li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                        <?php } ?>
                    </div>
                </div>


                <!-- <ul class="edu-pagination ">
                    <li><a href="#" aria-label="Previous"><i class="icon-west"></i></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li class="more-next"><a href="#"></a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#" aria-label="Next"><i class="icon-east"></i></a></li>
                </ul> -->

            </div>
        </div>
        <!-- End Course Area -->
        <!--=====================================-->
        <!--=        Footer Area Start          =-->
        <!--=====================================-->
        <!-- Start Footer Area  -->
        