
    <section class="position-relative space-top">
      <div class="as-shape2 d-none d-xxl-block jump-reverse"><img src="<?=base_url('public/website/assets/img/shape/ab-shape-1-1.png')?>" alt="About shape"></div>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-6 mb-40 mb-xl-0">
            <div class="img-box4">
              <div class="img-1"><img src="<?=base_url($about->image)?>" alt="About image"></div>
              <!-- <div class="about-box">
                <div class="about-box__icon">
                  <div class="icon-shape"></div>
                  <img src="<//?=base_url('public/website/assets/img/icon/ab-2-5.png')?>" alt="icon">
                </div>
                <h5 class="mb-0 mt-30 pt-20"><span class="counter-number">10</span>k+</h5>
                <p class="mb-0">Active Students</p>
              </div> -->
            </div>
          </div>
          <div class="col-xxl-5 col-xl-6">
            <div class="sec-line"></div>
            <h2 class=""><span class="text-theme2"><?=$about->title?></span></h2>
            <p class="mb-5"><?=$about->description?></p>
            <div class="row gx-60 gy-xxl-3 mb-2 pb-xl-3">
              <div class="col-6">
                <div class="about-media style2">
                  <div class="about-media__icon">
                    <div class="icon-shape"></div>
                    <img src="<?=base_url('public/website/assets/img/icon/online-course.png')?>" alt="icon">
                  </div>
                  <div class="media-body">
                    <h3 class="about-media__number"><span class="counter-number"><?=$corses_count;?></span>+</h3>
                    <p class="about-media__text">Courses</p>
                  </div>
                </div>
              </div>
              <div class="col-6">
                <div class="about-media style2">
                  <div class="about-media__icon">
                    <div class="icon-shape"></div>
                    <img src="<?=base_url('public/website/assets/img/icon/medical-team.png')?>" alt="icon">
                  </div>
                  <div class="media-body">
                    <h3 class="about-media__number"><span class="counter-number"><?=count($totalDoctors)?></span>+</h3>
                    <p class="about-media__text">Doctors</p>
                  </div>
                </div>
              </div>
              <div class="col-6">
                <div class="about-media style2">
                  <div class="about-media__icon">
                    <div class="icon-shape"></div>
                    <img src="<?=base_url('public/website/assets/img/icon/books.png')?>" alt="icon">
                  </div>
                  <div class="media-body">
                    <h3 class="about-media__number"><span class="counter-number"><?=count($books)?></span>+</h3>
                    <p class="about-media__text">Stores</p>
                  </div>
                </div>
              </div>
              <div class="col-6">
                <div class="about-media style2">
                  <div class="about-media__icon">
                    <div class="icon-shape"></div>
                    <img src="<?=base_url('public/website/assets/img/icon/people.png')?>" alt="icon">
                  </div>
                  <div class="media-body">
                    <h3 class="about-media__number"><span class="counter-number"><?=count($totalStudents)?></span>+</h3>
                    <p class="about-media__text">Students</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- <a href="course.html" class="as-btn style2">All Courses Here<i class="fas fa-long-arrow-right ms-2"></i></a> -->
          </div>
        </div>
      </div>
    </section>
    <section class="space-top" data-sec-pos="bottom-half" data-pos-for="#ctav2">
      <div class="container">
        <div class="bg-smoke z-index-common rounded-3">
          <div class="row gx-0">
            <div class="col-md-4 col-lg-4 feature-style1">
              <div class="feature-icon">
                <div class="icon-shape"></div>
                <div class="icon-border"></div>
                <div class="icon-inner"><img src="<?=base_url('public/website/assets/img/icon/feature-1-1.png')?>" alt="icon"></div>
              </div>
              <div class="media-body">
                <h3 class="feature-title h4">Learn new skills</h3>
                <p class="feature-text">With flexible courses</p>
              </div>
            </div>
            <div class="col-md-4 col-lg-4 feature-style1">
              <div class="feature-icon">
                <div class="icon-shape"></div>
                <div class="icon-border"></div>
                <div class="icon-inner"><img src="<?=base_url('public/website/assets/img/icon/feature-1-2.png')?>" alt="icon"></div>
              </div>
              <div class="media-body">
                <h3 class="feature-title h4">Expert Instructor</h3>
                <p class="feature-text">Upskill with Speacilist</p>
              </div>
            </div>
            <div class="col-md-4 col-lg-4 feature-style1">
              <div class="feature-icon">
                <div class="icon-shape"></div>
                <div class="icon-border"></div>
                <div class="icon-inner"><img src="<?=base_url('public/website/assets/img/icon/feature-1-3.png')?>" alt="icon"></div>
              </div>
              <div class="media-body">
                <h3 class="feature-title h4">Lifetime Access</h3>
                <p class="feature-text">Can start & finish one</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="bg-dark space" data-bg-src="<?=base_url('public/website/assets/img/bg/cta-bg-2-1.jpg')?>" id="ctav2">
      <div class="container">
        <div class="row justify-content-center text-center">
          <div class="col-lg-8 col-xl-7">

           
            <p class="cta-text text-white"><?=$about->short_description?></p>
           
          </div>
        </div>
      </div>
    </section>
    <section class="bg-smoke space-top space-extra-bottom" id="testi-sec">
    <?php if(count($ratings) > 0) { ?>
      <div class="container">
        <div class="row justify-content-center text-center">
          <div class="col-xl-5">
            <div class="title-area">
              <span class="subtitle">Reviews</span>
              <h2 class="sec-title">People Who Already Love Us</h2>
            </div>
          </div>
        </div>
        <div class="row slider-shadow as-carousel" data-slide-show="2" data-lg-slide-show="1" data-dots="true" data-xl-dots="true" data-ml-dots="true">
        <?php foreach($ratings as $rating){?>
          <div class="col-lg-6">
            <div class="testi-box">
              <div class="testi-box_content">
                <div class="testi-box_img"><img src="<?=base_url($rating->profile_pic)?>" alt="Avater" style="width:70px;height:70px;"></div>
                <p class="testi-box_text">“<?=$rating->description?>”</p>
              </div>
              <div class="testi-box_bottom">
                <div>
                  <h3 class="testi-box_name"><?=$rating->title?></h3>
                  <span class="testi-box_desig"><?=$rating->user_type?></span>
                </div>
                <div class="testi-box_review">
                <?php 
                  $userRating = array();
                  for($i=1; $i<=$rating->rating; $i++){
                      $userRating[] = $i;
                  }
                  for($j=1; $j<=5; $j++){
                      if(in_array($j,$userRating)){
                  ?>
                  <i class="icon-23"></i>
                  <?php }else{
                    ?>
                  <i class="fa-solid fa-star-sharp"></i>
               <?php }} ?>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
      <div class="shape-mockup jump d-none d-sm-block" data-top="18%" data-left="5%"><img src="assets/img/shape/triangle-1.png" alt="shapes"></div>
      <div class="shape-mockup jump-reverse d-none d-sm-block" data-top="18%" data-left="5%"><img src="assets/img/shape/dots-2.png" alt="shapes"></div>
      <div class="shape-mockup jump d-none d-sm-block" data-top="15%" data-right="6%"><img src="assets/img/shape/line-3.png" alt="shapes"></div>
      <div class="shape-mockup jump-reverse d-none d-sm-block" data-bottom="13%" data-left="5%"><img src="assets/img/shape/line-4.png" alt="shapes"></div>
      <div class="shape-mockup jump d-none d-sm-block" data-bottom="14%" data-right="5%"><img src="assets/img/shape/line-5.png" alt="shapes"></div>
    </section>
   