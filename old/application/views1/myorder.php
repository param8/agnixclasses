
 <div class="as-cart-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="woocommerce-notices-wrapper">
          <div class="woocommerce-message">Your Orders.</div>
        </div>
        <form action="#" class="woocommerce-cart-form">
          <table class="cart_table">
            <thead>
              <tr>
                <th class="cart-col-image">Order Id</th>
                <th class="cart-col-quantity">Quantity</th>
                <th class="cart-col-amount">Amount</th>
                <th class="cart-col-status">Status</th>
                <th class="cart-col-date">Date</th>
                <th class="cart-col-view">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 	
				foreach($orders as $order){
					$status = $order->status==0 ? '<span class="tetx-danger">Pending</span>' : ($order->status==1 ? '<span class="tetx-warning">In-Progress</span>' : '<span class="tetx-success">Delivered</span>' ); 
					?>
				<tr>
					<td class="product-item-no"><?= $order->orderID;?></td>
					<td class="product-item-name"><?= $order->qty;?></td>
					<td class="product-item-price"><?= $order->price?></td>
					<td><?=$status?></td>
					<td><?=date('d-m-Y',strtotime($order->created_at))?></td>
					<td><a href="javascript:void(0)" onclick="cartDetail('<?=$order->id?>')"class=""><i class="fa fa-eye"></i></a></td>
				</tr>
				<?php } ?>
				
				</tbody>
                </table>
             </form>
					</div>
				</div>
			</div>
	</section>
</div>

<div class="modal fade" id="cart_detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" >
        <div id="show_cart_detail"></div>

      <div class="modal-footer">
        <button type="button" onclick="CloseModal()" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </div>
      
  </div>
</div>

<script>
   function cartDetail(orderID){
    $.ajax({
        url: '<?=base_url('cart/cart_detail')?>',
        type: 'POST',
        data:{orderID},
        success: function (data) {
            $('#cart_detailModal').modal('show');
            $('#show_cart_detail').html(data);
        },
      });
   }
   function CloseModal(){
    $('#show_cart_detail').html('');
    $('#cart_detailModal').modal('hide');
   }
</script>