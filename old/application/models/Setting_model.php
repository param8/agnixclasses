<?php 
class Setting_model extends CI_Model 
{

  public function __construct()
  {
      parent::__construct();

  }

  public function update_siteInfo($data){
    $adminID =  $this->session->userdata('id');
    $this->db->where('adminID', $adminID);
    return $this->db->update('site_info',$data);
    //echo $this->db->last_query();die;
  }

  public function store_siteInfo($data){
    return $this->db->insert('site_info',$data);
  }
}