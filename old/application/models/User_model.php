<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}



	public function get_all_users($condition)
	{
		$this->db->select('users.*,site_info.discription,site_info.facebook_url,site_info.insta_url,site_info.twitter_url,site_info.linkedin_url,site_info.youtube_url,states.name as stateName,cities.name as cityName');
		$this->db->from('users');
		$this->db->join('site_info','site_info.adminID = users.id','left');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($condition);
	    $this->db->order_by('users.id','desc');
		return $this->db->get()->result();
	}

	public function get_doctor($condition){
       $this->db->select('users.*,site_info.discription,site_info.facebook_url,site_info.insta_url,site_info.twitter_url,site_info.linkedin_url,site_info.youtube_url');
	   $this->db->from('users');
	   $this->db->join('site_info','site_info.adminID = users.id','left');
	   $this->db->where($condition);
	   $this->db->order_by('users.id','desc');
	   return $this->db->get()->result();
	}

	public function get_user($condition){
		$this->db->select('users.*,site_info.discription,site_info.facebook_url,site_info.insta_url,site_info.twitter_url,site_info.linkedin_url,site_info.youtube_url,states.name as stateName,cities.name as cityName');
		$this->db->from('users');
		$this->db->join('site_info','site_info.adminID = users.id','left');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($condition);
		$query =  $this->db->get()->row();
		return $query;
	}

	public function get_student($condition){
     $this->db->where($condition);
	 return $this->db->get('student_detail')->row();
	}

	public function update_student($studentData,$condition){
		$this->db->where($condition);
	 return $this->db->update('student_detail',$studentData);
	}

	public function update_user_status($data,$id)
	{
		$this->db->where($id);
		return $this->db->update('users',$data);
	}

	public function get_user_details($data){
		$this->db->select('users.*,states.name as stateName,cities.name as cityName');
		$this->db->join('states','states.id=users.state','left');
		$this->db->join('cities','cities.id=users.city','left');
		$this->db->where($data);
		$result =  $this->db->get('users');
			return $result->row();	
	}
	
	public function stor_enquiry($data){
		return $this->db->insert('contact_us', $data);
	   }

	   public function store_student_detail($data){
		return $this->db->insert('student_detail', $data);
	   }

		 public function store_wallet($data){
			return $this->db->insert('wallet',$data);
		 }
		 public function get_wallet($condition){
            $this->db->where($condition);
			return $this->db->get('wallet')->row();
		 }

		 public function update_wallet($data,$condition){
			$this->db->where($condition);
			return $this->db->update('wallet',$data);
		 }

		 public function store_wallet_history($data){
			return $this->db->insert('wallet_history',$data);
		 }

}