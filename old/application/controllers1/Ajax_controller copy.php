<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();
     $this->load->model('course_model');
     $this->load->model('user_model');
     $this->load->model('Subject_model');

	}

	public function get_city()
    {
       $stateID =  $this->input->post('stateID');
       $cities = $this->Common_model->get_state_wise_city($stateID);
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$this->session->userdata('location_city')==$city->id ? 'selected' : ''?>><?=$city->city?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }


    public function load_more(){
       $id = $this->input->post('id');
       $output = "";
      if(!empty($this->input->post('categoryID'))){
        $categoryID = $this->input->post('categoryID');
        $category = $this->category_model->get_category(array('id' => $categoryID));
        $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id):array('courses.categoryID'=>$categoryID,'courses.status'=>1,'courses.id >'=>$id));
      }else{
        $categoryID = 0;
        $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1,'courses.id >'=>$id) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1,'courses.id >'=>$id):array('courses.status'=>1,'courses.id >'=>$id));
      }
        $courses = $this->course_model->get_courses($condition);
        $courseID = "";
          
          foreach($courses as $course){
            $courseID = $course->id;
            $earlier = new DateTime($course->start_date);
            $later = new DateTime($course->end_date);
            $totalDays =  $later->diff($earlier)->format("%a");
            if($course->fees > 0 && $course->discount > 0){
                $price =$course->fees - ($course->discount*$course->fees)/100;
            }else{
                $price =$course->fees;
            }
            $this->db->where(array('course_rating.courseID' => $course->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $course->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;
              if($newwholeRating > 0){
              for($s=1;$s<=$newwholeRating;$s++){
                $stars_rating .= '<i class="icon-23"></i>';	
              }
              if($fractionRating >= 0.25){
                $stars_rating .= '<i class="icon-23 n50"></i>';	
              }
            }
              else{
                for($s=1;$s<=5;$s++){
                 $stars_rating .= '<i class="icon-23 text-secondary"></i>';
                }
              }  
        
      $output .= '<div class="col-md-6 col-lg-4 col-xl-3 sal-animate" data-sal-delay="100" data-sal="slide-up" data-sal-duration="800">
        <div class="edu-course course-style-1 course-box-shadow hover-button-bg-white">
          <div class="inner">
            <div class="thumbnail">
              <a href="base_url(course-details/'.base64_encode($course->id).')">
              <img src="'.base_url($course->image).'" alt="Course Meta" style="height:269px">
              </a>
              <div class="time-top">
                <span class="duration"><i class="icon-61"></i>'.$totalDays.' Days</span>
              </div>
            </div>
            <div class="content">
              <!-- <span class="course-level">Beginner</span> -->
              <h6 class="title">
                <a href="#">'.$course->course.'</a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  '. $stars_rating.'
                </div>
                <span class="rating-count">('.$TotalratingAvg .'/'.count($allOverRating).' Rating)</span>
              </div>
              <div class="course-price">₹ '.$price.' / <del>₹ '.$course->fees.'</del></div>
              <div class="title">Mode <span class="text-success">'.$course->mode.'</span> </div>
              <ul class="course-meta">
                <li><i class="icon-24"></i>'.$course->seats.' Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i>'.$course->remaning_seats.' Remaning Seats</li>
              </ul>
            </div>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content-wrapper">
            <button class="wishlist-btn"><i class="icon-22"></i></button>
          </div>
          <div class="course-hover-content">
            <div class="content">
              <!-- <span class="course-level">Advanced</span> -->
              <h6 class="title">
                <a href="'.base_url('course-details/'.base64_encode($course->id)).'">'.$course->course.'</a>
              </h6>
              <div class="course-rating">
                <div class="rating">
                  <!-- <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i>
                    <i class="icon-23"></i> -->
                  '.$stars_rating.'
                </div>
                <span class="rating-count">('.$TotalratingAvg .'/'.count($allOverRating).' Rating)</span>
              </div>
              <div class="course-price">₹ '.$price.' / <del>₹ '.$course->fees.'</del></div>
              <p>'.$course->description.'</p>
              <ul class="course-meta">
                <li><i class="icon-24"></i>'.$course->seats.'Total Seats</li>
                <li class="text-danger"><i class="icon-24 text-danger"></i>'.$course->remaning_seats.' Remaning Seats</li>
              </ul>
              <a href="'.base_url('course-details/'.base64_encode($course->id)).'" class="edu-btn btn-secondary btn-small">Enrolled <i class="icon-4"></i></a>
            </div>
          </div>
        </div>
      </div>';
       } 
       //echo $courseID;
       if($courseID){
     $output .=  '<div class="load-more-btn sal-animate" id="remove_row" data-sal-delay="100" data-sal="slide-up" data-sal-duration="1200">
      <a href="#" data-id ="'.$courseID.'" id="load_more" class="edu-btn loadButton">Load More <i class="icon-56"></i></a>
     </div>';
      }
     
    echo $output;

    }

    public function getcomment(){
      $courseID = $this->input->post('courseID');
      $comments = $this->course_model->getcomments(array('course_rating.courseID'=>$courseID));
      //print_r($comments);
      if(count($comments)){
        foreach ($comments as $comment){
          ?>
       
          <div class="c_text" ><img src="<?=base_url($comment->profile_pic)?>" width="40" hright="40" alt="" class="img_circle">  <span><?=$comment->description?></span></div>

          <?php }
      }else{
      ?>
    
       <div class="text-danger c_text"> No Comment Found</div>
      <?php
      }
    }

    public function registration_form(){
       $user_type = $this->input->post('user_type');
       $states =$this->stateinfo();
       ?>
              <div class="form-group">
                 <label for="name"><?=$user_type?> Name *</label>
                 <input type="text"  id="name" name="name" placeholder="Please Enter <?=$user_type?> Name ">
               </div>
               <div class="form-group">
                   <label for="email"><?=$user_type?> Email *</label>
                   <input type="email"  id="email" name="email" placeholder="Please Enter <?=$user_type?> Email ">
               </div>
               <div class="form-group">
                   <label for="contact"><?=$user_type?> Mobile No. *</label>
                   <input type="text"  id="contact" name="contact" placeholder="Please Enter <?=$user_type?> Mobile No. " maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
               </div>
               <div class="form-group">
                   <label for="address"><?=$user_type?> Address *</label>
                   <textarea type="text" class="dropdown_css" id="address" name="address" placeholder="Please Enter Address"></textarea>
               </div>
               <div class="form-group">
                   <label for="state">State*</label>
                   <select class="dropdown_css"  id="state" name="state" onchange="getCity(this.value)">
                       <option value="">Choose State</option>
                       <?php foreach($states as $state){?>
                       <option value="<?=$state->id?>"><?=$state->name?></option>
                       <?php  } ?>
                   </select>
               </div>
               <div class="form-group">
                   <label for="city">City*</label>
                   <select class="city dropdown_css" id="city" name="city">
                     <option value="">Choose City</option>
                   </select>
               </div>
               <div class="form-group">
                   <label for="profile_pic"><?=$user_type?> Profile Pic*</label>
                   <input type="file" class="form-control" id="profile_pic" name="profile_pic" >
               </div>
               <div class="form-group">
                   <label for="password">Password*</label>
                   <input type="password" class="" id="password" name="password" placeholder="Please Enter Password">
                   <span class="password-show"><i class="icon-76"></i></span>
               </div>
               <!-- <div class="form-group chekbox-area">
                   <div class="edu-form-check">
                       <input type="checkbox" id="terms-condition">
                       <label for="terms-condition">I agree the User Agreement and <a href="terms-condition.html">Terms & Condition.</a> </label>
                   </div>
               </div> -->
               <div class="form-group">
                   <button type="submit" class="edu-btn btn-medium">Create Account <i class="icon-4"></i></button>
               </div>
       <?php
     }

     public function checkEmail(){
      if($this->session->userdata('otp_email')){
        $email = $this->session->userdata('otp_email');
        $otp = $this->input->post('otp');
      }else{
        $email = $this->input->post('email');
      }
      
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){  
        if($user->otp==$otp){
          echo json_encode(['status'=>200, 'message'=>'OTP Match Success']);
        }else{
          echo json_encode(['status'=>302, 'message'=>'Invalid OTP!']);
        }
      }else{
        echo json_encode(['status'=>302, 'message'=>'Invalid email id!']);
      }
     }

     public function sendOTP(){
      $email = $this->input->post('email');
      if(empty($email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter email ID!']);
        exit();
      }
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if($user){
        $otp = rand ( 100000 , 999999 );
      // echo $user->id;
          $data = array(
            'otp' => $otp,
        );
         $update_otp = $this->user_model->update_user_status($data,array('id'=>$user->id));
          if($update_otp){
          $email = $user->email;
          //$site_name  =   $this->input->post('site_name');
          $subject  =  "Reset Password" ;
          $html = "Your reset password otp is ".$otp ;
          $sendmail = sendEmail($email,$subject,$html);
          if($sendmail){
            $session = array(
             'otp_email' => $email,
            );
            $this->session->set_userdata($session);
            echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
        }else{
            echo json_encode(['status'=>403, 'message'=>'Mail not send please try again!']);    
        }  
      } else{
        echo json_encode(['status'=>403, 'message'=>'something went wrong']);
      }
     }else{
      echo json_encode(['status'=>403, 'message'=>'Invalid email id!']);
      }
     }

     public function resetPassword(){
      $email = $this->input->post('email');
      $otp = $this->input->post('otp');
      $password = $this->input->post('password');
      $confirmpassword = $this->input->post('confirmpassword');
      $user = $this->user_model->get_user(array('users.email' => $email,'users.status' => 1));
      if(empty($otp)){
        echo json_encode(['status'=>403, 'message'=>'Please enter OTP!']);
        exit();
      }
      if(empty($password)){
        echo json_encode(['status'=>403, 'message'=>'Please enter Password!']);
        exit();
      }
      if(empty($confirmpassword)){
        echo json_encode(['status'=>403, 'message'=>'Please enter COnfirm Password!']);
        exit();
      }
      if($user->otp!=$otp){
        echo json_encode(['status'=>403, 'message'=>'Please enter valid otp!']);
        exit();
      }
      if($password!=$confirmpassword){
        echo json_encode(['status'=>403, 'message'=>'Confirm Password and password not match!']);
        exit();
      }

      $data =array(
        'password'=>md5($password),
        'otp'=>0
      );
      $update = $this->user_model->update_user_status($data,array('id'=>$user->id));
      if($update){

        echo json_encode(['status'=>200, 'message'=>'Password Change successfully please login!']);
    }else{
        echo json_encode(['status'=>403, 'message'=>'Password Change failed']);    
    }
      
     }

     public function get_subjects(){
      $courseID = $this->input->post('courseID');
      $subjects = $this->Subject_model->get_subjects(array('subjects.courseID'=>$courseID));
       
     ?>
              <div class="as-sort-bar mb-30">
              <div class="row justify-content-between align-items-center">
                <div class="col-md-auto">
                  <div class="nav" role="tablist"><a href="#" class="active" id="tab-shop-grid" data-bs-toggle="tab" data-bs-target="#tab-grid" role="tab" aria-controls="tab-grid" aria-selected="true"><i class="fas fa-th"></i>Grid</a> <a href="#" id="tab-shop-list" data-bs-toggle="tab" data-bs-target="#tab-list" role="tab" aria-controls="tab-grid" aria-selected="false"><i class="fas fa-list"></i>List</a></div>
                </div>
                <div class="col-md">
                  <p class="woocommerce-result-count"><strong class="text-title"><?=count($subjects)?></strong> Subjects</p>
                </div>
                <!-- <div class="col-md-auto">
                  <form class="woocommerce-ordering" method="get">
                    <select name="orderby" class="orderby" aria-label="Shop order">
                      <option value="menu_order" selected="selected">Default Sorting</option>
                      <option value="popularity">Sort by popularity</option>
                      <option value="rating">Sort by average rating</option>
                      <option value="date">Sort by latest</option>
                      <option value="price">Sort by price: low to high</option>
                      <option value="price-desc">Sort by price: high to low</option>
                    </select>
                  </form>
                </div> -->
              </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade active show" id="tab-grid" role="tabpanel" aria-labelledby="tab-shop-grid">
                <div class="row">
           <?php if(count($subjects)>0){
              foreach($subjects as $subject){?>
                  <div class="col-md-3 col-lg-3 col-xxl-3">
                    <div class="as-box as-box--style4">
                      <!-- <div class="as-box__img">
                        <img src="assets/img/blog/blog-1-2.jpg" alt="course Image" class="w-100">
                        <div class="as-box__category"><a href="course.html">$445</a></div>
                      </div> -->
                      <div class="as-box__middle">
                        <div class="title"> <a ><?=$subject->course?></a></div>
                        <div class="title"> <a ><?=$subject->subject?></a></div>
                      </div>
                      <div class="as-box__bottom">
                        <div class="as-box__meta"><a href="<?=base_url('study-material/'.base64_encode($subject->id))?>" class="btn btn-primary">Explore <i class="far fa-long-arrow-right"></i></a> </div>
                      </div>
                    </div>
                  </div>
                  <?php } } ?>

                </div>
              </div>
              
            </div>
     <?php
      
    }

    public function get_subjects_home(){
      $courseID = $this->input->post('courseID');
      $subjects = $this->Subject_model->get_subjects(array('subjects.courseID'=>$courseID));
        if(count($subjects)>0){
     ?>
            
            <section class="course-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-12 col-xl-12">
            <div class="as-sort-bar mb-30">
              <div class="row justify-content-between align-items-center">
              
                <div class="col-md">
                  <p class="woocommerce-result-count"><strong class="text-title"><?=count($subjects)?></strong> Subjects</p>
                </div>
           
              </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade active show" id="tab-grid" role="tabpanel" aria-labelledby="tab-shop-grid">
                <div class="row">
                <?php 
        $id = "";
          foreach($subjects as $subject){
            $id = $subject->id;
            $earlier = new DateTime($subject->start_date);
            $later = new DateTime($subject->end_date);
            $totalDays =  $later->diff($earlier)->format("%a") +1;
            if($subject->fees > 0 && $subject->discount > 0){
                $price =$subject->fees - ($subject->discount*$subject->fees)/100;
            }else{
                $price =$subject->fees;
            }
            $this->db->where(array('course_rating.courseID' => $subject->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;
        ?>
                  <div class="col-md-6 col-lg-6 col-xxl-3">
                    <div class="as-box as-box--style4">
                      <div class="as-box__img">
                        <img src="<?=base_url($subject->image)?>" alt="course Image" class="w-100" style="width:332px;height:232px;">
                        <div class="as-box__category"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>">₹ <?=$price?> / <del><?=$subject->fees?></del></a></div>
                      </div>
                      <div class="as-box__middle">
                        <div class="as-box__author"><span class="mr-5 daysClass" ><a href="#"><?=$totalDays?> Days</a></span></div>
                        <div class="as-box__rating">
                        <?=$TotalratingAvg .'/'.count($allOverRating)?>
                          <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:<?=$newwholeRating * 20?>%"></span></div>
                        </div>
                      </div>
                      <h3 class="as-box__title"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>"><?=$subject->subject?></a></h3>
                      <div class="as-box__bottom">
                        <div class="as-box__meta"><span><i class="fal fa-file"></i>T-Seats <?=$subject->seats?></span> <span><i class="fal fa-file"></i>R-Seats <?=$subject->remaning_seats?></span> <span><i class="fal fa-eye"></i>Mode : <?=$subject->mode?></span></div>
                      </div>
                      <div class="as-box__bottom ">
                        <div class="text-center"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>" class="as-btn  btn-sm "> Book Now</a></div>
                      </div>
                    </div>
                  </div>
    <?php } ?>
              
                </div>
              </div>

            </div>
            <!-- <div class="as-pagination pt-20">
              <ul>
                <li><a href="blog.html">1</a></li>
                <li><a href="blog.html">2</a></li>
                <li><a href="blog.html">Next<i class="far fa-long-arrow-right"></i></a></li>
              </ul>
            </div> -->
          </div>

        </div>
      </div>
    </section>
    
     <?php
        }else{
          ?>
          <div><h3 class="text-danger text-center">No Subject Found</h3></div>
          <?php
        }
      
    }

    public function get_subject_courses(){
      $courseID = $this->input->post('courseID');
      $subjects = $this->Subject_model->get_subjects(array('subjects.courseID'=>$courseID));
        if(count($subjects)>0){
     ?>
            
            <section class="course-wrapper space-top space-extra-bottom">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-12 col-xl-12">
            <div class="as-sort-bar mb-30">
              <div class="row justify-content-between align-items-center">
              
                <div class="col-md">
                  <p class="woocommerce-result-count"><strong class="text-title"><?=count($subjects)?></strong> Subjects</p>
                </div>
           
              </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade active show" id="tab-grid" role="tabpanel" aria-labelledby="tab-shop-grid">
                <div class="row">
                <?php 
        $id = "";
          foreach($subjects as $subject){
            $id = $subject->id;
            $earlier = new DateTime($subject->start_date);
            $later = new DateTime($subject->end_date);
            $totalDays =  $later->diff($earlier)->format("%a") +1;
            if($subject->fees > 0 && $subject->discount > 0){
                $price =$subject->fees - ($subject->discount*$subject->fees)/100;
            }else{
                $price =$subject->fees;
            }
            $this->db->where(array('course_rating.courseID' => $subject->id));
            $allOverRating =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>5));
            $five =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>4));
            $four =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>3));
            $three =  $this->db->get('course_rating')->result();
        
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>2));
            $two =  $this->db->get('course_rating')->result();
        
            
            $this->db->where(array('course_rating.courseID' => $subject->id,'course_rating.rating'=>1));
            $one =  $this->db->get('course_rating')->result();
            $count_Five = 0;
            $count_Foure = 0;
            $count_Three = 0;
            $count_Two = 0;
            $count_One = 0;
            $total_CountFive = 0;
            $total_CountFoure = 0;
            $total_CountThree = 0;
            $total_CountTwo = 0;
            $total_CountOne = 0;
            if(count($five) > 0){
             $count_Five = count($five);
             $total_CountFive = $count_Five*5;
            }
            if(count($four) > 0){
             $count_Foure = count($four);
             $total_CountFoure = $count_Foure*4;
            }
            if(count($three) > 0){
             $count_Three = count($three);
             $total_CountThree = $count_Three*3;
            }
            if(count($two) > 0){
             $count_Two = count($two);
             $total_CountTwo = $count_Two*2;
            }
            if(count($one) > 0){
             $count_One = count($one);
             $total_CountOne = $count_One*1;
            }
            $sumTotalCountRating = $total_CountFive + $total_CountFoure + $total_CountThree + $total_CountTwo + $total_CountOne;
            $sumCountRating = $count_Five + $count_Foure + $count_Three + $count_Two + $count_One;
            if($sumTotalCountRating > 0 && $sumCountRating > 0){
            $TotalratingAvg = $sumTotalCountRating/$sumCountRating;
            }else{
              $TotalratingAvg = 0;
            }
              
              $stars_rating="";
              $newwholeRating = floor($TotalratingAvg);
              $fractionRating = $TotalratingAvg - $newwholeRating;
        ?>
                  <div class="col-md-6 col-lg-6 col-xxl-3">
                    <div class="as-box as-box--style4">
                      <div class="as-box__img">
                        <img src="<?=base_url($subject->image)?>" alt="course Image" class="w-100" style="width:332px;height:232px;">
                        <div class="as-box__category"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>">₹ <?=$price?> / <del><?=$subject->fees?></del></a></div>
                      </div>
                      <div class="as-box__middle">
                        <div class="as-box__author"><span class="mr-5 daysClass" ><a href="#"><?=$totalDays?> Days</a></span></div>
                        <div class="as-box__rating">
                        <?=$TotalratingAvg .'/'.count($allOverRating)?>
                          <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:<?=$newwholeRating * 20?>%"></span></div>
                        </div>
                      </div>
                      <h3 class="as-box__title"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>"><?=$subject->subject?></a></h3>
                      <div class="as-box__bottom">
                        <div class="as-box__meta"><span><i class="fal fa-file"></i>T-Seats <?=$subject->seats?></span> <span><i class="fal fa-file"></i>R-Seats <?=$subject->remaning_seats?></span> <span><i class="fal fa-eye"></i>Mode : <?=$subject->mode?></span></div>
                      </div>
                      <div class="as-box__bottom ">
                        <div class="text-center"><a href="<?=base_url('course-details/'.base64_encode($subject->id))?>" class="as-btn  btn-sm "> Book Now</a></div>
                      </div>
                    </div>
                  </div>
    <?php } ?>
              
                </div>
              </div>

            </div>
            <!-- <div class="as-pagination pt-20">
              <ul>
                <li><a href="blog.html">1</a></li>
                <li><a href="blog.html">2</a></li>
                <li><a href="blog.html">Next<i class="far fa-long-arrow-right"></i></a></li>
              </ul>
            </div> -->
          </div>

        </div>
      </div>
    </section>
    
     <?php
        }else{
          ?>
          <div><h3 class="text-danger text-center">No Subject Found</h3></div>
          <?php
        }
    }
}