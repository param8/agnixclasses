<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends My_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->not_logged_in();
    $this->load->library('encryption');
    $this->load->model('Order_model');
   // $this->load->model('Wishlist_model');
  }

  public function index()
  {
    $data['page_title'] = 'My Order';
    $data['orders'] = $this->Order_model->get_all_orders(array('orders.uid' => $this->session->userdata('id'))); 
    $this->website_template('myorder',$data);
  }

  public function invoice()
  {
    $order_id = base64_decode($this->uri->segment('2'));
    $data['page_title'] = 'Invoice';
    $data['totalwishlist'] = $this->totalwishlist();
		$data['siteinfo'] = $this->siteinfo();
    $data['schools'] = $this->schools();
    $data['order_details'] = $this->Order_model->get_order_details(array('orders.id'=>$order_id));
    print_r( $data['order_details']); die;
		$data['order_items'] = $this->Order_model->get_order_items(array('order_items.oid'=>$order_id));
    $data['shipping_details'] = $this->Order_model->get_shipping_details(array('uid'=>$data['order_details']->user_id)); 
    $this->load->view('layout/head',$data);
    $this->load->view('layout/header');
    $this->load->view('invoice');
  
  }








}
