<?php 
class Courses extends MY_Controller 
{
	public function __construct()
	{
	  parent::__construct();
    //$this->not_admin_logged_in();
    $this->load->model('course_model');
    $this->load->model('Subject_model');
    //$this->load->model('teachers_model');
    $this->load->library("pagination");
    
	}

	public function index()
  {
    $data['page_title'] = 'Courses';
    $data['category'] = 'All Courses';
    $data['courseID'] = !empty($this->uri->segment(2)) ? base64_decode($this->uri->segment(2)): '';
    
    $data['courses'] = $this->Subject_model->get_subject_courses(array('courses.status' => 1));
		$this->website_template('courses/courses',$data);
  }

  public function course_detail(){
    $userID = $this->session->userdata('id');
     $subjectID = base64_decode($this->uri->segment(2)); 
    //$condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1,'courses.id <>' => $courseID) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1,'courses.id <>' => $courseID):array('courses.status'=>1,'courses.id <>' => $courseID)) ;
    $data['page_title'] = 'Courses Deatil';
    $data['course'] = $this->Subject_model->get_subject(array('subjects.id' => $subjectID));
    $data['ratings'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID));
    $data['ratings_five'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID,'course_rating.rating'=>5));
    $data['ratings_four'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID,'course_rating.rating'=>4));
    $data['ratings_three'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID,'course_rating.rating'=>3));
    $data['ratings_two'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID,'course_rating.rating'=>2));
    $data['ratings_one'] = $this->Subject_model->get_ratings(array('course_rating.courseID' => $subjectID,'course_rating.rating'=>1));
   // $data['courses'] = $this->Subject_model->get_more_courses($condition);
   $data['booked_courses'] = $this->Subject_model->get_count_booked_course(array('courseID' => $subjectID));
   //print_r($data['booked_courses']);die;
   $data['booked'] = $this->session->userdata('user_type')=='Student' ? $this->Subject_model->get_count_booked_course(array('studentID' =>$this->session->userdata('id'),'status'=>1)) : '';
    $data['dublicateRatings'] = $this->Subject_model->get_dubliccate_ratings(array('course_rating.courseID' => $subjectID,'course_rating.userID'=>$userID));
   // $data['teacher'] = $this->teachers_model->get_teacher(array('teachers.subjectID'=>$data['course']->subject));
    
		$this->website_template('courses/courseDetail',$data);
  }

	public function booked_course()
  {
    $userID = $this->session->userdata('id');
    $data['page_title'] = 'Booked Courses';
    $condition = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id')) : ($this->session->userdata('user_type')=='Student' ? array('booked_courses.studentID'=>$this->session->userdata('id')):array('booked_courses.status'=>1)) ;
    $data['booked_courses'] = $this->course_model->get_student_booked_course($condition);
    $condition_courses = $this->session->userdata('user_type')=='Institute' ? array('courses.userID'=>$this->session->userdata('id'),'courses.status'=>1) : ($this->session->userdata('user_type')=='Student' ? array('courses.locationID'=>$this->session->userdata('city'),'courses.status'=>1):array('courses.status'=>1));
    $data['courses'] = $this->course_model->get_courses($condition_courses);
		$this->website_template('courses/booked-courses',$data);
  }

  public function createCourse(){
    $data['page_title'] = 'Create Course';
    $data['categories'] = $this->category_model->get_categories(array('category.status'=>1));
    $data['batchs'] = $this->Common_model->get_batch();
		$this->website_template('courses/create-course',$data);
  }

  public function store(){
    $userID = $this->session->userdata('id');
    $locationID = $this->session->userdata('city');
    $course = $this->input->post('course');
    $free_demo = $this->input->post('free_demo');
    $subject = $this->input->post('subject');
    $categoryID = $this->input->post('category');
    $mode = $this->input->post('mode');
    $start_date = $this->input->post('start_date');
    $end_date = $this->input->post('end_date');
    $seats = $this->input->post('seats');
    $fees = $this->input->post('fees');
    $discount = $this->input->post('discount');
    $teacher = $this->input->post('teacher');
    $description = $this->input->post('description');
    $batch = $this->input->post('batch');
    $check = $this->course_model->get_course(array('courses.course'=>$course,'courses.userID'=>$userID));
    if($check){
      echo json_encode(['status'=>403,'message'=>'This course already exists']);
      exit();
    }

    if(empty($categoryID)){
      echo json_encode(['status'=>403,'message'=>'Please select a category']);
      exit();
    }

    if($categoryID=='Other'){
      if(empty($this->input->post('category_name'))){
        echo json_encode(['status'=>403,'message'=>'Please Enter category Name']);
        exit();
      }
    }
    if(empty($course)){
      echo json_encode(['status'=>403,'message'=>'Please enter a course']);
      exit();
    }
  
    if(empty($seats)){
      echo json_encode(['status'=>403,'message'=>'Please enter a seats']);
      exit();
    }
    if(empty($start_date)){
      echo json_encode(['status'=>403,'message'=>'Please enter a start date']);
      exit();
    }
    if(empty($end_date)){
      echo json_encode(['status'=>403,'message'=>'Please enter a end date']);
      exit();
    }
    if(empty($batch)){
      echo json_encode(['status'=>403,'message'=>'Please please select atleast one batch']);
      exit();
    }
    if(empty($description)){
      echo json_encode(['status'=>403,'message'=>'Please enter a description']);
      exit();
    }
    $this->load->library('upload');
    if($_FILES['image']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/courses',
      'file_name' 	=> str_replace(' ','',$course.$userID).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/courses/'.$config['file_name'].'.'.$type;
      }
    }else{
      $image = 'public/dummy_image.jpg';
    }
   
    $batchs = implode(',',$batch);

    if($categoryID=='Other'){
     $categoryName  =  $this->input->post('category_name');
     $category = $this->category_model->get_category(array('category'=>$categoryName));
     if($category){
      echo json_encode(['status'=>403, 'message'=>'Category already exits!']);
      exit();
     }else{
      $catData = array(
        'userID' =>$this->session->userdata('id'),
        'category'=>$categoryName,
        'status' =>1,
      ) ;
      $categoryID = $this->category_model->store_category($catData);
    }
       
    }

   $data = array(

    'userID' => $userID,
    'locationID' => $locationID,
    'course' => $course,
    'free_demo' => $free_demo,
    'subject' => $subject,
    'categoryID' => $categoryID,
    'mode' => $mode,
    'start_date' => $start_date,
    'end_date' => $end_date,
    'seats' => $seats,
    'remaning_seats' => $seats,
    'fees' => $fees,
    'discount' => $discount,
    'description' => $description,
    'teacher' => $teacher,
    'batch' => $batchs,
    'image' => $image,
   );

   $store = $this->course_model->store_course($data);
   if($store){
         $email = $this->session->userdata('email');
          //$site_name  =   $this->input->post('site_name');
          $subject  =   "Course Add " ;
          $html = "<h1> Your ".$course." is submimited </h1>
          <p>Please wait for approve.. </p>";
          $sendmail = sendEmail($email,$subject,$html);

          $email = 'param.chandel@gmail.com';
          //$site_name  =   $this->input->post('site_name');
          $subject  =  $this->session->userdata('name')." is Course Add " ;
          $html = "<h1>  ".$course." is added by ".$this->session->userdata('name')." wait for approve</h1>";
          $sendmail = sendEmail($email,$subject,$html);
     echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
	 }else{
			echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
		}

  }

  public function store_rating(){
    $userID = $this->session->userdata('id');
    $courseID = $this->input->post('courseID');
    //$title = $this->input->post('title');
    $description = $this->input->post('description');
    $rating = $this->input->post('rating_course');

    if(empty($rating)){
      echo json_encode(['status'=>403,'message'=>'Please select rating']);
      exit();
    }

    // if(empty($title)){
    //   echo json_encode(['status'=>403,'message'=>'Please enter title']);
    //   exit();
    // }

    if(empty($description)){
      echo json_encode(['status'=>403,'message'=>'Please enter description']);
      exit();
    }

    $data = array(
      'userID'      => $userID,
      'courseID'    => $courseID,
      //'title'       => $title,
      'description' => $description,
      'rating'      => $rating,
    );
   
    $store = $this->Subject_model->store_rating($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Your rating send successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }
  }

  public function addStudentCourse(){
    $userID = $this->session->userdata('id');
    $subjectID = $this->input->post('courseID');
    $price = $this->input->post('price');
    $batch = $this->input->post('batch');
    $course = $this->Subject_model->get_subject(array('subjects.id' =>$subjectID));
    // if(empty($batch)){
    //   echo json_encode(['status'=>403,'message'=>'Please select batch']);
    //   exit();
    // }
    $data = array(
      'studentID' =>$userID,
      'courseID' =>$subjectID,
      'price' =>$price,
      'batch' =>$batch,
    );
  
    $store = $this->Subject_model->store_student_course($data);
    if($store){
      $message = $this->session->userdata('name')." has sent request to book course ".$course->course;
      $notification = array(
        'userID' => $userID,
        'send_by' => $this->session->userdata('id'),
        'message' => $message
      );
      
      $store_notification = $this->notification_model->store_notification($notification);
 
      echo json_encode(['status'=>200, 'message'=>'Course booked  successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }

  }

  public function studentCourseStatus(){
    $id = $this->input->post('id');
    $courseID = $this->course_model->get_booked_course(array('id' =>$id));
    $course = $this->course_model->get_course(array('courses.id'=>$courseID->courseID));
    $data = array(
      'status' => 1
    );

    $update= $this->course_model->updateStudentCourse($data,$id);
    if($update){
      if($course->remaning_seats > 0){
        $seats = $course->remaning_seats-1;
        $courseData = array(
          'remaning_seats' => $seats,
        );

        $this->course_model->updateCourse($courseData,$course->id);
        $message = $this->session->userdata('name')." has accepted your booking request course ".$course->course;
        $notification = array(
          'userID' => $courseID->studentID,
          'send_by' => $this->session->userdata('id'),
          'message' => $message
        );
        
        $store_notification = $this->notification_model->store_notification($notification);
          $email = $course->user_email;
          //$site_name  =   $this->input->post('site_name');
          $subject  =   "Your Course Approved " ;
          $html = "<h1>".$course->course." is approved </h1>
          <p>Please continue to study.. </p>";
          //$sendmail = sendEmail($email,$subject,$html);
      }
      echo json_encode(['status'=>200, 'message'=>'Booked status active successfully!']);
    }else{
       echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
     }
  }

  public function my_books(){
    $condition =  array('courses.status'=>1);
    $data['page_title'] = 'My-Books';
    $data['courses'] = $this->course_model->get_courses($condition);
		$this->website_template('courses/my-books',$data);
  }

  public function study_material(){
    $subjectID = base64_decode($this->uri->segment(2));
    $data['subject'] = $this->Subject_model->get_subject(array('subjects.id'=>$subjectID,'subjects.status'=>1));
    $data['page_title'] = $data['subject']->course.' ('.$data['subject']->subject.')'. ' Study Material';
    //$data['courses'] = $this->course_model->get_courses($condition);
		$this->website_template('courses/study-material',$data);
  }


  

}