<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->not_logged_in();
	  $this->load->model('Order_model');
      $this->load->model('Cart_model');
	
	}

	public function index(){
    $data['page_title'] = 'Cart';
    $this->website_template('cart',$data);
	}

  public function addCart(){
    $productID =$this->input->post('bookID');
    $cartItem = $this->Order_model->get_item(array('id'=>$productID));
    $data = array(
       'id'   => $productID,
       'name' => $cartItem->name,
       'qty' => 1,
       'image' =>$cartItem->image,
       'price' =>$cartItem->price ,
    );

    $cart = $this->cart->insert($data);
    //$cartItem=$this->cart->contents();
    echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
 }

 public function update_cart(){
   if($this->input->post('condition')=='minus'){
    $qty =  $this->input->post('qty')-1;
   }

   if($this->input->post('condition')=='plus'){
    $qty =  $this->input->post('qty')+1;
   }
  
  $rowid =  $this->input->post('rowid');
  $data = array(
   'rowid' => $rowid,
   'qty'   => $qty,
  );
  //Print_r($data);die;
  $update=$this->cart->update($data);
  echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
 }

 public function removeCartItem() {   
   $rowid =  $this->input->post('rowid');
   $data = array(
       'rowid'   => $rowid,
       'qty'     => 0
   );
   echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
   $update=$this->cart->update($data);
}

public function checkout(){
  $data['page_title'] = 'Checkout';
  $data['states'] = $this->Common_model->get_states();
  $data['addresses'] = $this->Order_model->get_shipping_address();
  $this->website_template('checkout',$data);
 }

 public function delete_address(){
  $id = $this->input->post('id');
  $data = array(
    'status' => 0
  );
  $delete = $this->Cart_model->update_address($data,$id);
  if($delete)
		{
			echo json_encode(['status'=>200, 'message'=>'Delete shipping address successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
      exit();
		}
 }

 public function store_shippingAddress(){
  $name = $this->input->post('name');
  $email = $this->input->post('email');
  $mobile = $this->input->post('mobile');
  $address = $this->input->post('address');
  $street = $this->input->post('street');
  $pin_code = $this->input->post('pin_code');
  $state = $this->input->post('state');
  $city = $this->input->post('city');
  $userID = $this->session->userdata('id');
  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a name']);
      exit();
  }
  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a email']);
      exit();
  }
  if(empty($mobile)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a mobile']);
      exit();
  }
  if(empty($address)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a address']);
      exit();
  }
  if(empty($street)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a street']);
      exit();
  }
  if(empty($pin_code)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a pincode']);
      exit();
  }
  if(empty($state)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a state']);
      exit();
  }
  if(empty($city)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a city']);
      exit();
  }
 $data = array(
  'uid' => $userID,
  'name' => $name,
  'mobile' => $mobile,
  'email' => $email,
  'address' => $address,
  'street' => $street,
  'pin_code' => $pin_code,
  'city' => $city,
  'state' => $state,
  'create_date' => date('Y-m-d H:i:s'),
  'modify_date' => date('Y-m-d H:i:s'),
  'status' => 1,
 );

  $store = $this->Order_model->store_shippingAddress($data);
  if($store)
  {
    echo json_encode(['status'=>200, 'message'=>'Shipping address add successfully!']);
        exit();
  }else{
    echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
    exit();
  }
 }

 public function cart_detail(){
  $orderID = $this->input->post('orderID');

 
  $items = $this->Order_model->get_order_items(array('item_order.orderID'=> $orderID));
 
  ?>
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Item Details</h5>
     
    </div>
    <div class="modal-body">
      <div class="table-responsive">
            <table class="table table-striped check-tbl">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Item name</th>
                  <th>Price</th>
                  <th>QTY</th>
                  <th>Total Price</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($items as $item){?>
                <tr>
                  <td class="product-item-img"><img src="<?= base_url($item['image'])?>" alt="" style="width:100%;height:100px;"></td>
                  <td class=""><?= $item['name']?></td>
                  <td class=""><i class="fa fa-inr"></i> <?= intval($item['price'])?></td>
                  <td class=""><?= $item['qty']?></td>
                  <td class=""><i class="fa fa-inr"></i> <?= $item['qty'] * intval($item['price'])?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
    </div>
    <?php 
}

 public function placeorder(){
  $payment_method = $this->input->post('payment_type');
  $address = $this->input->post('address');
  $totalQty = $this->cart->total_items();
  $totalPice = $this->cart->total();

  if(empty($totalQty)){
    echo json_encode(['status'=>403, 'message'=>'Please add book in cart']);
    exit();
  }
 $orderID = 'AGNIX'.rand(111111111, 9999999999);
  $data= array(
    'orderID' =>$orderID,
    'uid' =>$this->session->userdata('id'),
    'price' =>$totalPice,
    'qty' =>$totalQty,
    'address' =>$address,
    'payment_method' =>$payment_method
  );
    
  $storeOrder = $this->Order_model->storeOrder($data);
  if($storeOrder){
    $cartItems=$this->cart->contents();
    foreach($cartItems as $items){
      $Orderitems = array(
        'orderID' => $storeOrder,
        'productID' =>$items['id'],
        'name' =>$items['name'],
        'price' =>$items['price'],
        'qty' =>$items['qty'],
       );
       $storeItemOrder = $this->Order_model->storeOrderItems($Orderitems);
    }
    $this->cart->destroy();
    echo json_encode(['status'=>200, 'message'=>'Thanks for place order!']);
    
    exit();
  
  }else{
    echo json_encode(['status'=>403, 'message'=>'somethings went wrong']);
    exit();
  }

 }

	
}
