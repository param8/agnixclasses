<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('course_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 
		//print_r($this->session->userdata());die;
		 $data['page_title'] = 'Dashboard';
		 $data['Institute'] = $this->user_model->get_all_users(array('users.user_type' => 'Teacher'));
		 $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Student'));
		 $data['experts'] = $this->user_model->get_all_users(array('users.user_type' => 'Doctor'));
		//  $data['get_cources'] = $this->institute_cources();
		//  $data['booked_courses'] = $this->student_booked_cources();
		//  $data['total_cource'] = $this->db->count_all_results('courses');
	     $this->admin_template('dashboard',$data);
		
	}

//     public function addUID(){
        
//         $users = $this->db->get('users')->result();
//         foreach ($users as $user){
// 			$userPassword = $user->password;
// 			$this->db->where('id', $user->id);
// 			$this->db->update('users',array('password'=>md5($userPassword)));
//         }
//     }

//     public function institute_cources(){
// 		$total_cources = $this->course_model->get_all_courses(array('users.user_type' => 'Teacher'));
// 		$color2 = array('0'=>'GREEN','1'=>'Blue','2'=>'Yellow','3'=>'#ADD8E6','4'=>'#0D98BA','5'=>'#964B00 ','6'=>'#CBC3E3','7'=>'#80461B','8'=>'#2A52BE','9'=>'#90EE90','10'=>'Green','11'=>'#ffc107','12'=>'Yellow');
// 	    foreach($total_cources as $key=>$total_cource){
// 		$institute_name = $total_cource['name'];
// 		$color_name =  $color2[$key];
// 		$courseCount = $total_cource['object']['courseTotal'];
// 		$data[] ="['".$institute_name."',".floatval($courseCount).","."'color:".$color_name."'"."],";
// 		//$data[] = "['".$institute_name."',".$courseCount."],";
// 	   }
// 	   //print_r($data);die;
// 	  return $data;
// }

// public function student_booked_cources(){
// 	$booked_courses = $this->course_model->get_booked_course_by_student();
// 	$color2 = array('0'=>'Gray','1'=>'Blue','2'=>'Yellow','3'=>'#ADD8E6','4'=>'#0D98BA','5'=>'#964B00 ','6'=>'#CBC3E3','7'=>'#80461B','8'=>'#2A52BE','9'=>'#90EE90','10'=>'Green','11'=>'#ffc107','12'=>'Yellow');
//    //print_r($booked_courses);die;
// 	foreach($booked_courses as $key=>$booked_course){
// 		$color_name =  $color2[$key];
//         $course_name = $booked_course['course'];
// 		$studentCount = count($booked_course['object']);
// 		$data[] ="['".$course_name."',".floatval($studentCount).","."'color:".$color_name."'"."],";
// 	}
// 	//print_r($data);die;
// 	return $data;
// }




}