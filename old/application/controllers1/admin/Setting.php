<?php
class Setting extends MY_Controller
{
    public function __construct()
    {
      parent::__construct();
      //echo base_url();die;
     // $this->not_admin_logged_in();
      $this->load->model('Common_model');
    }
   public function store_contact(){
    $name = $this->input->post('name');
    $email = $this->input->post('email');
    $mobile = $this->input->post('mobile');
    $msg= $this->input->post('message');

    if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
        exit();
    }
    if(empty($email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
        exit();
    }
    if(empty($mobile)){
        echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
        exit();
    }
    $data = array(
        'name'                => $name,
        'email'               => $email,
        'mobile'              => $mobile ,
        'message'              => $msg ,
    );
    $store = $this->Common_model->store_contact($data);

    if($store){
        echo json_encode(['status'=>200, 'message'=>'Enquiry submitted successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }
    }
    public function site_setting()
    {  	$data['page_title'] = 'Site Info';
        $data['siteinfo'] = $this->siteinfo();
        $data['siteinfo'] = $this->Common_model->get_site_info();
        $this->admin_template('site_setting',$data);
    }

    public function store_siteInfo(){
       $name = $this->input->post('name');
       $mobile = $this->input->post('mobile');
       $whatsapp_no = $this->input->post('whatsapp_no');
       $email = $this->input->post('email');
       $address = $this->input->post('address');
       $facebook_url = $this->input->post('facebook_url');
       $youtube_url = $this->input->post('youtube_url');
       $linkdin_url = $this->input->post('linkdin_url');
       $twitter_url = $this->input->post('twitter_url');
       $insta_url = $this->input->post('insta_url');
       $footer_data = $this->input->post('footer_data');
       $siteinfo = $this->Common_model->get_site_info();
       if(empty($name)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site name']); 	
        exit();
       }
       if(empty($mobile)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site mobile']); 	
        exit();
       }
       if(strlen((string)$mobile) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter mobile number 10 digits']);
        exit();
       }
       if(!empty($whatsapp_no)){
       if(strlen((string)$whatsapp_no) != 10){
        echo json_encode(['status'=>403, 'message'=>'Please enter whatsapp number 10 digits']);
        exit();
       }
      }

       if(empty($email)){
        echo json_encode(['status'=>403, 'message'=>'Please enter site email']); 	
        exit();
       }

       if(empty($footer_data)){
        echo json_encode(['status'=>403, 'message'=>'Please enter footer data']); 	
        exit();
       }
     $this->load->library('upload');
    if(!empty($_FILES['image']['name'])){
     $config = array(
      'upload_path' 	=> 'uploads/siteInfo',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
     );
     $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('image'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]); 	
          exit();
      }
      else
      {
        $type = explode('.', $_FILES['image']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/siteInfo/'.$config['file_name'].'.'.$type;
      }
    }elseif(!empty($siteinfo->image)){
      $image = $siteinfo->image;
    }else{
      $image = 'public/website/images/dummy_image.jpg';
    }
      $data = array(
       'name' => $name,
       'mobile' => $mobile,
       'mobile' => $mobile,
       'email' => $email,
       'whatsapp_no' => $whatsapp_no,
       'address' => $address,
       'facebook_url' => $facebook_url,
       'youtube_url' => $youtube_url,
       'linkdin_url' => $linkdin_url,
       'twitter_url' => $twitter_url,
       'insta_url' => $insta_url,
       'footer_data' => $footer_data,
       'image' => $image,
      );

      $update = $this->Common_model->store_siteInfo($data);

      if($update){
        echo json_encode(['status'=>200, 'message'=>'Site-info update successfully!']);
    }else{
        echo json_encode(['status'=>302, 'message'=>'Something wrong happened']);   
    }

    }

    public function about()
    {	
        $data['page_title'] = 'About us';
        $data['siteinfo'] = $this->siteinfo();
        $data['about_us'] = $this->Common_model->get_about();
        $this->admin_template('about_us',$data);
    }

    public function store_about_us(){
      $title = $this->input->post('title');
      $description = $this->input->post('desc');
      $short_description = $this->input->post('short_description');
      $aboutus_image = $this->input->post('aboutus_image');

      if(empty($title)){
       echo json_encode(['status'=>403, 'message'=>'Please enter title']); 	
       exit();
      }
      if(empty($description)){
       echo json_encode(['status'=>403, 'message'=>'Please enter description']); 	
       exit();
      }

      if(empty($short_description)){
        echo json_encode(['status'=>403, 'message'=>'Please enter short description']); 	
        exit();
       }

       if(strlen($short_description) >= 200){
        echo json_encode(['status'=>403, 'message'=>'Please enter short description 200 characters']); 	
        exit();
       }
      
      $this->load->library('upload');
      if(!empty($_FILES['image']['name'])){
        $name = $_FILES['image']['name'];
        $config = array(
        'upload_path' 	=> 'uploads/siteInfo',
        'file_name' 	=> uniqid().str_replace(' ','_',$name),
        'allowed_types' => 'doc|jpg|jpeg|png|pdf|PDF|txt',
        'max_size' 		=> '10000000',
        );
        $this->upload->initialize($config);
      if ( ! $this->upload->do_upload('image'))
        {
            $error = $this->upload->display_errors();
            echo json_encode(['status'=>403, 'message'=>$error]); 	
            exit();
        }
        else
        {
          $image = 'uploads/siteInfo/'.$config['file_name'];
        }
      }elseif(!empty($aboutus_image)){
        $image = $aboutus_image;
      }else{
        $image = 'public/website/images/dummy_image.jpg';
      }
     $data = array(
      'title' => $title,
      'description' => $description,
      'short_description' => $short_description,
      'image' => $image
     );

     $update = $this->Common_model->store_aboutus($data);
     if($update){
      echo json_encode(['status'=>200, 'message'=>'About us content updated successfully!']); 	
      exit();
     }
   }

   public function enquiry()
   {
      $data['page_title'] = 'Enquiry';
      $data['siteinfo'] = $this->siteinfo();
      $data['enquiries'] = $this->Common_model->get_all_enquiry();
      $this->admin_template('enquiry',$data);
     
   }

}

?>