<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
		$this->load->model('Subject_model');
    $this->load->model('setting_model');
	}


	public function index()
	{	$data['page_title'] = 'My-Account';
         //get_client_ip();
		// $PublicIP = $this->get_client_ip();
		// $json     = file_get_contents("http://ipinfo.io/$PublicIP/geo");
		// $json     = json_decode($json, true);
		// print_r($json);
		// $country  = $json['country'];
		// $region   = $json['region'];
		// $city     = $json['city'];
		$data['subjects'] = $this->Subject_model->get_subjects(array('subjects.status'=>1));
      $this->website_template('signin',$data);

	}

	function get_client_ip()
	{
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		} else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_FORWARDED'])) {
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		} else if (isset($_SERVER['REMOTE_ADDR'])) {
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$ipaddress = 'UNKNOWN';
		}
	
		return $ipaddress;
	}

	public function login(){
		 $email = $this->input->post('email');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);

		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>403, 'message'=>'Your are not active please contact the administrator']);   
		}else{
      echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type')]);
    }
	}

	public function chat(){
		redirect('chat/index.php?email='.base64_encode($this->session->userdata('email')).'&unique_id='.base64_encode($this->session->userdata('unique_id')));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home', 'refresh');
	}

	public function forget_password()
	{	$data['page_title'] = 'Reset Password';
    $this->website_template('user/forget_password',$data);

	}


	public function register(){
       $siteinfo=$this->siteinfo();
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type = $this->input->post('user_type');
		$subjectID = $user_type == 'Teacher' ? $this->input->post('subjectID'): array();
		$ref_userID = $user_type == 'Student' ? $this->input->post('ref_userID'): '';
        
		if(empty($user_type)){
			echo json_encode(['status'=>403, 'message'=>'Please select a user type']); 	
			exit();
		}

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}

		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			exit();
		}
		$checkEmail = $this->User_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}
	
		if($user_type=='Teacher'){
			if(empty($subjectID)){
				echo json_encode(['status'=>403, 'message'=>'Please select a subject']); 	
				exit();
			}

			
		}
   
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}

		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}

    $this->load->library('upload');
    if($_FILES['profile_pic']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
    }else{
      $image = 'public/dummy_image.jpg';
    }

    
	if($user_type=='Student'){
		if(!empty($ref_userID)){
          $get_ref_id = $this->User_model->get_user(array('unique_id'=>$ref_userID));
		  if($get_ref_id->unique_id !==$ref_userID){
			echo json_encode(['status'=>403, 'message'=>'This referral id does not exist please try again']); 	
			exit();
		  }
		}
	}
	$subject = implode(',', $subjectID);
	    $ran_id = rand(time(), 100000000);
		$data = array(
			'unique_id'   => $ran_id,
			'name'        => $name,
			'email'       => $email,
			'password'    => md5($password),
			'contact'     => $contact,
			'subjectID'   => $subject,
			'ref_userID'  => $ref_userID,
			'address'     => $address,
			'user_type'   => $user_type,
            'profile_pic' => $image,
			'login_status' =>'Offline now',
			'otp' =>0,
		);
		$register = $this->Auth_model->register($data);

		if($register){
      if($user_type=='Teacher'){
        $teacherData = array(
          'adminID'        => $register,
          'site_name'      => $name,
		  'site_email'     => $email,
          'site_contact'   => $contact ,
          'site_address'   => $address ,
          'site_logo'      => $siteinfo->site_logo,
          'footer_contant' => $siteinfo->footer_contant,
        );
        $this->setting_model->store_siteInfo($teacherData);
      }

	  if($user_type=='Student'){
        $data = array(
          'studentID'      => $register,
        );
        $this->User_model->store_student_detail($data);
      }
		  echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
		}
	}
//   Admin Cradinciales

public function adminIndex()
{	$data['page_title'] = 'Login';
	$data['siteinfo'] = $this->siteinfo();
	$this->load->view('admin/layout/login_head',$data);
	$this->load->view('admin/login');
	$this->load->view('admin/layout/login_footer');
}

public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect('login', 'refresh');
}
	
	
}