<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->not_logged_in();
	  $this->load->model('Order_model');
	
	}

	public function index(){
		$data['page_title'] = 'Books';

		$data['books'] = $this->Order_model->get_all_items();
		$this->website_template('store',$data);
	}

    public function redirect_notification(){
        $id =$this->session->userdata('id');
        $data=array(
            'status' => 1
        );
        $status = $this->notification_model->update_notification($data,$id);
    }

	
}
