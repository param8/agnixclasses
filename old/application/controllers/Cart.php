<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->not_logged_in();
	  $this->load->model('Order_model');
    $this->load->model('Cart_model');
    $this->load->model('subject_model');
    $this->load->model('user_model');
    
	
	}

	public function index(){
    $data['page_title'] = 'Cart';
    $data['wallet'] = $this->user_model->get_wallet(array('userID'=>$this->session->userdata('id')));
    $this->website_template('cart',$data);
	}

  public function addCart(){
    $productID =$this->input->post('bookID');
    $type = $this->input->post('type');
    $price = round($this->input->post('price'));
    if($type=='Course'){
      $cartItem = $this->subject_model->get_subject(array('subjects.id'=>$productID));
      $name = $cartItem->course.'('.$cartItem->subject.')';
      $image = $cartItem->image;
    }else{
      $cartItem = $this->Order_model->get_item(array('id'=>$productID));
      $name = $cartItem->name;
      $image = $cartItem->image;
    }
    //print_r($cartItem);
    $data = array(
       'id'   => $productID,
       'name' => $name,
       'qty' => 1,
       'image' =>$image,
       'price' =>$price,
       'options' => array('type' => $type)
    );
    // print_r($data);
    $cart = $this->cart->insert($data);
    //$cartItem=$this->cart->contents();
    echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
 }

 public function update_cart(){
   if($this->input->post('condition')=='minus'){
    $qty =  $this->input->post('qty')-1;
   }

   if($this->input->post('condition')=='plus'){
    $qty =  $this->input->post('qty')+1;
   }
  
  $rowid =  $this->input->post('rowid');
  $data = array(
   'rowid' => $rowid,
   'qty'   => $qty,
  );
  //Print_r($data);die;
  $update=$this->cart->update($data);
  echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
 }

 public function removeCartItem() {   
   $rowid =  $this->input->post('rowid');
   $data = array(
       'rowid'   => $rowid,
       'qty'     => 0
   );
   echo json_encode(['status'=>200, 'totalCount'=> $this->cart->total_items()]);
   $update=$this->cart->update($data);
}

public function checkout(){
  $data['page_title'] = 'Checkout';
  $data['states'] = $this->Common_model->get_states(array('country_id'=>154));
  $data['addresses'] = $this->Order_model->get_shipping_address();
  $data['wallet'] = $this->user_model->get_wallet(array('userID'=>$this->session->userdata('id')));
  $this->website_template('checkout',$data);
 }

 public function delete_address(){
  $id = $this->input->post('id');
  $data = array(
    'status' => 0
  );
  $delete = $this->Cart_model->update_address($data,$id);
  if($delete)
		{
			echo json_encode(['status'=>200, 'message'=>'Delete shipping address successfully!']);
      		exit();
		}else{
			echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
      exit();
		}
 }

 public function store_shippingAddress(){
  $name = $this->input->post('name');
  $email = $this->input->post('email');
  $mobile = $this->input->post('mobile');
  $address = $this->input->post('address');
  $street = $this->input->post('street');
  $pin_code = $this->input->post('pin_code');
  $state = $this->input->post('state');
  $city = $this->input->post('city');
  $userID = $this->session->userdata('id');
  if(empty($name)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a name']);
      exit();
  }
  if(empty($email)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a email']);
      exit();
  }
  if(empty($mobile)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a mobile']);
      exit();
  }
  if(empty($address)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a address']);
      exit();
  }
  if(empty($street)){
    echo json_encode(['status'=>403, 'message'=> 'Please enter a street']);
      exit();
  }
  if(empty($pin_code)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a pincode']);
      exit();
  }
  if(empty($state)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a state']);
      exit();
  }
  if(empty($city)){
    echo json_encode(['status'=>403, 'message'=> 'Please choose a city']);
      exit();
  }
 $data = array(
  'uid' => $userID,
  'name' => $name,
  'mobile' => $mobile,
  'email' => $email,
  'address' => $address,
  'street' => $street,
  'pin_code' => $pin_code,
  'city' => $city,
  'state' => $state,
  'create_date' => date('Y-m-d H:i:s'),
  'modify_date' => date('Y-m-d H:i:s'),
  'status' => 1,
 );

  $store = $this->Order_model->store_shippingAddress($data);
  if($store)
  {
    echo json_encode(['status'=>200, 'message'=>'Shipping address add successfully!']);
        exit();
  }else{
    echo json_encode(['status'=>403, 'message'=>'some things went wrong']);
    exit();
  }
 }

 public function cart_detail(){
  $orderID = $this->input->post('orderID');

 
  $items = $this->Order_model->get_order_items(array('item_order.orderID'=> $orderID));
 
  ?>
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Item Details</h5>
     
    </div>
    <div class="modal-body">
      <div class="table-responsive">
            <table class="table table-striped check-tbl">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Item name</th>
                  <th>Price</th>
                  <th>QTY</th>
                  <th>Total Price</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($items as $item){?>
                <tr>
                  <td class="product-item-img"><img src="<?= base_url($item['image'])?>" alt="" style="width:100%;height:100px;"></td>
                  <td class=""><?= $item['name']?></td>
                  <td class=""><i class="fa fa-inr"></i> <?= intval($item['price'])?></td>
                  <td class=""><?= $item['qty']?></td>
                  <td class=""><i class="fa fa-inr"></i> <?= $item['qty'] * intval($item['price'])?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>
    </div>
    <?php 
}

 public function placeorder(){
 
  $wallet = $this->user_model->get_wallet(array('userID'=>$this->session->userdata('id')));
  $payment_method = $this->input->post('payment_type');
  $address = $this->input->post('address');
  $totalQty = $this->cart->total_items();
  $totalPrice = $this->input->post('wallet') == 'Yes' ? $this->cart->total()-$wallet->amount : $this->cart->total();
  
  if(empty($totalQty)){
    echo json_encode(['status'=>403, 'message'=>'Please add book in cart']);
    exit();
  }
 $orderID = 'AGNIX'.rand(111111111, 9999999999);
  $data= array(
    'orderID' =>$orderID,
    'uid' =>$this->session->userdata('id'),
    'price' =>$totalPrice,
    'qty' =>$totalQty,
    'address' =>$address,
    'payment_method' =>'Khalti',
    'wallet' => $this->input->post('wallet'),
    'wallet_price' => $wallet->amount,
    'payment_status' => 'UnPaid'
  );
    
  $storeOrder = $this->Order_model->storeOrder($data);
  $product_detail =array();
  if($storeOrder){
    $cartItems=$this->cart->contents();
    foreach($cartItems as $items){
      $Orderitems = array(
        'orderID' => $storeOrder,
        'productID' =>$items['id'],
        'name' =>$items['name'],
        'price' =>$items['price'],
        'qty' =>$items['qty'],
        'type' =>$items['options']['type']
       );
       $storeItemOrder = $this->Order_model->storeOrderItems($Orderitems);
       $product_detail[] = ["identity"=> $items['id'],"name"=> $items['name'],"total_price"=> round($totalPrice),"quantity"=>$items['qty'],"unit_price"=> round($items['price'])];
    }
   $product_details =  json_encode($product_detail);
   //live_secret_key_a1615657b2ea400f88aef1b14bdc10b3
  //  live_secret_key_68791341fdd94846a146f0457ff7b455
    $curl = curl_init();
    $header_data = array(
      'Authorization: Key live_secret_key_a1615657b2ea400f88aef1b14bdc10b3',
      'Content-Type: application/json',
      
    );
     $totalAmount = round($totalPrice)*100;
   $data_json =  array(
      CURLOPT_URL => 'https://khalti.com/api/v2/epayment/initiate/',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{
      "return_url": "'.base_url('Cart/responce').'",
      "website_url": "'.base_url('Cart/responce').'",
      "amount": '.$totalAmount.',
      "purchase_order_id": "'.$orderID.'",
      "purchase_order_name": "Product",
      "customer_info": {
          "name": "'.$this->session->userdata('name').'",
          "email": "'.$this->session->userdata('email').'",
          "phone": "'.$this->session->userdata('phone').'"
      },
      "amount_breakdown": [
          {
              "label": "Price",
              "amount": '.$totalAmount.'
          },
          {
              "label": "VAT",
              "amount": 0
          }
      ],
      
      "product_details": '.$product_details.'
      
    }',
      CURLOPT_HTTPHEADER => $header_data,
    );
   // print_r($data_json);
    curl_setopt_array($curl,$data_json);
    $response = curl_exec($curl);
    curl_close($curl);
     $json_decode = json_decode($response);
     echo $response;
    //  if($json_decode->error_key=='validation_error'){
    //   echo json_encode(['status'=>403, 'message'=>$response]);
    //     exit();
    //  }else{
    //   echo json_encode(['status'=>200, 'payment'=>$response]);
    //     exit();
    //  }
    


  }

 }


 public function pay(){
  $id = base64_decode($this->uri->segment(3));
  $order = $this->Order_model->get_order_details(array('orders.id'=>$id));
  $totalPrice = $order->price;
  
  $product_detail =array();
  $cartItems = $this->Order_model->get_order_items(array('item_order.orderID'=>$id));
    foreach($cartItems as $items){

       $product_detail[] = ["identity"=> $items['productID'],"name"=> $items['name'],"total_price"=> round($totalPrice),"quantity"=>$items['qty'],"unit_price"=> round($items['price'])];
    }
   $product_details =  json_encode($product_detail);

    $curl = curl_init();
    $header_data = array(
      'Authorization: Key live_secret_key_a1615657b2ea400f88aef1b14bdc10b3',
      'Content-Type: application/json',
      
    );
    $orderID = $order->orderID;
     $totalAmount = round($totalPrice)*100;
   $data_json =  array(
      CURLOPT_URL => 'https://khalti.com/api/v2/epayment/initiate/',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>'{
      "return_url": "'.base_url('Cart/responce').'",
      "website_url": "'.base_url('Cart/responce').'",
      "amount": '.$totalAmount.',
      "purchase_order_id": "'.$orderID.'",
      "purchase_order_name": "Product",
      "customer_info": {
          "name": "'.$this->session->userdata('name').'",
          "email": "'.$this->session->userdata('email').'",
          "phone": "'.$this->session->userdata('phone').'"
      },
      "amount_breakdown": [
          {
              "label": "Price",
              "amount": '.$totalAmount.'
          },
          {
              "label": "VAT",
              "amount": 0
          }
      ],
      
      "product_details": '.$product_details.'
      
    }',
      CURLOPT_HTTPHEADER => $header_data,
    );
    curl_setopt_array($curl,$data_json);
    $response = curl_exec($curl);
    curl_close($curl);
    $json_decode = json_decode($response);
    redirect($json_decode->payment_url);


 }

 public function responce(){
  $wallet = $this->user_model->get_wallet(array('userID'=>$this->session->userdata('id')));
   $orderID = $_GET['purchase_order_id'];
   $amount = $_GET['amount'];
   $txnId  = $_GET['txnId'];
   $pidx = $_GET['pidx'];

   $order = $this->Order_model->get_order_details(array('orders.orderID'=>$orderID));
   $data = array(
    'merchantID'     => $pidx,
    'txnID'          => $txnId,
    'payment_status' => 'Paid',
    'status'         => 1,
   );
  $update =  $this->Order_model->update_order($data,$order->id);
  if($update){
    $cartItems=$this->cart->contents();
    foreach($cartItems as $items){
      if($items['options']['type']=='Course'){
        $subject = $this->subject_model->get_subject(array('subjects.id'=>$items['id']));
        $booked_course = array(
          'studentID' => $this->session->userdata('id'),
          'courseID'  => $items['id'],
          'price'  => $items['price'],
          'batch'  => $subject->batch,
        );
      }  
       $storeBookedOrder = $this->subject_model->store_student_course($booked_course);
    }
    if($order->wallet=='Yes'){
      $wallet_data =array(
        'point'  => 0,
        'amount' => $wallet->amount-$order->wallet_price
      );
        $this->user_model->update_wallet($wallet_data,array('user_uniqueID'=>$this->session->userdata('unique_id')));
        $wallet_history = array(
          'user_uniqueID' => $this->session->userdata('unique_id'),
          'userID'        => $this->session->userdata('id'),
          'point'         => $wallet->point,
          'amount'        => $order->wallet_price,
          'type'          => 'Debit'
        );
      $this->user_model->store_wallet_history($wallet_history);
    }
      $this->cart->destroy(); 
      redirect(base_url('order'));
  }

 }

	
}
