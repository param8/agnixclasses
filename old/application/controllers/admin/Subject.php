<?php 
class Subject extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('course_model');
		$this->load->model('Subject_model');
	}

	public function index()
	{	
		$data['page_title'] = 'Subject';
		$condition =  array() ;
        $data['subjects'] = $this->Subject_model->get_subjects($condition);
		$this->admin_template('subject/subject',$data);
	}

	public function create_subject()
	{	
		$data['page_title'] = 'Create Subject';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$this->admin_template('subject/create-subject',$data);
	}

	public function store(){
		$courseID = $this->input->post('courseID');
		$subject = $this->input->post('subject');
		$free_demo = !empty($this->input->post('free_demo')) ? $this->input->post('free_demo') : 0;
		$categoryID = $this->input->post('category');
		$mode = $this->input->post('mode');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$seats = $this->input->post('seats');
		$fees = $this->input->post('fees');
		$discount = !empty($this->input->post('discount')) ? $this->input->post('discount') : 0;
		$teacher = $this->input->post('teacher');
		$description = $this->input->post('description');
		$batch = $this->input->post('batch');
		$zoom_link = $this->input->post('zoom_link');
		$check = $this->Subject_model->get_subject(array('subjects.courseID'=>$courseID,'subjects.subject'=>$subject));
		if($check){
		  echo json_encode(['status'=>403,'message'=>'This subject already exists in this course']);
		  exit();
		}
	
		if(empty($courseID)){
		  echo json_encode(['status'=>403,'message'=>'Please select a course']);
		  exit();
		}
	
	
		if(empty($subject)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a subject']);
		  exit();
		}
	  
		if(empty($seats)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a seats']);
		  exit();
		}
		if(empty($start_date)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a start date']);
		  exit();
		}
		if(empty($end_date)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a end date']);
		  exit();
		}
		if(empty($batch)){
		  echo json_encode(['status'=>403,'message'=>'Please please select atleast one batch']);
		  exit();
		}
		if(empty($description)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a description']);
		  exit();
		}
		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
		{
		$config = array(
		  'upload_path' 	=> 'uploads/courses',
		  'file_name' 	=> str_replace(' ','',$subject.$courseID).uniqid(),
		  'allowed_types' => 'jpg|jpeg|png|gif',
		  'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
		  {
			  $error = $this->upload->display_errors();
			  echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
		  }
		  else
		  {
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/courses/'.$config['file_name'].'.'.$type;
		  }
		}else{
		  $image = 'public/dummy_image.jpg';
		}
	   
		//$batchs = implode(',',$batch);
	
		// if($categoryID=='Other'){
		//  $categoryName  =  $this->input->post('category_name');
		//  $category = $this->category_model->get_category(array('category'=>$categoryName));
		//  if($category){
		//   echo json_encode(['status'=>403, 'message'=>'Category already exits!']);
		//   exit();
		//  }else{
		//   $catData = array(
		// 	'userID' =>$this->session->userdata('id'),
		// 	'category'=>$categoryName,
		// 	'status' =>1,
		//   ) ;
		//   $categoryID = $this->category_model->store_category($catData);
		// }
		   
		// }

		if(!empty($_FILES['subject_upload_pdf']['name'])){
			$pdf_value = array();
			
			 $total_course_pdf = count($_FILES['subject_upload_pdf']['name'])-1;
			for($i = 0; $i < $total_course_pdf;$i++){
			  $uid = uniqid();
			  $filename = $_FILES['subject_upload_pdf']['name'][$i];
			  $f_maxsize = 41943040;
			  //$f_ext_allowed = array("pdf", "jpg", "jpeg", "png", "gif","doc","docx","xlx","xlsx","");
			  $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
			  $f_size  =  $filename;
			  $f_tmp   =  $_FILES["subject_upload_pdf"]['tmp_name'][$i];
			  $f_error =  $_FILES["subject_upload_pdf"]['error'][$i];
			  $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
			  $files = $uid.$filename;
			   move_uploaded_file($f_tmp, "uploads/courses_pdf/".$files);   
			   $pdf_value[] = array(
               $this->input->post('subject_pdf_title')[$i] => $files
			   );             
			}
			
			$subject_upload_pdf=json_encode($pdf_value);
		}else{
			$subject_upload_pdf ="";
		}
	
	   $data = array(
		'courseID' => $courseID,
		'subject' => $subject,
		'free_demo' => $free_demo,
		'mode' => $mode,
		'start_date' => $start_date,
		'end_date' => $end_date,
		'seats' => $seats,
		'remaning_seats' => $seats,
		'fees' => $fees,
		'discount' => $discount,
		'description' => $description,
		'teacher' => $teacher,
		'batch' => $batch,
		'zoom_link' => $zoom_link,
		'image' => $image,
		'subject_upload_pdf' => $subject_upload_pdf,
	   );
	
	   $store = $this->Subject_model->store_subject($data);
	   if($store){
		 echo json_encode(['status'=>200, 'message'=>'Subject added successfully!']);
		 }else{
		   echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}


	public function edit_subject()
	{	 $id = base64_decode($this->uri->segment(2));
		$data['page_title'] = 'Edit Subject';
		$data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
		$data['subject'] = $this->Subject_model->get_subject(array('subjects.id'=>$id));
		$this->admin_template('subject/edit-subject',$data);
	}

  public function update(){
		$id = $this->input->post('id');
		$courseID = $this->input->post('courseID');
		$free_demo = !empty($this->input->post('free_demo')) ? $this->input->post('free_demo') : 0;
		 $subjectName = $this->input->post('subject');
		$mode = $this->input->post('mode');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$seats = $this->input->post('seats');
		$fees = $this->input->post('fees');
		$discount = !empty($this->input->post('discount')) ? $this->input->post('discount') : 0;
		$teacher = $this->input->post('teacher');
		$description = $this->input->post('description');
		$batch = $this->input->post('batch');
		$zoom_link = $this->input->post('zoom_link');
        $subject = $this->Subject_model->get_subject(array('subjects.id'=>$id));
		$check = $this->Subject_model->get_subject(array('subjects.courseID'=>$courseID,'subjects.subject'=>$subjectName,'subjects.id <>'=>$id));
		//print_r($check);die;
		if($check){
		  echo json_encode(['status'=>403,'message'=>'This subject already exists in this course']);
		  exit();
		}
	
		if(empty($courseID)){
		  echo json_encode(['status'=>403,'message'=>'Please select a course']);
		  exit();
		}
	
	
		if(empty($subjectName)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a subject']);
		  exit();
		}
	  
		if(empty($seats)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a seats']);
		  exit();
		}
		if(empty($start_date)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a start date']);
		  exit();
		}
		if(empty($end_date)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a end date']);
		  exit();
		}
		if(empty($batch)){
		  echo json_encode(['status'=>403,'message'=>'Please please select atleast one batch']);
		  exit();
		}
		if(empty($description)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a description']);
		  exit();
		}
		$this->load->library('upload');
		if($_FILES['image']['name'] != '')
			{
		$config = array(
		  'upload_path' 	=> 'uploads/courses',
		  'file_name' 	=> str_replace(' ','',$subject).uniqid(),
		  'allowed_types' => 'jpg|jpeg|png|gif',
		  'max_size' 		=> '10000000',
		);
			$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('image'))
		  {
			  $error = $this->upload->display_errors();
			  echo json_encode(['status'=>403, 'message'=>$error]);
			  exit();
		  }
		  else
		  {
			$type = explode('.',$_FILES['image']['name']);
			$type = $type[count($type) - 1];
			$image = 'uploads/courses/'.$config['file_name'].'.'.$type;
		  }
		}elseif(!empty($subject->image)){
      $image = $subject->image;
    }else{
		  $image = 'public/dummy_image.jpg';
		}
	   
		//$batchs = implode(',',$batch);
	
		// if($categoryID=='Other'){
		//  $categoryName  =  $this->input->post('category_name');
		//  $category = $this->category_model->get_category(array('category'=>$categoryName));
		//  if($category){
		//   echo json_encode(['status'=>403, 'message'=>'Category already exits!']);
		//   exit();
		//  }else{
		//   $catData = array(
		// 	'userID' =>$this->session->userdata('id'),
		// 	'category'=>$categoryName,
		// 	'status' =>1,
		//   ) ;
		//   $categoryID = $this->category_model->store_category($catData);
		// }
		   
		// }
    // $pdf_title = array();
    // $course_pdfs = json_decode($course->course_upload_pdf);
    //       foreach($course_pdfs as $coursePDF){
    //         foreach($coursePDF as $pdfTitle=>$pdf){
    //           $pdf_title[] = $pdfTitle;
    //         }
            
    //       }
    //  $count_pdf_title = count(array_filter($this->input->post('course_pdf_title')));
    //  $count_pdf_title = count(array_filter($this->input->post('course_pdf_title')));
    //  $pdf_value = array(); 
    // for($i = 0; $i < $count_pdf_title;$i++){
    //   $uid = uniqid();
    //    $course_pdf_title = $this->input->post('course_pdf_title')[$i]; 
      
    //    if(!empty($_FILES['course_upload_pdf']['name'][$i])){
            
    //       $filename = $_FILES['course_upload_pdf']['name'][$i];
    //       $f_maxsize = 41943040;
    //       //$f_ext_allowed = array("pdf", "jpg", "jpeg", "png", "gif","doc","docx","xlx","xlsx","");
    //       $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
    //       $f_size  =  $filename;
    //       $f_tmp   =  $_FILES["course_upload_pdf"]['tmp_name'][$i];
    //       $f_error =  $_FILES["course_upload_pdf"]['error'][$i];
    //       $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
    //       $files = $uid.$filename;
    //       move_uploaded_file($f_tmp, "uploads/courses_pdf/".$files);   
      
    //     }else{
    //       $course_pdf_title_hidden = $this->input->post('course_pdf_title_hidden')[$i];
    //         if(in_array($course_pdf_title_hidden,$pdf_title)){
    //            $files = $this->input->post('course_upload_pdf_hidden')[$course_pdf_title_hidden];
    //         }
        
          
    //       //$course_upload_pdf ="";
    //     }
    //     $pdf_value[] = array(
    //       $course_pdf_title => $files
    //     );
           
    // }
   //print_r($pdf_value);die;
    //$course_upload_pdf=json_encode($pdf_value);
	
	   $data = array(
		'courseID' => $courseID,
		'free_demo' => $free_demo,
		'subject' => $subjectName,
		'mode' => $mode,
		'start_date' => $start_date,
		'end_date' => $end_date,
		'seats' => $seats,
		'remaning_seats' => $seats,
		'fees' => $fees,
		'discount' => $discount,
		'description' => $description,
		'teacher' => $teacher,
		'batch' => $batch,
		'image' => $image,
		'zoom_link' => $zoom_link,
		//'course_upload_pdf' => $course_upload_pdf,
	   );
	
	   $update = $this->Subject_model->update_subject($data,$id);
	   if($update){
		 echo json_encode(['status'=>200, 'message'=>'Subject update successfully!']);
		 }else{
		   echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	}

	public function approved_courses()
	{	
		$data['page_title'] = 'Approved  Courses';
		$condition =  array('courses.status'=>1) ;
        $data['courses'] = $this->course_model->get_courses($condition);
		$this->admin_template('courses',$data);
	}

  public function update_status()
  {
	  $id = $this->input->post('userid');

	  $data =  array(
		'status' => 1
	  );
	  $update_status =  $this->course_model->updateCourse($data,$id);
    $course = $this->course_model->get_course(array('courses.id' =>$id));
	  $message = "Admin Accepted your course request for ".$course->course;
    $data = array(
      'userID' => $course->userID,
      'send_by' => $this->session->userdata('id'),
      'message' => $message
    );
    $store_notification = $this->notification_model->store_notification($data);
  }


  public function subject()
  {	
	  $data['page_title'] = 'Subject';
	  $condition =  array('subjects.status'=>1) ;
	  $data['subjects'] = $this->Subject_model->get_subjects($condition);
	  $data['courses'] = $this->course_model->get_courses(array('courses.status'=>1));
	  $this->admin_template('subject',$data);
  }

  public function store_subject(){

		$courseID = $this->input->post('courseID');
		$subject = $this->input->post('subject');
		//$check = $this->course_model->get_course(array('courses.course'=>$course,'courses.userID'=>$userID));
		// if($check){
		//   echo json_encode(['status'=>403,'message'=>'This course already exists']);
		//   exit();
		// }
	
		if(empty($courseID)){
		  echo json_encode(['status'=>403,'message'=>'Please select a course']);
		  exit();
		}
	
		if(empty($subject)){
		  echo json_encode(['status'=>403,'message'=>'Please enter a subject']);
		  exit();
		}
        $array_filetr = array_filter($_FILES['subject_pdf_upload']['name']);
		if(!empty($array_filetr)){
			 $pdf_value = array();
			 $total_course_pdf = count($_FILES['subject_pdf_upload']['name'])-1;
			for($i = 0; $i < $total_course_pdf;$i++){
			  $uid = uniqid();
			  $filename = $_FILES['subject_pdf_upload']['name'][$i];
			  $f_maxsize = 41943040;
			  $f_ext_allowed = array("pdf");
			  $f_name_2 = str_replace(" ","_", htmlspecialchars($filename));
			  $f_size  =  $filename;
			  $f_tmp   =  $_FILES["subject_pdf_upload"]['tmp_name'][$i];
			  $f_error =  $_FILES["subject_pdf_upload"]['error'][$i];
			  $f_ext = pathinfo($f_name_2, PATHINFO_EXTENSION); 
			  $files = $uid.$filename;
			   move_uploaded_file($f_tmp, "uploads/courses_pdf/".$files);   
			   $pdf_value[] = array(
               $this->input->post('subject_pdf_title')[$i] => $files
			   );             
			}
			
			$subject_pdf_upload=json_encode($pdf_value);
		}else{
			$subject_pdf_upload ="";
		}
	
	   $data = array(
		'courseID' => $courseID,
		'subject' => $subject,
		'subject_pdf_upload' => $subject_pdf_upload,
	   );
	
	   $store = $this->Subject_model->store_subject($data);
	   if($store){
		 echo json_encode(['status'=>200, 'message'=>'Subject add successfully!']);
		 }else{
		   echo json_encode(['status'=>302, 'message'=>mysqli_error()]);   
		}
	
  }

  public function edit_subject_form(){
	$subjectID = $this->input->post('subjectID');
	$subject = $this->Subject_model->get_subject(array('subjects.id' => $subjectID));
	$courses = $this->course_model->get_courses(array('courses.status'=>1));
	?>
	<div class="form-group">
        <label for="name" class="col-form-label">Course:</label>
        <select class="form-control" name="courseID" id="courseID">
          <option value="">Select Course</option>
          <?php foreach($courses as $course){?>
              <option value="<?=$course->id?>" <?=$subject->courseID ==$course->id ? 'selected' : ''  ?>><?=$course->course?></option>
          <?php } ?>
        </select>
        </div>
        <div class="form-group">
          <label for="name" class="col-form-label">Subject Name:</label>
          <input type="text" class="form-control" name="subject" id="subject" value="<?=$subject->subject?>">
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group">
                <label>Upload Course PDF</label>   
				
                <div class="input-group control-group after-add-more">
                  <div class="row">
				    <?php if($subject->subject_pdf_upload == ""){?> 
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="subject_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="subject_pdf_upload[]"  class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
				  <?php } else{ ?>
				<div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> </button>   
                  </div>
				<?php
			} ?>
                </div>
			
				
              </div>
              <div class="form-group copy hide">
                <div class="control-group input-group" style="margin-top:10px">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="text" name="subject_pdf_title[]"  class="form-control" placeholder="Enter PDF Title">  
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12">
                      <input type="file" name="subject_pdf_upload[]"  class="form-control" placeholder="Enter Name Here">  
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 input-group-btn">   
                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> </button>  
                  </div>
                </div>
              </div>
            </div>
        
	<?php
  }
	
}