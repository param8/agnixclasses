<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Admin_Controller 

{

	public function __construct()

	{

		parent::__construct();
		$this->load->model('Model_auth');

	}



	/* 

		Check if the login form is submitted, and validates the user credential

		If not submitted it redirects to the login page

	*/

	public function login()

	{

		$this->logged_in();

		$this->form_validation->set_rules('email', 'Email', 'required');

        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == TRUE) {

            // true case

           	$email_exists = $this->Model_auth->check_email($this->input->post('email'));
           	if($email_exists == TRUE) {

           		$login = $this->Model_auth->login($this->input->post('email'), $this->input->post('password'));
           		
           		if($login) {
           			$logged_in_sess = array(
           				'id' => $login['id'],
				        'username'    => $login['username'],
				        'email'       => $login['email'],
				        'firstname'   => $login['firstname'],
				        'lastname'    => $login['lastname'],
						'user_type'   => $login['user_type'],
						'location_id' => $login['location_id'],
						'user_access' => ($login['user_access']!='')?explode(', ',$login['user_access']):'',
				        'logged_in'   => TRUE

					);
           			
           			
           			if($login['user_type'] == 'Admin' || $login['user_type'] == 'Super Admin'){
           				$this->session->set_userdata($logged_in_sess);
           				redirect('admin/dashboard', 'refresh');
	
           			}else{
           				$this->session->set_userdata($logged_in_sess);
           				redirect('user/application', 'refresh');
           			}

           		}

           		else {

           			$this->data['errors'] = 'Incorrect username/password combination';

           			
           			$this->load->view('includes/header');
           			$this->load->view('login', $this->data);
           			$this->load->view('includes/footer');
           		}

           	}

           	else {

           		$this->data['errors'] = 'Email/Mobile no does not exists';
           		$this->load->view('includes/header');
           		$this->load->view('login', $this->data);
           		$this->load->view('includes/footer');
           	}	

        }

        else {

            // false case

            $this->load->view('login');

        }	

	}



	/*

		clears the session and redirects to login page

	*/

	public function logout()

	{

		$this->session->sess_destroy();

		redirect('login', 'refresh');

	}

	public function qr_user_application()
	{
		 $form_id =base64_decode($this->uri->segment(3));
		$data=array();	
		$application_details = $this->application_model->get_application_details($form_id);
		$data['application_details'] = $application_details;
		$data['qualification'] = $this->application_model->get_qualification_view($form_id);
		$this->load->library('pdf');
		$html = $this->load->view('application_report', $data, true);
		$this->pdf->createPDF($html, 'mypdf', false);
	}



}

