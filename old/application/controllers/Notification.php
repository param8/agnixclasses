<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->not_logged_in();
		$this->load->model('user_model');
	
	}

	public function index(){
		$data['page_title'] = 'Notification';
        $id =$this->session->userdata('id');

		$data['notification'] = $this->notification_model->get_notifications(array('userID' =>$id));
		$this->website_template('notification',$data);
	}

    public function redirect_notification(){
        $id =$this->session->userdata('id');
        $data=array(
            'status' => 1
        );
        $status = $this->notification_model->update_notification($data,$id);
    }

	public function refer_earn(){
		$data['page_title'] = 'Referral-Earn';
		$data['wallet'] = $this->user_model->get_wallet(array('userID'=>$this->session->userdata('id')));
		$this->website_template('earn-refral',$data);
	}

	public function redeem_now(){
		$data['page_title'] = 'Redeem-Now';
		$this->website_template('redeem-now',$data);
	}
	
}
