<?php 
  session_start();
  include('components/config.php');
  include('components/head.php');
  
  ini_set('display_errors',1);
  //echo "SELECT * FROM courses";
  // $Courses = $conn->query("SELECT * FROM courses");
  // print_r($courses->fetch_assoc());
  $locationID = $_SESSION['city'];
  $query="SELECT * FROM courses WHERE locationID = $locationID";
  $courses = $conn->query($query);
  
    $user_id = mysqli_real_escape_string($conn, $_GET['user_id']);
    $sql = mysqli_query($conn, "SELECT * FROM users WHERE unique_id = {$user_id}");
  
    if(mysqli_num_rows($sql) > 0){
      $row_user = mysqli_fetch_assoc($sql);
    }
    if(!empty($_SESSION['course_id'])){
      $sessionCourseId = $_SESSION['course_id'];
      $query_course="SELECT * FROM courses WHERE id = $sessionCourseId";
      $session_course = $conn->query($query_course);
      $course_session = $session_course->fetch_assoc();
    }
  ?>
  <style>
    .gap{
      gap:15px;
      ali
    }
    .from_input {
    width: 95%;
}
.c_hat {
    display: flex;
    width: 100%;
}
.c_hat button {
    padding: 0 17px;
    border: none;
    background-color: #665dfe;
    color: #fff;
}
.c_hat button:focus{
  outline: none!important ;
}
    </style>
<body class="chats-tab-open">
  <!-- Main Layout Start -->
  <div class="main-layout">
    <!-- Navigation Start -->
    <!-- Navigation End -->
    <!-- Sidebar Start -->
    <aside class="sidebar">
      <!-- Tab Content Start -->
      <div class="tab-content">
        <!-- Chat Tab Content Start -->
        <div class="tab-pane active" id="chats-content">
          <div class="d-flex flex-column h-100">
            <div class="hide-scrollbar h-100" id="chatContactsList">
              <!-- Chat Header Start -->
              <div class="sidebar-header sticky-top p-2">
                <div class="d-flex justify-content gap align-items-center">
                  <!-- Chat Tab Pane Title Start -->
                  <div class="avatar avatar-online">
                    <img src="../<?=$_SESSION['profile_pic']?>" alt="">
                  </div>
                  <div class="mr-8">
                  <?=$_SESSION['name']?>
                    <p class="text-success"><?=$_SESSION['login_status']?></p>
                  </div>
                  <h6 >
                    
                  </h6>
                  <!-- Chat Tab Pane Title End -->
                </div>
                <!-- Sidebar Header Start -->
                <!--<div class="sidebar-sub-header">-->
                  <!-- Sidebar Header Dropdown Start -->
                <!--  <div class="dropdown mr-2">-->
                    <!-- Dropdown Button Start -->
                <!--    <button class="btn btn-outline-default dropdown-toggle" type="button"-->
                <!--      data-chat-filter-list="" data-toggle="dropdown" aria-haspopup="true"-->
                <!--      aria-expanded="false" >-->
                <!--      <?=!empty($_SESSION['course_id']) ? $course_session['course'] : ' All Courses' ;?>-->
                   
                <!--    </button>-->
                    <!-- Dropdown Button End -->
                    <!-- Dropdown Menu Start -->
                    <!--<div class="dropdown-menu">-->
                    <!--  <?php //while ($course = $courses->fetch_assoc()){?>-->
                    <!--  <a class="dropdown-item" data-chat-filter="" onclick="getGroupUsers(<?//=$course['id']?>)" data-select="all-chats"  href="#" ><?=$course['course']?></a>-->
                    <!--  <?php //} ?>-->
                    <!--</div>-->
                    
                <!--    <p class="text-danger text-center" style="cursor:pointer" onclick="resetCourse()"> Reset</p>-->
                    <!-- Dropdown Menu End -->
                <!--  </div>-->
                  <!-- Sidebar Header Dropdown End -->
                  <!-- Sidebar Search Start -->
                <!--  <form class="form-inline">-->
                <!--    <div class="input-group">-->
                <!--      <input type="text"-->
                <!--        class="form-control search border-right-0 transparent-bg pr-0"-->
                <!--        placeholder="Search users" name="search_user" id="search_user" value = "<?= !empty($_SESSION['search_user_name']) ? $_SESSION['search_user_name'] : ''?>">-->
                <!--      <div class="input-group-append">-->
                <!--        <div class="input-group-text transparent-bg border-left-0"-->
                <!--          role="button" onclick="searchUser()">-->
                          <!-- Default :: Inline SVG -->
                <!--          <svg class="text-muted hw-20" fill="none" viewBox="0 0 24 24"-->
                <!--            stroke="currentColor">-->
                <!--            <path stroke-linecap="round" stroke-linejoin="round"-->
                <!--              stroke-width="2"-->
                <!--              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />-->
                <!--          </svg>-->
                          <!-- Alternate :: External File link -->
                          <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/search.svg" alt=""> -->
                <!--        </div>-->
                <!--      </div>-->
                     
                <!--    </div>-->
                <!--    <p class="text-danger text-center ml-5" style="cursor:pointer" onclick="resetUser()"> Reset</p>-->
                <!--  </form>-->
                  <!-- Sidebar Search End -->
                <!--</div>-->
                <!-- Sidebar Header End -->
              </div>
              <!-- Chat Header End -->
              <!-- Chat Contact List Start -->
              <ul class="contacts-list" id="chatContactTab" data-chat-list="">
                <!-- Chat Item Start -->
                <?php 
                $user_where ="";
                $id = $_SESSION['id'];
                if(!empty($_SESSION['search_user_name'])){
                  $search_user_name = $_SESSION['search_user_name'];
                  $user_where = " AND name LIKE '%$search_user_name%'";
                } 

                  if(!empty($_SESSION['course_id'])){
                    $course_id = $_SESSION['course_id'];
                    $user_where = " AND booked_courses.courseID = $course_id";
                    $users_query = "SELECT * FROM users LEFT JOIN booked_courses ON booked_courses.studentID = users.id WHERE users.status = 1 AND users.user_type <> 'Admin' AND users.id <> $id $user_where GROUP BY booked_courses.studentID";
                  }else{
                    $users_query = "SELECT * FROM users WHERE `status` = 1 AND user_type <> 'Admin' AND id <> $id $user_where";
                  }
                   
                   $users = $conn->query($users_query);
                   if($users->num_rows > 0){
                   while ( $user= $users->fetch_assoc()){
                  
                  ?>
                <li class="contacts-item friends ">
                  <a class="contacts-link" href="chat.php?user_id=<?=$user['unique_id']?>" >
                    <div class="avatar <?=$user['login_status']=='Active now' ? 'avatar-online' : 'avatar-away'?> ">
                      <img src="../<?=$user['profile_pic']?>" alt="">
                    </div>
                    <div class="contacts-content">
                      <div class="contacts-info">
                        <h6 class="chat-name text-truncate"><?=$user['name']?></h6>
                        <div class="chat-time"><?//=$user['unique_id']?></div>
                      </div>
                      <div class="contacts-texts">
                        <p class="text-truncate"><?=$user['user_type']?></p>
                      </div>
                    </div>
                  </a>
                </li>
                <?php } }else{ ?>
                  <li class="contacts-item friends ">
                    <p class="text-danger mt-5 text-center">No User Found</p>
                </li>
                  <?php } ?>
                <!-- Chat Item End -->
              </ul>
              <!-- Chat Contact List End -->
            </div>
          </div>
        </div>
        <!-- Chats Tab Content End -->
      </div>
      <!-- Tab Content End -->
    </aside>
    <!-- Sidebar End -->
    <!-- Main Start -->
    <main class="main main-visible">
      <!-- Chats Page Start -->
      <div class="chats">
        <div class="chat-body">
          <!-- Chat Header Start-->
          <div class="chat-header">
            <!-- Chat Back Button (Visible only in Small Devices) -->
            <button class="btn btn-secondary btn-icon btn-minimal btn-sm text-muted d-xl-none"
              type="button" data-close="">
              <!-- Default :: Inline SVG -->
              <svg class="hw-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                  d="M10 19l-7-7m0 0l7-7m-7 7h18" />
              </svg>
              <!-- Alternate :: External File link -->
              <!-- <img class="injectable hw-20" src="./../../assets/media/heroicons/outline/arrow-left.svg" alt=""> -->
            </button>
            <!-- Chat participant's Name -->
            <div class="media chat-name align-items-center text-truncate">
              <div class="avatar avatar-online d-none d-sm-inline-block mr-3">
                <img src="../<?=$row_user['profile_pic']?>" alt="">
              </div>
              <div class="media-body align-self-center ">
                <h6 class="text-truncate mb-0"><?=$row_user['name']?></h6>
                <small class="text-muted"><?=$row_user['login_status']?></small>
              </div>
            </div>
          </div>
          <!-- Chat Header End-->
          <!-- Chat Content Start-->
          <div class="chat-content p-2" id="messageBody">
            <div class="container">
              <!-- Message Day Start -->
              <div class="message-day">
                <!-- <div class="message-divider sticky-top pb-2" data-label="Yesterday">&nbsp;</div> -->
                <div class="chat-box"></div>
              </div>
              <!-- Message Day End -->
            </div>
            <!-- Scroll to finish -->
            <div class="chat-finished" id="chat-finished"></div>
          </div>
          <!-- Chat Content End-->
          <!-- Chat Footer Start-->
          <div class="chat-footer">
            <form action="#" class="typing-area">
            <div class="c_hat"> 
           <div class="from_input">
           <input type="text" class="incoming_id" name="incoming_id" value="<?php echo $user_id; ?>" hidden>
            <input type="text" name="message" class="input-field form-control" placeholder="Type a message here..." autocomplete="off">
            
           </div>
            <button><i class="fab fa-telegram-plane"></i></button>
            
           </div>
          </form>
          </div>
          <!-- Chat Footer End-->
        </div>
      </div>
      <!-- Chats Page End -->
    </main>
    <!-- Main End -->
    <div class="backdrop"></div>
  </div>
  <!-- Main Layout End -->
  <?php include('components/footer.php')?>
  <script>
   function getGroupUsers(id){
    var setSession = "Set Session";
    $.ajax({
      url: 'setCourseSession.php',
      type: 'POST',
      data: {id,setSession},
      dataType: 'json',
      success: function (data) {
        if(data==true) {
          location.reload();
        }
      }
      });
   }

   function searchUser(){
    var search_user = $('#search_user').val();
    var setSession = 'Set User Name';
     $.ajax({
      url: 'setCourseSession.php',
      type: 'POST',
      data: {search_user,setSession},
      //dataType: 'json',
      success: function (data) {
        if(data==true) {
          location.reload();
        }
      }
      });
   }

   function resetCourse(){
    var setSession = 'Reset Course';
     $.ajax({
      url: 'setCourseSession.php',
      type: 'POST',
      data: {setSession},
      //dataType: 'json',
      success: function (data) {
        if(data==true) {
          location.reload();
        }
      }
      });
   }

   function resetUser(){
    var setSession = 'Reset User Name';
     $.ajax({
      url: 'setCourseSession.php',
      type: 'POST',
      data: {setSession},
      //dataType: 'json',
      success: function (data) {
        if(data==true) {
          location.reload();
        }
      }
      });
   }
  </script>